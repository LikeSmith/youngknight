"""
MSDlin.py

Simulates a linear 1DOF mass-spring-damper system with semi-implicit integration.
"""

import numpy as np

from ..core import Simulator

class MSDlin(Simulator):
    def __init__(self, limits, noise_params, dt=0.05, t_f=None, enforce_limits=False, ctrl=None, obs=None, rwd=None, done_cond=None, full_state_feedback=True, name='MSDlin'):
        self.limits = limits
        self.noise_params = noise_params
        self.dt = dt
        self.t_f = t_f
        self.enforce_limits = enforce_limits
        self.ctrl = ctrl
        self.obs = obs
        self.rwd = rwd
        self.done_cond = done_cond
        self.full_state_feedback = full_state_feedback
        self.name = name
        
        if self.obs is None:
            obs_size = (3,)
            obs_dtype = 'float32'
        else:
            s = self.init_state_gen(1)
            params = self.param_gen(1)
            o = self.obs(s, 0, params)
            obs_size = o.shape[1:]
            obs_dtype = o.dtype
            
        super(MSDlin, self).__init__(state_size=(2,), action_size=(1,), obs_size=obs_size, obs_dtype=obs_dtype, name=name)
        
    def step_forward(self, state, action, k, params):
        p = state[:, 0]
        v = state[:, 1]
        
        f = action[:, 0]
        
        m = params[:, 0]
        k = params[:, 1]
        c = params[:, 2]
        
        if self.enforce_limits:
            f = np.clip(f, self.limits['f_min'], self.limits['f_max'])
        
        a_new = (f - k*p - c*v)/m
        v_new = v + self.dt*a_new
        p_new = p + self.dt*v_new
        
        if self.enforce_limits:
            v_new = np.clip(v_new, self.limits['v_min'], self.limits['v_max'])
            p_new = np.clip(p_new, self.limits['p_min'], self.limits['p_max'])
        
        return np.stack((p_new, v_new), axis=1)
    
    def action_gen(self, state, observ, k, params, des_traj=None):
        if self.ctrl is not None:
            if self.full_state_feedback:
                return self.ctrl(state, params, k, des_traj=None)
            else:
                return self.ctrl(observ, params, k, des_traj=None)
        else:
            n = state.shape[0]
            return np.tile(self.limits['f_min'], (n, 1)) + np.tile(self.limits['f_max'] - self.limits['f_min'], (n, 1))*np.random.rand(n, 1)
        
    def obs_gen(self, state, k, params):
        if self.obs is None:
            return state
        else:
            return self.obs(state, k, params)
        
    def reward_gen(self, state, state1, action, k, params, des_traj=None):
        if self.rwd is None:
            p = state[:, 0]
            v = state[:, 1]
            f = action[:, 0]
            cost = p**2 + 0.1*v**2 + 0.001*f**2
            return -cost
        else:
            return self.rwd(state, state1, action, k, params, des_traj)
        
    def done_gen(self, state, k, params):
        if self.done_cond is None:
            if self.t_f is not None and k >= self.t_f:
                return np.ones((state.shape[0],), dtype='bool')
            else:
                return np.zeros((state.shape[0],), dtype='bool')
        else:
            return self.done_cond(state, k, params)
        
    def noise_gen(self, state, k, params):
        n = state.shape[0]
        process_noise = np.random.multivariate_normal(self.noise_params['process_mean'], self.noise_params['process_cov'], n).reshape((n,)+self.state_size)
        signal_noise = np.random.multivariate_normal(self.noise_params['signal_mean'], self.noise_params['signal_cov'], n).reshape((n,)+self.obs_size)
        
        return [process_noise, signal_noise]
        
    def param_gen(self, n):
        m = np.tile(self.limits['m_min'], n) + (self.limits['m_max'] - self.limits['m_min'])*np.random.rand(n)
        k = np.tile(self.limits['k_min'], n) + (self.limits['k_max'] - self.limits['k_min'])*np.random.rand(n)
        c = np.tile(self.limits['c_min'], n) + (self.limits['c_max'] - self.limits['c_min'])*np.random.rand(n)
        
        return np.stack((m, k, c), axis=1)
    
    def init_state_gen(self, n):
        p = np.tile(self.limits['p_min'], n) + (self.limits['p_max'] - self.limits['p_min'])*np.random.rand(n)
        v = np.tile(self.limits['v_min'], n) + (self.limits['v_max'] - self.limits['v_min'])*np.random.rand(n)
        
        return np.stack((p, v), axis=1)

