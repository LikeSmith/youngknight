"""
ExtSim.py

Uses external dynamics
"""

import numpy as np

from ..core import Simulator, rand_vectors, quad

class ExtSim(Simulator):
    def __init__(self, step_fun, limits, noise_params, dt=0.05, t_f=None, enforce_limits=False, inv=True, ctrl=None, obs=None, rwd=None, done_cond=None, full_state_feedback=True, name='ExtSim'):
        self.step_fun = step_fun
        self.limits = limits
        self.noise_params = noise_params
        self.t_f = t_f
        self.enforce_lims = enforce_limits
        self.inv = inv
        self.ctrl = ctrl
        self.obs = obs
        self.rwd = rwd
        self.done_cond = done_cond
        self.full_state_feedback = full_state_feedback
        self.dt = dt
        
        s = self.init_state_gen(1)
        params = self.param_gen(1)
        o = self.obs_gen(s, 0, params)
        a = self.action_gen(s, o, 0, params)
            
        super(ExtSim, self).__init__(state_size=s.shape[1:], action_size=a.shape[1:], obs_size=o.shape[1:], obs_dtype=o.dtype, name=name)
        
    def step_forward(self, state, action, k, params):
        batch_size = state.shape[0]
        if self.enforce_lims:
            action = np.clip(action, np.tile(np.expand_dims(self.limits['a_min'], axis=0), (batch_size, 1)),  np.tile(np.expand_dims(self.limits['a_max'], axis=0), (batch_size, 1)))
        
        next_state = self.step_fun(state, action, k, params)
        
        if self.enforce_lims:
            next_state = np.clip(next_state, np.tile(np.expand_dims(self.limits['s_min'], axis=0), (batch_size, 1)),  np.tile(np.expand_dims(self.limits['s_max'], axis=0), (batch_size, 1)))
            
        return next_state
            
    def action_gen(self, state, observ, k, params, des_traj=None):
        if self.ctrl is not None:
            if self.full_state_feedback:
                return self.ctrl(state, params, k, des_traj=None)
            else:
                return self.ctrl(observ, params, k, des_traj=None)
        else:
            n = state.shape[0]
            return rand_vectors(n, self.limits['a_min'], self.limits['a_max'])
        
    def obs_gen(self, state, k, params):
        if self.obs is None:
            return state
        else:
            return self.obs(state, k, params)
        
    def reward_gen(self, state, state1, action, k, params, des_traj=None):
        if self.rwd is None:
            return quad(state, np.eye(self.state_size))
        else:
            return self.rwd(state, state1, action, k, params)
        
    def done_gen(self, state, k, params):
        if self.done_cond is None:
            if self.t_f is not None and k >= self.t_f:
                return np.ones((state.shape[0],), dtype='bool')
            else:
                return np.zeros((state.shape[0],), dtype='bool')
        else:
            return self.done_cond(state, k, params)
        
    def noise_gen(self, state, k, params):
        n = state.shape[0]
        process_noise = np.random.multivariate_normal(self.noise_params['process_mean'], self.noise_params['process_cov'], n).reshape((n,)+self.state_size)
        signal_noise = np.random.multivariate_normal(self.noise_params['signal_mean'], self.noise_params['process_cov'], n).reshape((n,)+self.obs_size)
        
        return [process_noise, signal_noise]
        
    def param_gen(self, n):
        return rand_vectors(n, self.limits['p_min'], self.limits['p_max'])
    
    def init_state_gen(self, n):
        return rand_vectors(n, self.limits['s_min'], self.limits['s_max'])
