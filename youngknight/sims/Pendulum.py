"""
Pendulum simulator

Simulates a pendulum system with semi-implicit integration
"""

import numpy as np
import tensorflow as tf

from ..core import Simulator

class Pendulum(Simulator):
    def __init__(self, limits, noise_params, dt=0.05, t_f=None, enforce_lims=False, inv=True, ctrl=None, obs=None, rwd=None, done_cond=None, full_state_feedback=True, name='Pendulum'):
        self.limits = limits
        self.noise_params = noise_params
        self.t_f = t_f
        self.enforce_lims = enforce_lims
        self.inv = inv
        self.ctrl = ctrl
        self.obs = obs
        self.rwd = rwd
        self.done_cond = done_cond
        self.full_state_feedback = full_state_feedback
        self.dt = dt
        
        if self.obs is None:
            obs_size = (3,)
            obs_dtype = 'float32'
        else:
            s = self.init_state_gen(1)
            params = self.param_gen(1)
            o = self.obs(s, 0, params)
            obs_size = o.shape[1:]
            obs_dtype = o.dtype
            
        super(Pendulum, self).__init__(state_size=(2,), action_size=(1,), obs_size=obs_size, obs_dtype=obs_dtype, name=name)
        
    def step_forward(self, state, action, k, params):
        theta = state[:, 0]
        omega = state[:, 1]
        tau = action[:, 0]
        m = params[:, 0]
        l = params[:, 1]
        c = params[:, 2]
        g = params[:, 3]
        
        if self.enforce_lims:
            tau = np.clip(tau, self.limits['tau_min'], self.limits['tau_max'])
        
        if self.inv:
            alpha = 3*tau/(m*l**2) + 3*g/(2*l)*np.sin(theta) - 3*omega*c/(m*l**2)
        else:
            alpha = 3*tau/(m*l**2) - 3*g/(2*l)*np.sin(theta) - 3*omega*c/(m*l**2)
        
        #alpha = np.cos(theta)*tau/m - omega*c/m
        
        new_omega = omega + self.dt*alpha
        new_theta = theta + self.dt*new_omega
        
        if self.enforce_lims:
            new_omega = np.clip(new_omega, self.limits['omega_min'], self.limits['omega_max'])
        
        return np.concatenate((np.expand_dims(new_theta, axis=-1), np.expand_dims(new_omega, axis=-1)), axis=1)
            
    def action_gen(self, state, observ, k, params, des_traj=None):
        if self.ctrl is not None:
            if self.full_state_feedback:
                return self.ctrl(state, params, k, des_traj=None)
            else:
                return self.ctrl(observ, params, k, des_traj=None)
        else:
            n = state.shape[0]
            return np.tile(self.limits['tau_min'], (n, 1)) + np.tile(self.limits['tau_max'] - self.limits['tau_min'], (n, 1))*np.random.rand(n, 1)
        
    def obs_gen(self, state, k, params):
        if self.obs is None:
            return np.concatenate((np.cos(state[:, 0:1]), np.sin(state[:, 0:1]), state[:, 1:]), axis=1)
        else:
            return self.obs(state, k, params)
        
    def reward_gen(self, state, state1, action, k, params, des_traj=None):
        if self.rwd is None:
            theta = ((state[:, 0] + np.pi)%(2*np.pi)) - np.pi
            omega = state[:, 1]
            tau = action[:, 0]
            cost = theta**2 + 0.1*omega**2 + 0.001*tau**2
            return -cost
        else:
            return self.rwd(state, state1, action, k, params)
        
    def done_gen(self, state, k, params):
        if self.done_cond is None:
            if self.t_f is not None and k >= self.t_f:
                return np.ones((state.shape[0],), dtype='bool')
            else:
                return np.zeros((state.shape[0],), dtype='bool')
        else:
            return self.done_cond(state, k, params)
        
    def noise_gen(self, state, k, params):
        n = state.shape[0]
        process_noise = np.random.multivariate_normal(self.noise_params['process_mean'], self.noise_params['process_cov'], n).reshape((n,)+self.state_size)
        signal_noise = np.random.multivariate_normal(self.noise_params['signal_mean'], self.noise_params['process_cov'], n).reshape((n,)+self.obs_size)
        
        return [process_noise, signal_noise]
        
    def param_gen(self, n):
        m = np.tile(self.limits['m_min'], (n, 1)) + np.tile(self.limits['m_max'] - self.limits['m_min'], (n, 1))*np.random.rand(n, 1)
        l = np.tile(self.limits['l_min'], (n, 1)) + np.tile(self.limits['l_max'] - self.limits['l_min'], (n, 1))*np.random.rand(n, 1)
        c = np.tile(self.limits['c_min'], (n, 1)) + np.tile(self.limits['c_max'] - self.limits['c_min'], (n, 1))*np.random.rand(n, 1)
        g = np.tile(self.limits['g_min'], (n, 1)) + np.tile(self.limits['g_max'] - self.limits['g_min'], (n, 1))*np.random.rand(n, 1)
        
        return np.concatenate((m, l, c, g), axis=1)
    
    def init_state_gen(self, n):
        theta =  np.tile(self.limits['theta_min'], (n, 1)) + np.tile(self.limits['theta_max'] - self.limits['theta_min'], (n, 1))*np.random.rand(n, 1)
        #omega =  np.tile(0.5*self.limits['omega_min'], (n, 1)) + 0.5*np.tile(self.limits['omega_max'] - self.limits['omega_min'], (n, 1))*np.random.rand(n, 1)
        omega =  np.tile(-1.0, (n, 1)) + np.tile(2.0, (n, 1))*np.random.rand(n, 1)
        
        return np.concatenate((theta, omega), axis=1)
        
    def tf_step_forward(self, state, action, k, params):
        dt = tf.Constant(self.dt)
        
        theta = state[:, 0]
        theta_dot = state[:, 1]
        tau = action[:, 0]
        m = params[:, 0]
        l = params[:, 1]
        c = params[:, 2]
        g = params[:, 3]
        
        if self.inv:
            theta_ddot = tau/(m*(l**2)) + tf.sin(theta)*g/l - theta_dot*c/(m*(l**2))
        else:
            theta_ddot = tau/(m*(l**2)) - tf.sin(theta)*g/l - theta_dot*c/(m*(l**2))
            
        new_theta_dot = theta_dot + dt*theta_ddot
        new_theta = theta + dt*new_theta_dot
        
        return tf.concat([new_theta, new_theta_dot], axis=-1)
