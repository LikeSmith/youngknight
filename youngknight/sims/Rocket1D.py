"""
Rocket1D.py

A 1 Dimentsional rocket simulation using semi-implicit integration.
"""

import numpy as np

from ..core import Simulator

class Rocket1D(Simulator):
    def __init__(self, limits, noise_params, dt=0.05, t_f=None, enforce_limits=False, ctrl=None, obs=None, rwd=None, done_cond=None, full_state_feedback=True, name='Rocket1D'):
        self.limits = limits
        self.noise_params = noise_params
        self.dt = dt
        self.t_f = t_f
        self.Enforce_limits = enforce_limits
        self.ctrl = ctrl
        self.obs = obs
        self.rwd = rwd
        self.done_cond = done_cond
        self.full_state_feedback = full_state_feedback
        self.name = name
        
        if self.obs is None:
            obs_size = (3,)
            obs_dtype = 'float32'
        else:
            s = self.init_state_gen(1)
            params = self.param_gen(1)
            o = self.obs(s, 0, params)
            obs_size = o.shape[1:]
            obs_dtype = o.dtype
            
        super(Rocket1D, self).__init__(state_size=(2,), action_size=(1,), obs_size=obs_size, obs_dtype=obs_dtype, name=name)
        
    def step_forward(self, state, action, k, params):
        p = state[:, 0]
        v = state[:, 1]
        m_f = state[:, 2]
        
        f_t = action[:, 1]
        
        v_e = params[:, 0]
        m_r = params[:, 1]
        rho = params[:, 2]
        c_d = params[:, 3]
        A = params[:, 4]
        
        if m_f < self.dt*f_t/v_e:
            f_t = v_e*m_f/self.dt
            
        if self.enforce_limits:
            f_t = np.clip(f_t, self.limits['f_t_min'], self.limits['f_t_max'])
            
        m_f_new = m_f - self.dt*f_t/v_e
        if m_f_new < 0:
            m_f_new = 0
            
        a_new = (f_t - 0.5*rho*c_d*A*v**2)/(m_r + m_f_new)
        v_new = v + self.dt*a_new
        p_new = p + self.dt*v_new
        
        if self.enforce_limits:
            v_new = np.clip(v_new, self.limits['v_min'], self.limits['v_max'])
            p_new = np.clip(p_new, self.limits['p_min'], self.limits['p_max'])
            
        return self.stack((p_new, v_new, m_f_new), axis=1)
        
    def action_gen(self, state, observ, k, params, des_traj=None):
        if self.ctrl is not None:
            if self.full_state_feedback:
                return self.ctrl(state, params, k, des_traj=None)
            else:
                return self.ctrl(observ, params, k, des_traj=None)
        else:
            n = state.shape[0]
            return np.tile(self.limits['f_t_min'], (n, 1)) + np.tile(self.limits['f_t_max'] - self.limits['f_min'], (n, 1))*np.random.rand(n, 1)
        
    def obs_gen(self, state, k, params):
        if self.obs is None:
            return state
        else:
            return self.obs(state, k, params)
        
    def reward_gen(self, state, state1, action, k, params, des_traj=None):
        if self.rwd is None:
            p = state[:, 0]
            v = state[:, 1]
            f = action[:, 0]
            cost = p**2 + 0.1*v**2 + 0.001*f**2
            return -cost
        else:
            return self.rwd(state, state1, action, k, params)
        
    def done_gen(self, state, k, params):
        if self.done_cond is None:
            if self.t_f is not None and k >= self.t_f:
                return np.ones((state.shape[0],), dtype='bool')
            else:
                return np.zeros((state.shape[0],), dtype='bool')
        else:
            return self.done_cond(state, k, params)
        
    def noise_gen(self, state, k, params):
        n = state.shape[0]
        process_noise = np.random.multivariate_normal(self.noise_params['process_mean'], self.noise_params['process_cov'], n).reshape((n,)+self.state_size)
        signal_noise = np.random.multivariate_normal(self.noise_params['signal_mean'], self.noise_params['process_cov'], n).reshape((n,)+self.obs_size)
        
        return [process_noise, signal_noise]
        
    def param_gen(self, n):
        v_e = np.tile(self.limits['v_e_min'], n) + (self.limits['v_e_max'] - self.limits['v_e_min'])*np.random.rand(n)
        m_r = np.tile(self.limits['m_r_min'], n) + (self.limits['m_r_max'] - self.limits['m_r_min'])*np.random.rand(n)
        rho = np.tile(self.limits['rho_min'], n) + (self.limits['rho_max'] - self.limits['rho_min'])*np.random.rand(n)
        c_d = np.tile(self.limits['c_d_min'], n) + (self.limits['c_d_max'] - self.limits['c_d_min'])*np.random.rand(n)
        A = np.tile(self.limits['A_min'], n) + (self.limits['A_max'] - self.limits['A_min'])*np.random.rand(n)
        
        return np.stack((v_e, m_r, rho, c_d, A), axis=1)
        
    def init_state_gen(self, n):
        p = np.tile(self.limits['p_min'], n) + (self.limits['p_max'] - self.limits['p_min'])*np.random.rand(n)
        v = np.tile(self.limits['v_min'], n) + (self.limits['v_max'] - self.limits['v_min'])*np.random.rand(n)
        m_f = np.tile(self.limits['m_f_min'], n) + (self.limits['m_f_max'] - self.limits['m_f_min'])*np.random.rand(n)
        
        return np.stack((p, v, m_f), axis=1)