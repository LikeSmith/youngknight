"""
MFRMFP.py

This file has all the classes for building an MFRMFP simulator
"""

import numpy as np

from ..core import Simulator, Swarm, ParentChild

class SingleIntegratorSwarm(Swarm):
    def __init__(self, limits, del_t, L=1.0, enforce_limits=False, full_state_feedback=False, disturbance_chance=0.0, ctrl=None, obs=None, rwd=None, done=None, glb_rwd=None, abs_calc=None, aux_calc=None, name='SI_Swarm'):
        self.limits = limits
        self.del_t = del_t
        self.L = L
        self.enforce_limits = enforce_limits
        self.full_state_feedback = full_state_feedback
        self.ctrl = ctrl
        self.obs = obs
        self.rwd = rwd
        self.done = done
        self.abs_calc = abs_calc
        self.aux_calc = aux_calc
        
        if self.obs is None:
            obs_size = (1, )
            obs_dtype = 'float32'
        else:
            s = self.m_init_state_gen(1)
            p = self.m_param_gen(1)
            o = self.m_obs_gen(s, 0, p)
            obs_size = o.shape[1:]
            obs_dtype = o.dtype
        if self.abs_calc is None:
            abs_size = (3,)
            abs_dtype = 'float32'
        else:
            s = self.init_state_gen(1, 1)
            p = self.param_gen(1, 1)
            a = abs_calc(s, 0, p)
            abs_size = a.shape[1:]
            abs_dtype = a.dtype
        if self.aux_calc is None:
            aux_size = (4,)
            aux_dtype = 'float32'
        else:
            s = self.init_state_gen(1, 10)
            p = self.param_gen(1, 10)
            a = aux_calc(s, 0, p)
            aux_size = a.shape[1:]
            aux_dtype = a.dtype
            
        super(SingleIntegratorSwarm, self).__init__(state_size=(2,), action_size=(1,), obs_size=obs_size, abs_size=abs_size, aux_size=aux_size, obs_dtype=obs_dtype, abs_dtype=abs_dtype, aux_dtype=aux_dtype, glb_rwd=glb_rwd, name=name)
        
    def m_step_forward(self, state, action, k, params):
        if self.enforce_limits:
            action = np.clip(action, self.limits['v_min'], self.limits['v_max'])
        state[:, 1] = action
        state[:, 0] = state[:, 0] + self.del_t*action
        
        if self.enforce_limits:
            state[:, 0] = np.clip(state[:, 0], self.limits['p_min'], self.limits['p_max'])
            
        return state
        
    def m_action_gen(self, state, observ, k, params, abs_state, aux_state, des_traj=None):
        if self.ctrl is not None:
            if self.full_state_feedback:
                return self.ctrl(state, k, params, abs_state, aux_state, des_traj)
            else:
                return self.ctrl(observ, k, params, abs_state, aux_state, des_traj)
        else:
            n = state.shape[0]
            a_min = np.array([[self.limits['v_min']]])
            a_max = np.array([[self.limits['v_max']]])
            
            return np.tile(a_min, (n, 1)) + np.tile(a_max-a_min, (n, 1))*np.random.rand(n, 1)
        
    def m_obs_gen(self, state, k, params):
        if self.obs is not None:
            return self.obs(state, k, params)
        else:
            return state[:, 0]
        
    def m_reward_gen(self, state, state1, action, k, params, des_traj=None):
        if self.rwd is not None:
            return self.rwd(state, state1, action, k, params, des_traj)
        else:
            return state[:, 0]**2 + 0.01*action[:, 0]**2
        
    def m_done_gen(self, state, k, params):
        if self.done is not None:
            return self.done(state, k, params)
        else:
            #return np.abs(state[:, 0]) > self.L/2.0
            return np.zeros((state.shape[0], 1,), dtype='bool')
        
    def m_noise_gen(self, state, k, params):
        n = state.shape[0]
        return [np.zeros((n,)+self.state_size), np.zeros((n,)+self.obs_size)]
        
    def m_param_gen(self, n):
        p_min = np.array([self.limits['m_min']])
        p_max = np.array([self.limits['m_max']])
        return np.tile(p_min, (n, 1)) + np.tile(p_max - p_min, (n, 1))*np.random.rand(n, 1)
    
    def m_init_state_gen(self, n):
        x_min = np.array([[self.limits['p_min'], self.limits['v_min']]])
        x_max = np.array([[self.limits['p_max'], self.limits['v_max']]])
        
        return np.tile(x_min, (n, 1)) + np.tile(x_max - x_min, (n, 1))*np.random.rand(n, 2)
    
    def calc_abs_state(self, state, k, params):
        if self.abs_calc is not None:
            return self.abs_calc(state, k, params)
        
        n = state.shape[0]
        N = state.shape[1]
        
        a = np.zeros((n, 3))
        
        for i in range(N):
            a[:, 0] += params[:, i, 0]*state[:, i, 0]
            a[:, 1] += params[:, i, 0]*state[:, i, 0]**2
            a[:, 2] += 2.0*params[:, i, 0]*state[:, i, 0]*state[:, i, 1]
        
        return a
    
    def calc_aux_state(self, state, k, params):
        if self.aux_calc is not None:
            return self.aux_calc(state, k, params)
        
        n = state.shape[0]
        N = state.shape[1]
        
        a = np.zeros((n, 4))
        
        for i in range(N):
            a[:, 0] += params[:, i, 0]**2
            a[:, 1] += params[:, i, 0]**2*state[:, i, 0]
            a[:, 2] += params[:, i, 0]**2*state[:, i, 0]**2
            
            for j in range(i+1, N):
                a[:, 3] += params[:, i, 0]**2*params[:, j, 0]**2*(state[:, i, 0] - state[:, j, 0])**2
                
        return a
    
class DoubleIntegratorSwarm(Swarm):
    def __init__(self, limits, del_t, L=1.0, k_sd=0.0, enforce_limits=False, full_state_feedback=False, ctrl=None, obs=None, rwd=None, done=None, abs_calc=None, aux_calc=None, name='SI_Swarm'):
        self.limits = limits
        self.del_t = del_t
        self.L = L
        self.k_sd = k_sd
        self.enforce_limits = enforce_limits
        self.full_state_feedback = full_state_feedback
        self.ctrl = ctrl
        self.obs = obs
        self.rwd = rwd
        self.done = done
        self.abs_calc = abs_calc
        self.aux_calc = aux_calc
        
        if self.obs is None:
            obs_size = (2, )
            obs_dtype = 'float32'
        else:
            s = self.m_init_state_gen(1)
            p = self.m_param_gen(1)
            o = self.m_obs_gen(s, 0, p)
            obs_size = o.shape[1:]
            obs_dtype = o.dtype
        if self.abs_calc is None:
            abs_size = (3,)
            abs_dtype = 'float32'
        else:
            s = self.init_state_gen(1)
            p = self.param_gen(1)
            a = abs_calc(s, 0, p)
            abs_size = a.shape[1:]
            abs_dtype = a.dtype
        if self.aux_calc is None:
            aux_size = (10,)
            aux_dtype = 'float32'
        else:
            s = self.init_state_gen(1)
            p = self.param_gen(1)
            a = aux_calc(s, 0, p)
            aux_size = a.shape[1:]
            aux_dtype = a.dtype
            
        super(DoubleIntegratorSwarm, self).__init__(state_size=(2,), action_size=(1,), obs_size=obs_size, abs_size=abs_size, aux_size=aux_size, obs_dtype=obs_dtype, abs_dtype=abs_dtype, aux_dtype=aux_dtype, name=name)
        
    def m_step_forward(self, state, action, k, params):
        m = params[:, 0]
        gamma_1 = params[:, 1]
        gamma_2 = params[:, 2]
        gamma_3 = params[:, 3]
        gamma_4 = params[:, 4]
        gamma_5 = params[:, 5]
        gamma_6 = params[:, 6]
        
        p = state[:, 0]
        v = state[:, 1]
        f = action[:, 0]
        
        if self.enforce_limits:
            action = np.clip(action, self.limits['f_min'], self.limits['f_max'])
            
        f_f = gamma_1*(np.tanh(gamma_2*v) - np.tanh(gamma_3*v)) + gamma_4*np.tanh(gamma_5*v) + gamma_6*v
        a = (f - f_f)/m
        v_new = v + self.del_t*a
        p_new = p + self.del_t*v_new
        
        state = np.stack([p_new, v_new], axis=1)
        
        if self.enforce_limits:
            state[:, 0] = np.clip(state[:, 0], self.limits['p_min'], self.limits['p_max'])
            state[:, 1] = np.clip(state[:, 1], self.limits['v_min'], self.limits['v_max'])
            
        return state
        
    def m_action_gen(self, state, observ, k, params, abs_state, aux_state):
        if self.ctrl is not None:
            if self.full_state_feedback:
                return self.ctrl(state, k, params, abs_state, aux_state)
            else:
                return self.ctrl(observ, k, params, abs_state, aux_state)
        else:
            n = state.shape[0]
            a_min = np.array([[self.limits['f_min']]])
            a_max = np.array([[self.limits['f_max']]])
            
            return np.tile(a_min, (n, 1)) + np.tile(a_max-a_min, (n, 1))*np.random.rand(n, 1)
        
    def m_obs_gen(self, state, k, params):
        if self.obs is not None:
            return self.obs(state, k, params)
        else:
            return state
        
    def m_reward_gen(self, state, state1, action, k, params):
        if self.rwd is not None:
            return self.rwd(state, state1, action, k, params)
        else:
            return state[:, 0]**2 + state[:, 1]**2 + 0.01*action[:, 0]**2
        
    def m_done_gen(self, state, k, params):
        if self.done is not None:
            return self.done(state, k, params)
        else:
            #return np.abs(state[:, 0]) > self.L/2.0
            return np.zeros((state.shape[0], 1,), dtype='bool')
        
    def m_noise_gen(self, state, k, params):
        n = state.shape[0]
        return [np.zeros((n,)+self.state_size), np.zeros((n,)+self.obs_size)]
        
    def m_param_gen(self, n):
        p_min = np.array([[self.limits['m_min'], self.limits['gamma_1_min'], self.limits['gamma_2_min'], self.limits['gamma_23_min'], self.limits['gamma_14_min'], self.limits['gamma_5_min'], self.limits['gamma_6_min']]])
        p_max = np.array([[self.limits['m_max'], self.limits['gamma_1_max'], self.limits['gamma_2_max'], self.limits['gamma_23_max'], self.limits['gamma_14_max'], self.limits['gamma_5_max'], self.limits['gamma_6_max']]])
        
        p = np.tile(p_min, (n, 1)) + np.tile(p_max - p_min, (n, 1))*np.random.rand(n, 7)
        
        p[:, 3] = p[:, 2] - p[:, 3]
        p[:, 4] = p[:, 1] - p[:, 4]
        
        return p
    
    def m_init_state_gen(self, n):
        x_min = np.array([[self.limits['p_min'], self.limits['v_min']]])
        x_max = np.array([[self.limits['p_max'], self.limits['v_max']]])
        
        return np.tile(x_min, (n, 1)) + np.tile(x_max - x_min, (n, 1))*np.random.rand(n, 2)
    
    def calc_abs_state(self, state, k, params):
        n = state.shape[0]
        N = state.shape[1]
        
        a = np.zeros((n, 3))
        
        for i in range(N):
            a[:, 0] += params[:, i, 0]*state[:, i, 0]
            a[:, 1] += params[:, i, 0]*state[:, i, 0]**2
            a[:, 2] += 2.0*params[:, i, 0]*state[:, i, 0]*state[:, i, 1]
            
        return a
    
    def calc_aux_state(self, state, k, params):
        if self.aux_calc is not None:
            return self.aux_calc(state, k, params)
        
        n = state.shape[0]
        N = state.shape[1]
        
        a = np.zeros((n, 10))
        
        for i in range(N):
            a[:, 0] += params[:, i, 0]**2
            a[:, 1] += params[:, i, 0]**2*state[:, i, 0]
            a[:, 2] += params[:, i, 0]**2*state[:, i, 0]**2
            
            for j in range(i+1, N):
                a[:, 3] += params[:, i, 0]**2*params[:, j, 0]**2*(state[:, i, 0] - state[:, j, 0])**2
        
        for i in range(N):
            a[:, 4] += params[:, i, 0]*(self.k_sd + params[:, i, 6])*(a[:, 2] - state[:, i, 0]*a[:, 1])
            a[:, 5] += params[:, i, 0]*(self.k_sd + params[:, i, 6])*(state[:, i, 0]*a[:, 0] - a[:, 1])
            a[:, 6] += params[:, i, 0]*state[:, i, 0]*(self.k_sd + params[:, i, 6])*(a[:, 2] - state[:, i, 0]*a[:, 1])
            a[:, 7] += params[:, i, 0]*state[:, i, 0]*(self.k_sd + params[:, i, 6])*(state[:, i, 0]*a[:, 0] - a[:, 1])
            a[:, 8] += params[:, i, 0]*state[:, i, 1]
            a[:, 9] += params[:, i, 0]*state[:, i, 1]**2
        
        return a
    
class PlaneSystem(Simulator):
    def __init__(self, limits, dt=0.01, disturbance=None, enforce_limits=False, full_state_feedback=True, ctrl=None, obs=None, rwd=None, done=None, name='Plane', mode=0):
        self.limits = limits
        self.dt = dt
        self.enforce_limits=enforce_limits
        self.ctrl = ctrl
        self.obs = obs
        self.rwd = rwd
        self.done = done
        self.full_state_feedback = full_state_feedback
        self.mode = mode
        self.disturbance = None
        
        if self.obs is None:
            if self.mode == 3:
                obs_size = (3,)
            else:
                obs_size = (2, )
            obs_dtype = 'float32'
        else:
            s = self.init_state_gen(1)
            p = self.param_gen(1)
            o = self.obs_gen(s, 0, p)
            obs_size = o.shape[1:]
            obs_dtype = o.dtype
            
        action_size = (3,)
        state_size = (2,)
        
        if self.mode == 1:
            action_size = (1,)
        elif self.mode == 2:
            action_size = (2,)
        
        super(PlaneSystem, self).__init__(state_size=state_size, action_size=action_size, obs_size=obs_size, obs_dtype=obs_dtype, name=name)
        
    def step_forward(self, state, action, k, params):
        theta = state[:, 0]
        omega = state[:, 1]
        
        mu = action[:, 0]
        if self.mode == 0:
            J = action[:, 1]
            J_dot = action[:, 2]
        elif self.mode == 1:
            J = np.zeros_like(mu)
            J_dot = np.zeros_like(mu)
        elif self.mode == 2:
            J = action[:, 1]
            J_dot = np.zeros_like(mu)
        
        if self.disturbance is not None:
            mu += self.disturbance(state.shape[0], k)
            
        J_p = params[:, 0]
        gamma_1 = params[:, 1]
        gamma_2 = params[:, 2]
        gamma_3 = params[:, 3]
        gamma_4 = params[:, 4]
        gamma_5 = params[:, 5]
        gamma_6 = params[:, 6]
        
        if self.enforce_limits:
            mu = np.clip(mu, self.limits['mu_min'], self.limits['mu_max'])
            J = np.clip(J, self.limits['J_min'], self.limits['J_max'])
            J_dot = np.clip(J_dot, self.limits['J_dot_min'], self.limits['J_dot_max'])
        
        f_f = gamma_1*(np.tanh(gamma_2*omega) - np.tanh(gamma_3*omega)) + gamma_4*np.tanh(gamma_5*omega) + gamma_6*omega
        
        new_alpha = -(np.cos(theta)*mu + omega*J_dot + f_f)/(J_p + J)
        new_omega = omega + self.dt*new_alpha
        new_theta = theta + self.dt*new_omega
        
        if self.enforce_limits:
            new_omega = np.clip(new_omega, self.limits['omega_min'], self.limits['omega_max'])
            new_theta = np.clip(new_theta, self.limits['theta_min'], self.limits['theta_max'])
            new_omega[np.nonzero(new_theta == self.limits['theta_min'])] = 0.0
            new_omega[np.nonzero(new_theta == self.limits['theta_max'])] = 0.0
        
        if self.mode == 3:
            return np.concatenate((np.expand_dims(new_theta, axis=-1), np.expand_dims(new_omega, axis=-1), np.expand_dims(J, axis=-1)), axis=1)
        else:
            return np.concatenate((np.expand_dims(new_theta, axis=-1), np.expand_dims(new_omega, axis=-1)), axis=1)
    
    def action_gen(self, state, observ, k, params, abs_state=None):   
        if self.ctrl is not None:
            if self.full_state_feedback:
                return self.ctrl(state, k, params, abs_state)
            else:
                return self.ctrl(observ, k, params, abs_state)
        elif self.use_J:
            a_min = np.array([[self.limits['mu_min'], self.limits['J_min'], self.limits['J_dot_min']]])
            a_max = np.array([[self.limits['mu_max'], self.limits['J_max'], self.limits['J_dot_max']]])
        else:
            a_min = np.array([[self.limits['mu_min'],]])
            a_max = np.array([[self.limits['mu_max'],]])
        
        n = state.shape[0]
        return np.tile(a_min, (n, 1)) + np.tile(a_max - a_min, (n, 1))*np.random.rand(n, 1)
        
    def obs_gen(self, state, k, params):
        if self.obs is not None:
            return self.obs(state, k, params)
        else:
            return state
        
    def reward_gen(self, state, state1, action, k, params, des_traj=None):
        if self.rwd is not None:
            return self.rwd(state, state1, action, k, params)
        else:
            cost = state[:, 0]**2 + 0.1*state[:, 1]**2 + 0.001*action[:, 0]**2
            return -cost
        
    def done_gen(self, state, k, params):
        if self.done is not None:
            return self.done(state, k, params)
        else:
            return np.zeros(state.shape[0], dtype='bool')
        
    def noise_gen(self, state, k, params):
        n = state.shape[0]
        return [np.zeros((n,)+self.state_size), np.zeros((n,)+self.obs_size)]
        
    def param_gen(self, n):
        p_min = np.array([[self.limits['J_p_min'], self.limits['gamma_1_min'], self.limits['gamma_2_min'], self.limits['gamma_23_min'], self.limits['gamma_14_min'], self.limits['gamma_5_min'], self.limits['gamma_6_min']]])
        p_max = np.array([[self.limits['J_p_max'], self.limits['gamma_1_max'], self.limits['gamma_2_max'], self.limits['gamma_23_max'], self.limits['gamma_14_max'], self.limits['gamma_5_max'], self.limits['gamma_6_max']]])
        p = np.tile(p_min, (n, 1)) + np.tile(p_max - p_min, (n, 1))*np.random.rand(n, 7)
        
        p[:, 3] = p[:, 2] - p[:, 3]
        p[:, 4] = p[:, 1] - p[:, 4]
        
        return p
    
    def init_state_gen(self, n):
        if self.mode == 3:
            s_min = np.array([[self.limits['theta_min'], self.limits['omega_min'], self.limits['J_min']]])
            s_max = np.array([[self.limits['theta_max'], self.limits['omega_max'], self.limits['J_max']]])
        else:
            s_min = np.array([[self.limits['theta_min'], self.limits['omega_min']]])
            s_max = np.array([[self.limits['theta_max'], self.limits['omega_max']]])
        
        return np.tile(s_min, (n, 1)) + np.tile(s_max - s_min, (n, 1))*np.random.rand(n, self.state_size[0])
    
class MFRMFP(ParentChild):
    def __init__(self, parent_limits, child_limits, swarm_size, dt=0.01, L=1.0, use_double_int=False, k_sd=1.0, enforce_limits=False, parent_full_state_feedback=True, child_full_state_feedback=True, abs_calc=None, parent_ctrl=None, child_ctrl=None, parent_obs=None, child_obs=None, parent_rwd=None, child_rwd=None, parent_done=None, child_done=None, name='MFRMFP'):
        parent_sim = PlaneSystem(limits=parent_limits, dt=dt, enforce_limits=enforce_limits, full_state_feedback=parent_full_state_feedback, ctrl=parent_ctrl, obs=parent_obs, rwd=parent_rwd, done=parent_done, name='%s_Plane'%(name,))
        if use_double_int:
            child_sim = DoubleIntegratorSwarm(swarm_size=swarm_size, limits=child_limits, del_t=dt, L=L, k_sd=k_sd, enforce_limits=enforce_limits, full_state_feedback=child_full_state_feedback, ctrl=child_ctrl, obs=child_obs, rwd=child_rwd, done=child_done, name='%s_SISwarm'%(name,))
        else:
            child_sim = SingleIntegratorSwarm(swarm_size=swarm_size, limits=child_limits, del_t=dt, L=L, enforce_limits=enforce_limits, full_state_feedback=child_full_state_feedback, ctrl=child_ctrl, obs=child_obs, rwd=child_rwd, done=child_done, name='%s_SISwarm'%(name,))
        
        if abs_calc is None:
            abs_calc = child_sim.calc_abs_state
        
        super(MFRMFP, self).__init__(parent_sim=parent_sim, child_sim=child_sim, abs_calc=abs_calc, name=name)
    