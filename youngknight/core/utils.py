'''
youngknight utilities
'''

import numpy as np
import tensorflow as tf

def quad(x, A):
    x1 = np.matmul(x, A)
    return np.einsum('ij, ij->i', x1, x)

def rand_vectors(n, min_v, max_v):
    if not isinstance(n, list) and not isinstance(n, tuple):
        n = (n,)

    v = []    
    if len(n) == 1:
        for min_el, max_el in zip(min_v, max_v):
            v.append(min_el + (max_el - min_el)*np.random.rand(n[0]))
        return np.stack(v, axis=1)
    else:
        for i in range(n[-1]):
            v.append(rand_vectors(n[:-1], min_v, max_v))
        return np.stack(v, axis=1)
