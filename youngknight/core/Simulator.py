"""
Simulator

This is the base class for simulator objects and subclasses for various
simulators.  Child classes must provide the following member functions:
    step_forward:     function for stepping the network forward
    input_gen:        function that generates inputs given a state
    obs_gen:          function that generates an observation given a state
    reward_gen:       function that generates a reward value
    done_gen:         function that tells if the system is done executing
    noise_gen:        function generate process and signal noise
    param_gen:        function that generates parameter sets
    init_state_gen:   function that generates initial states
    reset:            (optional) function resets simulation (for time varying systems)
    tf_step_forwared: function that returns tensorflow operations for stepping forward
"""

import numpy as np
import tensorflow as tf
from tqdm import tqdm

class Simulator(object):
    def __init__(self, state_size, action_size, obs_size, state_dtype='float32', action_dtype='float32', obs_dtype='float32', name='sim'):
        self.state_size = state_size
        self.action_size = action_size
        self.obs_size = obs_size
        self.state_dtype = state_dtype
        self.action_dtype = action_dtype
        self.obs_dtype = obs_dtype
        self.name=name
        
    def step_forward(self, state, action, k, params):
        raise NotImplementedError
        
    def action_gen(self, state, observ, k, params, des_traj=None):
        raise NotImplementedError
        
    def obs_gen(self, state, k, params):
        raise NotImplementedError
        
    def reward_gen(self, state, state1, action, k, params, des_traj=None):
        raise NotImplementedError
        
    def done_gen(self, state, k, params):
        raise NotImplementedError
        
    def noise_gen(self, state, k, params):
        raise NotImplementedError
        
    def param_gen(self, n):
        raise NotImplementedError
        
    def init_state_gen(self, n):
        raise NotImplementedError
        
    def reset(self, init_state, params):
        pass
        
    def step(self, state, action, k=0, params=None, use_noise=False, des_traj=None):
        state1 = self.step_forward(state, action, k, params)
        
        if use_noise:
            process_noise, sensor_noise = self.noise_gen(state, k, params)
            state1 += process_noise
            
        obs1 = self.obs_gen(state1, k, params)
        if use_noise:
            obs1 += sensor_noise
        
        rew1 = self.reward_gen(state, state1, action, k, params, des_traj)
        done1 = self.done_gen(state1, k, params)
        
        return [state1, obs1, rew1, done1]

    def gen_trajectories(self, initial_states, horizon, params=None, use_noise=False, verbosity=0, des_traj=None):
        self.reset(initial_states, params)

        n = initial_states.shape[0]

        states = np.zeros((n, horizon+1) + self.state_size, dtype=self.state_dtype)
        actions = np.zeros((n, horizon) + self.action_size, dtype=self.action_dtype)
        observ = np.zeros((n, horizon+1) + self.obs_size, dtype=self.obs_dtype)
        rewards = np.zeros((n, horizon))
        dones = np.zeros((n, horizon), dtype='bool')

        results = {}
        results['states'] = states
        results['actions'] = actions
        results['observ'] = observ
        results['rewards'] = rewards
        results['dones'] = dones

        if params is None:
            params = self.param_gen(n)

        states[:, 0] = initial_states
        observ[:, 0] = self.obs_gen(states[:, 0], 0, params)

        if verbosity >= 1:
            wrapper = tqdm
        else:
            wrapper = lambda x: x
            
        for i in wrapper(range(horizon)):
            actions[:, i] = self.action_gen(states[:, i], observ[:, i], i, params, des_traj)
            states[:, i+1], observ[:, i+1], rewards[:, i], dones[:, i] = self.step(states[:, i], actions[:, i], i, params, use_noise, des_traj)

        return [results, params]
    
class Swarm(Simulator):
    def __init__(self, state_size, action_size, obs_size, abs_size=None, aux_size=None, state_dtype='float32', action_dtype='float32', obs_dtype='float32', abs_dtype='float32', aux_dtype='float32', glb_rwd=None, name='swarm'):
        self.abs_size = abs_size
        self.aux_size = aux_size
        self.abs_dtype = abs_dtype
        self.aux_dtype = aux_dtype
        self.glb_rwd = glb_rwd
        
        super(Swarm, self).__init__(state_size=state_size, action_size=action_size, obs_size=obs_size, state_dtype=state_dtype, action_dtype=action_dtype, obs_dtype=obs_dtype, name=name)
    
    def m_step_forward(self, state, action, k, params):
        raise NotImplementedError
        
    def m_action_gen(self, state, observ, k, params, abs_state=None, aux_state=None, des_traj=None):
        raise NotImplementedError
        
    def m_obs_gen(self, state, k, params):
        raise NotImplementedError
        
    def m_reward_gen(self, state, state1, action, k, params, des_traj=None):
        raise NotImplementedError
        
    def m_done_gen(self, state, k, params):
        raise NotImplementedError
        
    def m_noise_gen(self, state, k, params):
        raise NotImplementedError
        
    def m_param_gen(self, n):
        raise NotImplementedError
        
    def m_init_state_gen(self, n):
        raise NotImplementedError
        
    def calc_abs_state(self, state, k, params):
        raise NotImplementedError
        
    def calc_aux_state(self, state, k, params):
        raise NotImplementedError
        
    def reset(self, init_state, params):
        pass
        
    def step_forward(self, state, action, k, params):
        n = state.shape[0]
        swarm_size = state.shape[1]
        state1 = np.zeros((n,swarm_size)+self.state_size, dtype=self.state_dtype)
        for i in range(swarm_size):
            state1[:, i] = self.m_step_forward(state[:, i], action[:, i], k, params[:, i])
        return state1
        
    def action_gen(self, state, observ, k, params, des_traj=None):
        n = state.shape[0]
        swarm_size = state.shape[1]
        action = np.zeros((n, swarm_size)+self.action_size, dtype=self.action_dtype)
        
        if self.abs_size is not None:
            abs_state = self.calc_abs_state(state, k, params)
            if self.aux_size is not None:
                aux_state = self.calc_aux_state(state, k, params)
                for i in range(swarm_size):
                    action[:, i] = self.m_action_gen(state[:, i], observ[:, i], k, params[:, i], abs_state, aux_state, des_traj)
            else:
                for i in range(swarm_size):
                    action[:, i] = self.m_action_gen(state[:, i], observ[:, i], k, params[:, i], abs_state, des_traj)
        else:
            for i in range(swarm_size):
                action[:, i] = self.m_action_gen(state, observ, i, k, params, des_traj)
                
        return action
        
    def obs_gen(self, state, k, params):
        n = state.shape[0]
        swarm_size = state.shape[1]
        obs = np.zeros((n, swarm_size)+self.obs_size, dtype=self.obs_dtype)
        for i in range(swarm_size):
            obs[:, i] = self.m_obs_gen(state[:, i], k, params[:, i])
        return obs
        
    def reward_gen(self, state, state1, action, k, params, des_traj=None):
        n = state.shape[0]
        
        if self.glb_rwd is not None:
            if self.abs_size is not None:
                r_glb = self.glb_rwd(self.calc_abs_state(state, k, params), self.calc_abs_state(state1, k+1, params), action, k, params, des_traj)
            else:
                r_glb =  self.glb_rwd(state, state1, action, k, params, des_traj)
        else:
            r_glb = np.zeros(n)
            
        swarm_size = state.shape[1]
        r = np.zeros(n)
        for i in range(swarm_size):
            r += self.m_reward_gen(state[:, i], state1[:, i], action[:, i], k, params[:, i], des_traj)
        
        return r_glb + r
        
    def done_gen(self, state, k, params):
        n = state.shape[0]
        swarm_size = state.shape[1]
        d = np.zeros(n, dtype='bool')
        for i in range(swarm_size):
            d = d or self.m_done_gen(state[:, i], k, params[:, i])
        return d
        
    def noise_gen(self, state, k, params):
        n = state.shape[0]
        swarm_size = state.shape[1]
        p_noise = np.zeros((n, swarm_size)+self.state_size, dtype=self.state_dtype)
        m_noise = np.zeros((n, swarm_size)+self.obs_size, dtype=self.obs_dtype)
        for i in range(swarm_size):
            p_noise[:, i], m_noise[:, i] = self.m_noise_gen(state[:, i], k, params)
        return [p_noise, m_noise]
        
    def param_gen(self, n, swarm_size):
        p = []
        for i in range(swarm_size):
            p.append(np.expand_dims(self.m_param_gen(n), axis=1))
        p = np.concatenate(p, axis=1)
        
        return p
        
    def init_state_gen(self, n, swarm_size):
        s_0 = []
        for i in range(swarm_size):
            s_0.append(np.expand_dims(self.m_init_state_gen(n), axis=1))
        return np.concatenate(s_0, axis=1)
    
    def gen_trajectories(self, initial_states, horizon, params=None, use_noise=False, verbosity=0, des_traj=None):
        self.reset(initial_states, params)

        n = initial_states.shape[0]
        swarm_size = initial_states.shape[1]

        states = np.zeros((n, horizon+1, swarm_size) + self.state_size, dtype=self.state_dtype)
        actions = np.zeros((n, horizon, swarm_size) + self.action_size, dtype=self.action_dtype)
        observ = np.zeros((n, horizon+1, swarm_size) + self.obs_size, dtype=self.obs_dtype)
        rewards = np.zeros((n, horizon))
        dones = np.zeros((n, horizon), dtype='bool')

        results = {}
        results['states'] = states
        results['actions'] = actions
        results['observ'] = observ
        results['rewards'] = rewards
        results['dones'] = dones
        
        if self.abs_size is not None:
            abs_states = np.zeros((n, horizon+1) + self.abs_size, dtype=self.abs_dtype)
            results['abs_states'] = abs_states
        
        if self.aux_size is not None:
            aux_states = np.zeros((n, horizon+1) + self.aux_size, dtype=self.aux_dtype)
            results['aux_states'] = aux_states
            
        if params is None:
            params = self.param_gen(n)

        states[:, 0] = initial_states
        observ[:, 0] = self.obs_gen(states[:, 0], 0, params)

        if self.abs_size is not None:
            abs_states[:, 0] = self.calc_abs_state(states[:, 0], 0, params)
        if self.aux_size is not None:
            aux_states[:, 0] = self.calc_aux_state(states[:, 0], 0, params)
            
        if verbosity >= 1:
            wrapper = tqdm
        else:
            wrapper = lambda x: x
            
        for i in wrapper(range(horizon)):
            actions[:, i] = self.action_gen(states[:, i], observ[:, i], i, params, des_traj)
            states[:, i+1], observ[:, i+1], rewards[:, i], dones[:, i] = self.step(states[:, i], actions[:, i], i, params, use_noise, des_traj)
            if self.abs_size is not None:
                abs_states[:, i+1] = self.calc_abs_state(states[:, i+1], i+1, params)
            if self.aux_size is not None:
                aux_states[:, i+1] = self.calc_aux_state(states[:, i+1], i+1, params)
                

        return [results, params]
    
class ParentChild(object):
    def __init__(self, parent_sim, child_sim, abs_calc=None, name='ParentChild'):
        self.parent_sim = parent_sim
        self.child_sim = child_sim
        self.abs_calc = abs_calc
        self.name = name
        self.state_size = [self.parent_sim.state_size, self.child_sim.state_size]
        self.obs_size = [self.parent_sim.obs_size, self.child_sim.obs_size]
        self.action_size = self.child_sim.action_size
        
    def parent_init_state_gen(self, n):
        return self.parent_sim.init_state_gen(n)
    
    def child_init_state_gen(self, n):
        return self.child_sim.init_state_gen(n)
    
    def parent_param_gen(self, n):
        return self.parent_sim.param_gen(n)
    
    def child_param_gen(self, n):
        return self.child_sim.param_gen(n)
    
    def param_gen(self, n):
        return None
        
    def step(self, parent_state, child_state, action, k=0, parent_params=None, child_params=None, use_noise=False):
        if self.abs_calc is not None:
            abs_state = self.abs_calc(child_state, k, child_params)
            parent_state1 = self.parent_sim.step_forward(parent_state, abs_state, k, parent_params)
        else:
            parent_state1 = self.parent_sim.step_forward(parent_state, child_state, k, [parent_params, child_params])
            
        child_state1 = self.child_sim.step_forward(child_state, action, k, child_params)
        
        if use_noise:
            parent_process_noise, parent_sensor_noise = self.parent_state.noise_gen(parent_state, k, parent_params)
            child_process_noise, child_sensor_noise = self.child_state.noise_gen(child_state, k, child_params)
            
            parent_state1 += parent_process_noise
            child_state1 += child_process_noise
            
        parent_obs1 = self.parent_sim.obs_gen(parent_state1, k, parent_params)
        child_obs1 = self.child_sim.obs_gen(child_state1, k, child_params)
        
        if use_noise:
            parent_obs1 += parent_sensor_noise
            child_obs1 += child_sensor_noise
            
        rew1 = self.child_sim.reward_gen(child_state, child_state1, action, k, child_params)
        if self.abs_calc is not None:
            rew1 += self.parent_sim.reward_gen(parent_state, parent_state1, abs_state, k, parent_params)
        else:
            rew1 += self.parent_sim.reward_gen(parent_state, parent_state1, child_state, k, parent_params)
        done1 = self.parent_sim.done_gen(parent_state1, k, parent_params) + self.child_sim.done_gen(child_state1, k, child_params)
        
        return [parent_state1, child_state1, parent_obs1, child_obs1, rew1, done1]
        
    def gen_trajectories(self, initial_parent_states, initial_child_states, horizon, parent_params=None, child_params=None, use_noise=False, verbosity=0):
        self.parent_sim.reset(initial_parent_states, parent_params)
        self.child_sim.reset(initial_child_states, child_params)

        n = initial_parent_states.shape[0]

        parent_states = np.zeros((n, horizon+1) + self.parent_sim.state_size, dtype=self.parent_sim.state_dtype)
        child_states = np.zeros((n, horizon+1) + self.child_sim.state_size, dtype=self.child_sim.state_dtype)
        actions = np.zeros((n, horizon) + self.child_sim.action_size, dtype=self.child_sim.action_dtype)
        parent_obs = np.zeros((n, horizon+1) + self.parent_sim.obs_size, dtype=self.parent_sim.obs_dtype)
        child_obs = np.zeros((n, horizon+1) + self.child_sim.obs_size, dtype=self.child_sim.obs_dtype)
        rewards = np.zeros((n, horizon))
        dones = np.zeros((n, horizon), dtype='bool')

        results = {}
        results['parent_states'] = parent_states
        results['child_states'] = child_states
        results['actions'] = actions
        results['parent_obs'] = parent_obs
        results['child_obs'] = child_obs
        results['rewards'] = rewards
        results['dones'] = dones

        if parent_params is None:
            parent_params = self.parent_sim.param_gen(n)
        if child_params is None:
            child_params = self.child_sim.param_gen(n)

        parent_states[:, 0] = initial_parent_states
        child_states[:, 0] = initial_child_states
        
        parent_obs[:, 0] = self.parent_sim.obs_gen(parent_states[:, 0], 0, parent_params)
        child_obs[:, 0] = self.child_sim.obs_gen(child_states[:, 0], 0, child_params)

        if verbosity >= 1:
            wrapper = tqdm
        else:
            wrapper = lambda x: x
            
        for i in wrapper(range(horizon)):
            if self.abs_calc is not None:
                abs_state = self.abs_calc(child_states[:, i], i, child_params)
                des_child_state = self.parent_sim.action_gen(parent_states[:, i], parent_obs[:, i], i, parent_params, abs_state)
            else:
                des_child_state = self.parent_sim.action_gen(parent_states[:, i], parent_obs[:, i], i, parent_params, child_states[:, i])
            
            actions[:, i] = self.child_sim.action_gen(child_states[:, i], child_obs[:, i], i, child_params, des_state=des_child_state)
            parent_states[:, i+1], child_states[:, i+1], parent_obs[:, i+1], child_obs[:, i+1], rewards[:, i], dones[:, i] = self.step(parent_states[:, i], child_states[:, i], actions[:, i], i, parent_params, child_params, use_noise)

        return [results, parent_params, child_params]
        
'''
class Continuous_Simulator(Simulator):
    def __init__(self, state_size, input_size, obs_size, int_method='Euler', dt=0.1, name='cont_sim'):
        self.int_method = int_method
        self.dt = dt
        
        super(Continuous_Simulator, self).__init__(state_size, input_size, obs_size, name)
    
    def f_cont(self, state, action, t, params):
        raise NotImplementedError
        
    def f_cont_tf(self, state, action, t, params):
        raise NotImplementedError
        
    def step_forward(self, state, action, params):
        
    def euler(self, state, action, params):
'''