"""
DataManager

Base Model for managing datasets
method load_data_params should include paramet
"""

import numpy as np
import os
import _pickle as pkl

class DataManager(object):
    def __init__(self, index, home, param_dir='params/', data_dir='data/'):
        self.home = home
        self.param_dir = param_dir
        self.data_dir = data_dir
        self.index = index

        self.load_data_params(index)
        self.gen_sim()

        self.data_avail = False
        self.shuffled = False
        self.sassy = None
        self.sassy_shuffled = False

    def load_data_params(self, index):
        raise NotImplementedError

    def gen_sim(self):
        raise NotImplementedError

    def gen_description(self):
        raise NotImplementedError

    def gen_trajectories(self):
        self.trajectories, self.params = self.sim.gen_trajectories(self.init_states, self.horizon, params=self.params, use_noise=self.use_noise)

    def gen_datasets(self, n, horizon, init_states=None, use_noise=False, val=0.2, test=0.1, stack_front=1, stack_back=0, shuffle=False, description=None):
        self.n = n
        self.horizon = horizon
        self.use_noise = use_noise
        self.stack_front = stack_front
        self.stack_back = stack_back

        if description is None:
            description = self.gen_description()

        self.description = description

        if init_states is None:
            init_states = self.sim.init_state_gen(self.n)

        self.init_states = init_states
        self.gen_trajectories()
        self.sassify()

        self.data_avail = True

        self.split_data(val, test, shuffle)
        self.sassy_split(val, test, shuffle)

    def load_dataset(self, val=0.2, test=0.1, shuffle=False):
        self.n, self.horizon, self.use_noise, self.description, self.init_states, self.trajectories, self.params, self.sassy, self.stack_front, self.stack_back, self.p_inv, self.sassy_p_inv, self.cutoff_train, self.cutoff_val, self.cutoff_train_sassy, self.cutoff_val_sassy = pkl.load(open(self.home+self.data_dir+'dataset_%s.pkl'%self.index, 'rb'))

        self.data_avail = True

        self.split_data(val, test, shuffle)

    def autoload(self, n, horizon, init_states=None, use_noise=False, val=0.2, test=0.1, stack_front=1, stack_back=0, shuffle=False, description=None):
        if self.check_dataset():
            self.load_dataset(val, test, shuffle)
            if self.n == n and self.horizon == horizon and self.use_noise == use_noise:
                return False

        self.gen_datasets(n, horizon, init_states, use_noise, val, test, stack_front, stack_back, shuffle, description)
        return True

    def check_dataset(self):
        return os.path.isfile(self.home+self.data_dir+'dataset_%s.pkl'%self.index)

    def save(self):
        assert self.data_avail
        os.makedirs(self.home+self.data_dir, exist_ok=True)
        pkl.dump([self.n, self.horizon, self.use_noise, self.description, self.init_states, self.trajectories, self.params, self.sassy, self.stack_front, self.stack_back, self.p_inv, self.sassy_p_inv, self.cutoff_train, self.cutoff_val, self.cutoff_train_sassy, self.cutoff_val_sassy], open(self.home+self.data_dir+'dataset_%s.pkl'%self.index, 'wb'))

    def split_data(self, val, test, shuffle=False):
        assert self.data_avail
        self.cutoff_train = int(self.n*(1 - val - test))
        self.cutoff_val = int(self.n*(1 - test))

        if shuffle:
            self.shuffle()
        else:
            self.p_inv = np.arange(self.n)

    def shuffle(self):
        assert self.data_avail and not self.shuffled
        p = np.random.permutation(self.n)
        self.p_inv = np.zeros(self.n)
        for i in range(self.n):
            self.p_inv[p[i]] = i

        for i in range(self.n):
            tmp_init_state = np.copy(self.init_state[i])
            tmp_param = np.copy(self.params[i])
            tmp_traj = {}
            for key in self.trajectories.keys():
                tmp_traj[key] = np.copy[self.trajectories[key][i]]

            j=i
            while True:
                k = p[j]
                p[j] = j
                if k == i:
                    break
                self.init_state[j] = self.init_state[k]
                self.params[j] = self.params[k]
                for key in self.trajectories.keys():
                    self.trajectories[key][j] = self.trajectories[k]

                j=k
            self.init_state[j] = tmp_init_state
            self.params[j] = tmp_param
            for key in self.trajectories.keys():
                self.trajectories[key][j] = tmp_traj[key]

        self.shuffled = True

    def unshuffle(self):
        assert self.data_avail and self.shuffled
        for i in range(self.n):
            tmp_init_state = np.copy(self.init_state[i])
            tmp_param = np.copy(self.params[i])
            tmp_traj = {}
            for key in self.trajectories.keys():
                tmp_traj[key] = np.copy[self.trajectories[key][i]]

            j=i
            while True:
                k = self.p_inv[j]
                self.p_inv[j] = j
                if k == i:
                    break
                self.init_state[j] = self.init_state[k]
                self.params[j] = self.params[k]
                for key in self.trajectories.keys():
                    self.trajectories[key][j] = self.trajectories[k]

                j=k
            self.init_state[j] = tmp_init_state
            self.params[j] = tmp_param
            for key in self.trajectories.keys():
                self.trajectories[key][j] = tmp_traj[key]

        self.shuffled = False


    def sassify(self):
        n_per_traj = self.horizon - self.stack_back - self.stack_front+1
        n_data = self.n*n_per_traj

        self.sassy = np.zeros((n_data, 2), dtype='int')

        for i in range(self.n):
            self.sassy[i*n_per_traj:(i+1)*n_per_traj, 0] = np.ones(n_per_traj)*i
            self.sassy[i*n_per_traj:(i+1)*n_per_traj, 1] = np.arange(n_per_traj)+self.stack_back

    def sassy_split(self, val, test, shuffle=False):
        assert self.data_avail
        self.cutoff_train_sassy = int(self.sassy.shape[0]*(1-val-test))
        self.cutoff_val_sassy = int(self.sassy.shape[0]*(1-test))

        if shuffle:
            self.sassy_suffle()
        else:
            self.sassy_p_inv = np.arange(self.sassy.shape[0])

    def sassy_shuffle(self):
        assert self.data_avail and not self.sassy_shuffled
        n_data = self.sassy.shape[0]

        p = np.random.permutation(n_data)
        self.sassy_p_inv = np.zeros(self.n)

        for i in range(n_data):
            self.sassy_p_inv[p[i]] = i

        for i in range(n_data):
            tmp = np.copy[self.sassy[i, :]]

            j=i
            while True:
                k = p[j]
                p[j] = j
                if k == i:
                    break
                self.sassy[j, :] = self.sassy[k, :]
                j=k

            self.sassy[j, :] = tmp

        self.sassy_shuffled = True

    def sassy_unshuffle(self):
        assert self.data_avail and self.sassy_shuffled
        n_data = self.sassy.shape[0]

        for i in range(n_data):
            tmp = np.copy[self.sassy[i, :]]

            j=i
            while True:
                k = self.sassy_p_inv[j]
                self.sassy_p_inv[j] = j
                if k == i:
                    break
                self.sassy[j, :] = self.sassy[k, :]
                j=k

            self.sassy[j, :] = tmp

        self.sassy_shuffled = False

    def keys(self):
        assert self.data_avail
        keys = ['init_state', 'params']
        for key in self.trajectories.keys():
            keys += [key]
            keys += ['sassy_'+key]

        return keys

    def subsets(self):
        assert self.data_avail
        return ['full', 'train', 'val', 'test']

    def __getitem__(self, key):
        assert self.data_avail
        if key in self.subsets():
            return self.DataSubset(self, key)
        elif key in self.keys():
            ds = self.DataSubset(self)
            return ds[key]
        else:
            raise KeyError

    @staticmethod
    def uniform_dist(n, lims):
        m = lims[0].shape[0]
        x_min = np.tile(lims[0].reshape(1, m), (n, 1))
        x_max = np.tile(lims[1].reshape(1, m), (n, 1))

        return x_min + (x_max - x_min)*np.random.rand(n, m)

    class DataSubset(object):
        def __init__ (self, dm, subset='full'):
            self.dm = dm
            self.subset = subset

        def __getitem__(self, key):
            if self.subset == 'full':
                if key == 'init_state':
                    return self.dm.init_state
                elif key == 'params':
                    return self.dm.params
                elif key == 'sassy_states':
                    ret = np.zeros((self.dm.sassy.shape[0], self.dm.stack_back+self.dm.stack_front+1)+self.dm.sim.state_size, dtype=self.dm.sim.state_dtype)
                    for i in range(self.dm.sassy.shape[0]):
                        for j in range(self.dm.stack_back+self.dm.stack_front+1):
                            ret[i, j,] = self.dm.trajectories['states'][self.dm.sassy[i, 0], self.dm.sassy[i, 1]-self.dm.stack_back+j]
                    return ret
                elif key == 'sassy_observ':
                    ret = np.zeros((self.dm.sassy.shape[0], self.dm.stack_back+self.dm.stack_front+1)+self.dm.sim.obs_size, dtype=self.dm.sim.obs_dtype)
                    for i in range(self.dm.sassy.shape[0]):
                        for j in range(self.dm.stack_back+self.dm.stack_front+1):
                            ret[i, j,] = self.dm.trajectories['observ'][self.dm.sassy[i, 0], self.dm.sassy[i, 1]-self.dm.stack_back+j]
                    return ret
                elif key == 'sassy_actions':
                    ret = np.zeros((self.dm.sassy.shape[0],)+self.dm.sim.action_size, dtype=self.dm.sim.action_dtype)
                    for i in range(self.dm.sassy.shape[0]):
                        ret[i] = self.dm.trajectories['actions'][self.dm.sassy[i, 0], self.dm.sassy[i, 1]]
                    return ret
                elif key == 'sassy_rewards':
                    ret = np.zeros(self.dm.sassy.shape[0])
                    for i in range(self.dm.sassy.shape[0]):
                        ret[i] = self.dm.trajectories['rewards'][self.dm.sassy[i, 0], self.dm.sassy[i, 1]]
                    return ret
                elif key == 'sassy_dones':
                    ret = np.zeros(self.dm.sassy.shape[0], dtype='bool')
                    for i in range(self.dm.sassy.shape[0]):
                        ret[i] = self.dm.trajectories['dones'][self.dm.sassy[i, 0], self.dm.sassy[i, 1]]
                    return ret
                else:
                    return self.dm.trajectories[key]
            elif self.subset == 'train':
                if key == 'init_state':
                    return self.dm.init_state[:self.dm.cutoff_train]
                elif key == 'params':
                    return self.dm.params[:self.dm.cutoff_train]
                elif key == 'sassy_states':
                    dat_ref = self.dm.sassy[:self.dm.cutoff_train_sassy]
                    ret = np.zeros((dat_ref.shape[0], self.dm.stack_back+self.dm.stack_front+1)+self.dm.sim.state_size, dtype=self.dm.sim.state_dtype)
                    for i in range(dat_ref.shape[0]):
                        for j in range(self.dm.stack_back+self.dm.stack_front+1):
                            ret[i, j,] = self.dm.trajectories['states'][dat_ref[i, 0], dat_ref[i, 1]-self.dm.stack_back+j]
                    return ret
                elif key == 'sassy_observ':
                    dat_ref = self.dm.sassy[:self.dm.cutoff_train_sassy]
                    ret = np.zeros((dat_ref.shape[0], self.dm.stack_back+self.dm.stack_front+1)+self.dm.sim.obs_size, dtype=self.dm.sim.obs_dtype)
                    for i in range(dat_ref.shape[0]):
                        for j in range(self.dm.stack_back+self.dm.stack_front+1):
                            ret[i, j,] = self.dm.trajectories['observ'][dat_ref[i, 0], dat_ref[i, 1]-self.dm.stack_back+j]
                    return ret
                elif key == 'sassy_actions':
                    dat_ref = self.dm.sassy[:self.dm.cutoff_train_sassy]
                    ret = np.zeros((dat_ref.shape[0],)+self.dm.sim.action_size, dtype=self.dm.sim.action_dtype)
                    for i in range(dat_ref.shape[0]):
                        ret[i] = self.dm.trajectories['actions'][dat_ref[i, 0], dat_ref[i, 1]]
                    return ret
                elif key == 'sassy_rewards':
                    dat_ref = self.dm.sassy[:self.dm.cutoff_train_sassy]
                    ret = np.zeros(dat_ref.shape[0])
                    for i in range(dat_ref.shape[0]):
                        ret[i] = self.dm.trajectories['rewards'][dat_ref[i, 0], dat_ref[i, 1]]
                    return ret
                elif key == 'sassy_dones':
                    dat_ref = self.dm.sassy[:self.dm.cutoff_train_sassy]
                    ret = np.zeros(dat_ref.shape[0], dtype='bool')
                    for i in range(dat_ref.shape[0]):
                        ret[i] = self.dm.trajectories['dones'][dat_ref[i, 0], dat_ref[i, 1]]
                    return ret
                else:
                    return self.dm.trajectories[key][:self.dm.cutoff_train]
            elif self.subset == 'val':
                if key == 'init_state':
                    return self.dm.init_state[self.dm.cutoff_train:self.dm.cutoff_val]
                elif key == 'params':
                    return self.dm.params[self.dm.cutoff_train:self.dm.cutoff_val]
                elif key == 'sassy_states':
                    dat_ref = self.dm.sassy[self.dm.cutoff_train_sassy:self.dm.cutoff_val_sassy]
                    ret = np.zeros((dat_ref.shape[0], self.dm.stack_back+self.dm.stack_front+1)+self.dm.sim.state_size, dtype=self.dm.sim.state_dtype)
                    for i in range(dat_ref.shape[0]):
                        for j in range(self.dm.stack_back+self.dm.stack_front+1):
                            ret[i, j,] = self.dm.trajectories['states'][dat_ref[i, 0], dat_ref[i, 1]-self.dm.stack_back+j]
                    return ret
                elif key == 'sassy_observ':
                    dat_ref = self.dm.sassy[self.dm.cutoff_train_sassy:self.dm.cutoff_val_sassy]
                    ret = np.zeros((dat_ref.shape[0], self.dm.stack_back+self.dm.stack_front+1)+self.dm.sim.obs_size, dtype=self.dm.sim.obs_dtype)
                    for i in range(dat_ref.shape[0]):
                        for j in range(self.dm.stack_back+self.dm.stack_front+1):
                            ret[i, j,] = self.dm.trajectories['observ'][dat_ref[i, 0], dat_ref[i, 1]-self.dm.stack_back+j]
                    return ret
                elif key == 'sassy_actions':
                    dat_ref = self.dm.sassy[self.dm.cutoff_train_sassy:self.dm.cutoff_val_sassy]
                    ret = np.zeros((dat_ref.shape[0],)+self.dm.sim.action_size, dtype=self.dm.sim.action_dtype)
                    for i in range(dat_ref.shape[0]):
                        ret[i] = self.dm.trajectories['actions'][dat_ref[i, 0], dat_ref[i, 1]]
                    return ret
                elif key == 'sassy_rewards':
                    dat_ref = self.dm.sassy[self.dm.cutoff_train_sassy:self.dm.cutoff_val_sassy]
                    ret = np.zeros(dat_ref.shape[0])
                    for i in range(dat_ref.shape[0]):
                        ret[i] = self.dm.trajectories['rewards'][dat_ref[i, 0], dat_ref[i, 1]]
                    return ret
                elif key == 'sassy_dones':
                    dat_ref = self.dm.sassy[self.dm.cutoff_train_sassy:self.dm.cutoff_val_sassy]
                    ret = np.zeros(dat_ref.shape[0], dtype='bool')
                    for i in range(dat_ref.shape[0]):
                        ret[i] = self.dm.trajectories['dones'][dat_ref[i, 0], dat_ref[i, 1]]
                    return ret
                else:
                    return self.dm.trajectories[key][self.dm.cutoff_train:self.dm.cutoff_val]
            elif self.subset == 'test':
                if key == 'init_state':
                    return self.dm.init_state[self.dm.cutoff_val:]
                elif key == 'params':
                    return self.dm.params[self.dm.cutoff_val:]
                elif key == 'sassy_states':
                    dat_ref = self.dm.sassy[self.dm.cutoff_val_sassy:]
                    ret = np.zeros((dat_ref.shape[0], self.dm.stack_back+self.dm.stack_front+1)+self.dm.sim.state_size, dtype=self.dm.sim.state_dtype)
                    for i in range(dat_ref.shape[0]):
                        for j in range(self.dm.stack_back+self.dm.stack_front+1):
                            ret[i, j,] = self.dm.trajectories['states'][dat_ref[i, 0], dat_ref[i, 1]-self.dm.stack_back+j]
                    return ret
                elif key == 'sassy_observ':
                    dat_ref = self.dm.sassy[self.dm.cutoff_val_sassy:]
                    ret = np.zeros((dat_ref.shape[0], self.dm.stack_back+self.dm.stack_front+1)+self.dm.sim.obs_size, dtype=self.dm.sim.obs_dtype)
                    for i in range(dat_ref.shape[0]):
                        for j in range(self.dm.stack_back+self.dm.stack_front+1):
                            ret[i, j,] = self.dm.trajectories['observ'][dat_ref[i, 0], dat_ref[i, 1]-self.dm.stack_back+j]
                    return ret
                elif key == 'sassy_actions':
                    dat_ref = self.dm.sassy[self.dm.cutoff_val_sassy:]
                    ret = np.zeros((dat_ref.shape[0],)+self.dm.sim.action_size, dtype=self.dm.sim.action_dtype)
                    for i in range(dat_ref.shape[0]):
                        ret[i] = self.dm.trajectories['actions'][dat_ref[i, 0], dat_ref[i, 1]]
                    return ret
                elif key == 'sassy_rewards':
                    dat_ref = self.dm.sassy[self.dm.cutoff_val_sassy:]
                    ret = np.zeros(dat_ref.shape[0])
                    for i in range(dat_ref.shape[0]):
                        ret[i] = self.dm.trajectories['rewards'][dat_ref[i, 0], dat_ref[i, 1]]
                    return ret
                elif key == 'sassy_dones':
                    dat_ref = self.dm.sassy[self.dm.cutoff_val_sassy:]
                    ret = np.zeros(dat_ref.shape[0], dtype='bool')
                    for i in range(dat_ref.shape[0]):
                        ret[i] = self.dm.trajectories['dones'][dat_ref[i, 0], dat_ref[i, 1]]
                    return ret
                else:
                    return self.dm.trajectories[key][self.dm.cutoff_val:]
            else:
                raise KeyError
