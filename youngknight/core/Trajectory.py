"""
Trajectory.py

Classes for handeling trajectory generation
"""

import numpy as np
import tensorflow as tf

class Trajectory(object):
    def __init__(self, params, dt=0.05):
        self.params = params
        self.dt = dt

        self.reset()

    def get_state(self, k):
        raise NotImplementedError

    def reset(self):
        pass

    def spawn_trajectory(self):
        return self.__class__(self.params, self.dt)

    def __call__(self, k, n=1):
        if isinstance(k, tf.Tensor):
            s = tf.expand_dims(self.get_state(k), axis=0)
            multiples = [n]
            for i in range(len(s.shape)-1):
                multiples.append(1)
            return tf.tile(s, multiples)
        else:
            s = np.expand_dims(self.get_state(k), axis=0)
            multiples = np.ones_like(s.shape)
            multiples[0] = n
            return np.tile(s, multiples)
