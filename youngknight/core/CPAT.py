"""
CPAT.py

Class for Coninuous Peicewise Affine Triangulation
"""

import numpy as np
from scipy.spatial import Delaunay

class CPAT(object):
    def __init__(self, limits, tau, output_dim):
        self.limits = limits
        self.tau =  tau
        self.output_dim = output_dim
        
        self.ndims = self.limits.shape[0]
        
        offsets = []
        self.n_pts = []
        self.axes = []
        
        for i in range(self.ndims):
            offsets.append(-self.limits[i, 0])
            self.n_pts.append(int(np.ceil((self.limits[i, 1] - self.limits[i, 0])/self.tau)))
            
        self.verts = self.gen_verts(self.ndims, self.n_pts)
        self.y = np.zeros((self.verts.shape[0], self.output_dim))
        
        self.vert_map = np.zeros(self.n_pts, dtype='int32')
        
        for i in range(self.verts.shape[0]):
            coord = tuple(np.array(self.verts[i, :], dtype='int32').tolist())
            self.vert_map[coord] = i
            
        self.verts = self.verts*tau
        for i in range(self.ndims):
            self.verts[:, i] -= offsets[i]
            
        self.tri = Delaunay(self.verts)
    
    def gen_verts(self, ndims, n_pts):
        verts = np.zeros((np.prod(n_pts), ndims))
        
        if ndims == 1:
            verts[:, 0] = np.arange(n_pts[0])
        else:
            rest = self.gen_verts(ndims-1, n_pts[1:])
            seg_size = np.prod(n_pts[1:])
            for i in range(n_pts[0]):
                verts[i*seg_size:(i+1)*seg_size, 0] = np.ones(seg_size)*i
                verts[i*seg_size:(i+1)*seg_size, 1:] = rest 
                
        return verts
    
    def get_barrycentric_coords(self, x):
        batch_size = x.shape[0]
        
        simps = obj.tri.simplices(x)
        
        w = np.zeros((batch_size, self.ndims+1))
        w[:, -1] = np.ones(batch_size)
        
        for i in range(batch_size):
            shift = x[i, :] - self.tri.transform[simps[i], self.ndims, :]
            w[i, :-1] = np.matmul(shift, self.tri.transform[simps[i], :self.ndims, :self.ndims])
            w[i, :] /= np.sum(w[i, :])
        
        return w, simps
    
    def update_weights(self, x, y, lr=1.0):
        batch_size = x.shape[0]
        w, simps = self.get_barrycentric_coords(x)
        
        y_est = self(x)
        err = y - y_est
        
        for i in range(batch_size):
            for j in range(self.ndim+1):
                self.y[self.tri.simplices[simps[i], j], :] += lr*err[i, :]*w[i, j]
                
        return np.linalg.norm(err)
    
    def __call__(self, x):
        batch_size = x.shape[0]
        
        simps = obj.tri.simplices(x)
        
        w = np.zeros((batch_size, self.ndims+1))
        w[:, -1] = np.ones(batch_size)
        y = np.zeros((batch_size, self.output_dim))
        
        for i in range(batch_size):
            shift = x[i, :] - self.tri.transform[simps[i], self.ndims, :]
            w[i, :-1] = np.matmul(shift, self.tri.transform[simps[i], :self.ndims, :self.ndims])
            w[i, :] /= np.sum(w[i, :])
            
            for j in range(self.ndims+1):
                y_i = self.y[self.tri.simplices[simps[i], j], :]
                y[i, :] += w[j]*y_i
        
        return y

if __name__ == '__main__':
    obj = CPAT(np.array([[-2, 2], [-3, 3], [-4, 4]]), 0.5, 2)
    