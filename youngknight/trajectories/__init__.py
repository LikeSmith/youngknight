"""
youngknight.trajectories init
"""

from .Constant import *
from .Sinusoid import *
from .Ramp import *
from .Spline import *
from .Dataset import *