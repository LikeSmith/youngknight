"""
Sinusoid.py

random sinusoid trajectory generator
"""

import numpy as np

from ..core.Trajectory import Trajectory

class Sinusoid(Trajectory):
    def get_state(self, k):
        if 'n_ders' in self.params.keys() and self.params['n_ders'] > 0:
            s = np.zeros((self.params['n_ders']+1, self.params['n_dof']))

            s[0, :] = self.mag*np.sin(self.dt*k*self.frq*2*np.pi + self.phi) + self.off

            neg = 1
            for i in range(self.params['n_ders']):
                if i % 2 == 0:
                    s[i, :] = self.mag*(self.frq*2*np.pi)**(i+1)*neg*np.cos(self.dt*k*self.frq[i]*2*np.pi + self.phi)
                    neg *= -1
                else:
                    s[i, :] = self.mag*(self.frq*2*np.pi)**(i+1)*neg*np.sin(self.dt*k*self.frq[i]*2*np.pi + self.phi)

        else:
            s = self.mag*np.sin(self.dt*k*self.frq*2*np.pi + self.phi) + self.off

        return s

    def reset(self):
        self.mag = self.limits['min_mag'] + (self.limits['max_mag'] - self.limits['min_mag'])*np.random.rand(self.params['n_dof'])
        self.off = self.limits['min_off'] + (self.limits['max_off'] - self.limits['min_off'])*np.random.rand(self.params['n_dof'])
        self.phi = self.limits['min_phi'] + (self.limits['max_phi'] - self.limits['min_phi'])*np.random.rand(self.params['n_dof'])
