"""
Spline.py

Spline trajectory between two points
"""

import numpy as np

from ..core.Trajectory import Trajectory

class Spline(Trajectory):
    def get_state(self, k):
        if 'n_ders' in self.params.keys() and self.params['n_ders'] > 0:
            s = np.zeros((self.params['n_ders']+1, self.params['n_dof']))

            s[0, :] = self.alpha[0, :] + self.alpha[1, :]*k*self.dt + self.alpha[2, :]*(k*self.dt)**2 + self.alpha[3, :]*(k*self.dt)**3
            s[1, :] = self.alpha[1, :] + 2*self.alpha[2, :]*k*self.dt + 3*self.alpha[3, :]*(k*self.dt)**2
            if self.params['n_ders'] >= 2:
                s[2, :] = 2*self.alpha[2, :] + 6*self.alpha[3, :]*k*self.dt
            if self.params['n_ders'] >= 3:
                s[3, :] = 6*self.alpha[3, :]
        else:
            s = self.alpha[0, :] + self.alpha[1, :]*k*self.dt + self.alpha[2, :]*(k*self.dt)**2 + self.alpha[3, :]*(k*self.dt)**3

        return s

    def reset(self):
        p0 = self.params['min_p0'] + (self.params['max_p0'] - self.params['min_p0'])*np.random.rand(self.params['n_dof'])
        pf = self.params['min_pf'] + (self.params['max_pf'] - self.params['min_pf'])*np.random.rand(self.params['n_dof'])
        v0 = self.params['min_v0'] + (self.params['max_v0'] - self.params['min_v0'])*np.random.rand(self.params['n_dof'])
        vf = self.params['min_vf'] + (self.params['max_vf'] - self.params['min_vf'])*np.random.rand(self.params['n_dof'])

        tf = self.params['tf']

        A = np.linalg.inv(np.array([[1, 0, 0, 0], [1, tf, tf**2, tf**3], [0, 1, 0, 0], [0, 1, 2*tf, 3*tf**2]]))
        b = np.stack([p0, pf, v0, vf], axis=0)

        self.alpha = np.matmul(A, b)


class MFRMFP_Spline(Spline):
    def reset(self):
        c = (1/(9.81**2*0.5))

        p0 = self.params['min_p0'] + (self.params['max_p0'] - self.params['min_p0'])*np.random.rand(self.params['n_dof'])
        pf = self.params['min_pf'] + (self.params['max_pf'] - self.params['min_pf'])*np.random.rand(self.params['n_dof'])
        v0 = self.params['min_v0'] + (self.params['max_v0'] - self.params['min_v0'])*np.random.rand(self.params['n_dof'])
        vf = self.params['min_vf'] + (self.params['max_vf'] - self.params['min_vf'])*np.random.rand(self.params['n_dof'])

        p0[1] += c*p0[0]**2
        pf[1] += c*pf[0]**2

        tf = self.params['tf']

        A = np.linalg.inv(np.array([[1, 0, 0, 0], [1, tf, tf**2, tf**3], [0, 1, 0, 0], [0, 1, 2*tf, 3*tf**2]]))
        b = np.stack([p0, pf, v0, vf], axis=0)

        self.alpha = np.matmul(A, b)
