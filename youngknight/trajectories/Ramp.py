"""
Ramp.py

Ramp Trajectory
"""

import numpy as np

from ..core.Trajectory import Trajectory

class Ramp(Trajectory):
    def get_state(self, k):
        if 'n_ders' in self.params.keys() and self.params['n_ders'] > 0:
            s = np.zeros((self.params['n_ders']+1, self.params['n_dof']))

            s[0, :] = self.off + k*self.slope*self.dt
            s[1, :] = self.slope
        else:
            s = self.off + k*self.slope

        return s

    def reset(self):
        self.off = self.params['min_off'] + (self.params['max_off'] - self.params['min_off'])*np.random.rand(self.params['n_dof'])
        self.slope = self.params['min_slope'] + (self.params['max_slope'] - self.params['min_slope'])*np.random.rand(self.params['n_dof'])
