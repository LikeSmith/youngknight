"""
Constant.py

random constant trajectory generator
"""

import numpy as np

from ..core.Trajectory import Trajectory

class Constant(Trajectory):
    def get_state(self, k):
        if 'n_ders' in self.params.keys() and self.params['n_ders'] > 0:
            s = np.zeros((self.params['n_ders']+1, self.params['n_dof']))

            s[0, :] = self.state
        else:
            s = self.state

        return s

    def reset(self):
        self.state = self.params['min_val'] + (self.params['max_val'] - self.params['min_val'])*np.random.rand(self.params['n_dof'])
