"""
Dataset.py

pulls trajectory from dataset
"""

import numpy as np
import tensorflow as tf
import _pickle as pkl

from ..core.Trajectory import Trajectory

class Dataset(Trajectory):
    def __init__(self, data, dt=0.05):
        self.data = data
        self.data_tf = tf.constant(data, dtype='float')
        self.n = data.shape[0]

        super(Dataset, self).__init__(None, dt)

    def get_state(self, k):
        if isinstance(k, tf.Tensor):
            return self.data_tf[self.i, k]
        else:
            return self.data[self.i, k]

    def reset(self):
        self.i = np.random.randint(0, self.n)

class DatasetFromFile(Dataset):
    def __init__(self, filename, home=''):
        data, dt = pkl.load(open(home+filename, 'rb'))
        super(DatasetFromFile, self).__init__(data, dt)
