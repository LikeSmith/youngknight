"""
LinPol.py

linear policy

params:
h_size
activ
use_bias
use_l1_loss
use_l2_loss
sig_min
beta_entropy
init_var
"""

import tensorflow as tf

from .Policy import Policy
from ....core import activ_deserialize

class LinearPolicy(Policy):
    def setup_weights(self):
        w_init = tf.random_normal_initializer(0, self.params['init_var'])
        weights = {}
        weights['K'] = tf.get_variable('K', (self.state_size, self.actin_size), initializer=w_init)
        weights['W1'] = tf.get_variable('W1', (self.state_size+self.param_size, self.params['h_size']), initializer=w_init)
        weights['W2'] = tf.get_variable('W2', (self.params['h_size'], self.actin_size), initializer=w_init)
        
        if self.params['use_bias']:
            weights['b1'] = tf.get_variable('b1', (self.params['h_size'],), initializer=w_init)
            weights['b2'] = tf.get_variable('b2_mu', (self.actin_size,), initializer=w_init)
            
        return weights
        
    def build_policy(self, s, p, k, weights=None, training=True):
        if weights is None:
            weights = self.weights
            
        sig_min = tf.constant(self.params['sig_min'], name='sig_min')
        
        mu = tf.matmul(s, weights['K'])
        
        comb_in = tf.concat((s, p), axis=1)
        h = tf.matmul(comb_in, weights['W1'])
        if self.params['use_bias']:
            h = tf.add(h, weights['b1'])
        h = activ_deserialize(self.params['activ'])(h)
        
        sig = tf.matmul(h, weights['W2'])
        if self.params['use_bias']:
            sig = tf.add(sig, weights['b2'])
        sig = tf.nn.softplus(sig) + sig_min
        
        if training:
            a_dist = tf.distributions.Normal(mu, sig)
            
            a = a_dist.sample(1)[0, :, :]
            
            return a, a_dist
        else:
            a_dist = tf.distributions.Normal(mu, tf.zeros_like(sig))
            
            return mu, a_dist
        
    def build_loss(self, a, a_dist, adv, k, weights=None):
        if weights is None:
            weights = self.weights
            
        log_prob = a_dist.log_prob(a)
        exp_v = log_prob*tf.stop_gradient(adv)
        entropy = a_dist.entropy()
        loss = -tf.reduce_mean(self.params['beta_entropy']*entropy + exp_v)
        
        if self.params['use_l1_loss']:
            for key in weights.keys():
                loss += 0.001*tf.reduce_sum(tf.abs(weights[key]))
        
        if self.params['use_l2_loss']:
            for key in weights.keys():
                loss += tf.nn.l2_loss(weights[key])
                
        return loss
        