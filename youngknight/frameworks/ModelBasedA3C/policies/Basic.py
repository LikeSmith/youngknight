"""
Basic.py

basic mlp policy

params:
h_size
activ
use_bias
use_l1_loss
use_l2_loss
a_min
a_max
sig_min
beta_entropy
init_var
"""

import tensorflow as tf

from .Policy import Policy
from ....core import activ_deserialize

class BasicPolicy(Policy):
    def setup_weights(self):
        w_init = tf.random_normal_initializer(0, self.params['init_var'])
        weights = {}
        weights['W1'] = tf.get_variable('W1', (self.state_size+self.param_size, self.params['h_size']), initializer=w_init)
        weights['W2_mu'] = tf.get_variable('W2_mu', (self.params['h_size'], self.actin_size), initializer=w_init)
        weights['W2_sig'] = tf.get_variable('W2_sig', (self.params['h_size'], self.actin_size), initializer=w_init)
        
        if self.params['use_bias']:
            weights['b1'] = tf.get_variable('b1', (self.params['h_size'],), initializer=w_init)
            weights['b2_mu'] = tf.get_variable('b2_mu', (self.actin_size,), initializer=w_init)
            weights['b2_sig'] = tf.get_variable('b2_sig', (self.actin_size,), initializer=w_init)
            
        return weights
        
    def build_policy(self, s, p, k, weights=None, training=True):
        if weights is None:
            weights = self.weights
            
        if 'a_min' in self.params.keys() and 'a_max' in self.params.keys():
            limited = True
            offset = []
            scale = []
            for i in range(self.actin_size):
                offset.append(tf.constant((self.params['a_max'][i] + self.params['a_min'][i])/2.0, name='a_offset%d'%(i,)))
                scale.append(tf.constant((self.params['a_max'][i] - self.params['a_min'][i])/2.0, name='a_scale%d'%(i,)))
        else:
            limited = False
            
        sig_min = tf.constant(self.params['sig_min'], name='sig_min')
        
        comb_in = tf.concat((s, p), axis=1)
        h = tf.matmul(comb_in, weights['W1'])
        if self.params['use_bias']:
            h = tf.add(h, weights['b1'])
        h = activ_deserialize(self.params['activ'])(h)
        
        mu = tf.matmul(h, weights['W2_mu'])
        if self.params['use_bias']:
            mu = tf.add(mu, weights['b2_mu'])
        if limited:
            mu_sep = []
            for i in range(self.actin_size):
                mu_sep.append(tf.nn.tanh(mu[:, i])*scale[i] + offset[i])  
            mu = tf.stack(mu_sep, axis=1, name='corrected_mu')
        
        sig = tf.matmul(h, weights['W2_sig'])
        if self.params['use_bias']:
            sig = tf.add(sig, weights['b2_mu'])
        sig = tf.nn.softplus(sig) + sig_min
        
        if training:
            a_dist = tf.distributions.Normal(mu, sig)
            
            a = a_dist.sample(1)[0, :, :]
            
            if limited:
                a = tf.clip_by_value(a, self.params['a_min'], self.params['a_max'])
            
            return a, a_dist
        else:
            a_dist = tf.distributions.Normal(mu, tf.zeros_like(sig))
            
            return mu, a_dist
        
    def build_loss(self, a, a_dist, adv, k, weights=None):
        if weights is None:
            weights = self.weights
            
        log_prob = a_dist.log_prob(a)
        exp_v = log_prob*tf.stop_gradient(adv)
        entropy = a_dist.entropy()
        loss = -tf.reduce_mean(self.params['beta_entropy']*entropy + exp_v)
        
        if self.params['use_l1_loss']:
            for key in weights.keys():
                loss += 0.001*tf.reduce_sum(tf.abs(weights[key]))
        
        if self.params['use_l2_loss']:
            for key in weights.keys():
                loss += 0.0001*tf.nn.l2_loss(weights[key])
                
        return loss
        
