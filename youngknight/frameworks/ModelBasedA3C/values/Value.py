"""
Value.py

Base class for Value models
"""

import tensorflow as tf

from ....core.Model import Model

class Value(Model):
    def __init__(self, state_size, param_size, params={}, load_file=None, home='', name='Value', sess=None, debug=False):
        self.state_size = state_size
        self.param_size = param_size
        
        super(Value, self).__init__(params=params, load_file=load_file, home=home, name=name, sess=sess, debug=debug)
        
    def setup_weights(self):
        raise NotImplementedError
        
    def build_value(self, s, p, k, weights=None):
        raise NotImplementedError
        
    def build_loss(self, v, adv, k, weights=None):
        raise NotImplementedError
        
    def get_shared_weights(self):
        pass
    
    def build_network(self, inputs=None):
        with tf.variable_scope(self.name):
            if inputs is None:
                inputs = {}
                inputs['s'] = tf.placeholder('float', (None, self.state_size), 's0')
                inputs['p'] = tf.placeholder('float', (None, self.param_size), 'p')
                inputs['adv'] = tf.placeholder('float', (None,), 'adv')
                inputs['k'] = tf.placeholder('float', (), 'k')
                
            assert 's' in inputs.keys()
            assert 'p' in inputs.keys()
            assert 'adv' in inputs.keys()
            assert 'k' in inputs.keys()
            
            weights = self.setup_weights()
            v = self.build_value(inputs['s'], inputs['p'], inputs['k'], weights)
            loss = self.build_loss(v, inputs['adv'], inputs['k'], weights)
            
            output_ops = {}
            output_ops['v'] = v
            
            loss_ops = {}
            loss_ops['loss'] = loss
            
            return inputs, output_ops, weights, loss_ops, {}
        
    def __call__(self, s, p, k):
        return self.sess.run(self.output_ops['v'], feed_dict={self.inputs['s']:s, self.inputs['p']:p, self.inputs['k']:k})
    
    def val(self, s, p, k):
        return self.sess.run(self.output_ops['v'], feed_dict={self.inputs['s']:s, self.inputs['p']:p, self.inputs['k']:k})
    