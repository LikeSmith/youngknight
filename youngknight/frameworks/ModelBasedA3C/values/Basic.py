"""
Basic.py

Basic Value function

params:
h_size
activ
use_bias
use_l1_loss
use_l2_loss
init_var
"""

import tensorflow as tf

from .Value import Value
from ....core import activ_deserialize

class BasicValue(Value):
    def setup_weights(self):
        w_init = tf.random_normal_initializer(0, self.params['init_var'])
        weights = {}
        weights['W1'] = tf.get_variable('W1', (self.state_size + self.param_size, self.params['h_size']), initializer=w_init)
        weights['W2'] = tf.get_variable('W2', (self.params['h_size'], 1), initializer=w_init)
        
        if self.params['use_bias']:
            weights['b1'] = tf.get_variable('b1', (self.params['h_size'],), initializer=w_init)
            weights['b2'] = tf.get_variable('b2', (1,), initializer=w_init)
            
        return weights
        
    def build_value(self, s, p, k, weights=None):
        if weights is None:
            weights = self.weights
            
        comb_in = tf.concat((s, p), axis=1)
        h = tf.matmul(comb_in, weights['W1'])
        if self.params['use_bias']:
            h = tf.add(h, weights['b1'])
        h = activ_deserialize(self.params['activ'])(h)
        v = tf.matmul(h, weights['W2'])
        if self.params['use_bias']:
            v = tf.add(v, weights['b2'])
        
        return v
        
    def build_loss(self, v, adv, k, weights=None):
        if weights is None:
            weights = self.weights
            
        loss = tf.reduce_mean(tf.square(adv))
        
        if self.params['use_l1_loss']:
            for key in weights.keys():
                loss += 0.001*tf.reduce_sum(tf.abs(weights[key]))
        
        if self.params['use_l2_loss']:
            for key in weights.keys():
                loss += 0.001*tf.nn.l2_loss(weights[key])
                
        return loss
    
