"""
Actor.py

base class for Actors in the A3C framework using built in model

params:
gamma
block_size
pol_trainer
val_trainer
get_init_state
get_params
batch_size
T_max
t_max
verbosity
validation_rate
validation_patience
new_param_rate
"""

import numpy as np
import tensorflow as tf
from tensorflow.python import debug as tf_debug

from ....core import Model

class Actor(Model):
    T = 0
    UPDATE = 0
    GLB_RUN_R = []
    
    def __init__(self, dyn, rwd, policy, value, obs=None, params={}, load_file=None, home='', name='base_agent', debug=False, glb_actor=None, coord=tf.train.Coordinator(), sess=None):
        self.name = name
        self.home = home
        self.coord = coord
        self.hist = {}
        
        self.dyn = dyn
        self.obs = obs
        self.rwd = rwd
        self.policy = policy
        self.value = value
        
        if glb_actor is not None:
            self.glb_actor = glb_actor
            self.is_global = False
        else:
            self.is_global = True
            
        self.state_size = dyn.state_size
        self.actin_size = dyn.actin_size
        self.param_size = dyn.param_size
        
        if obs is not None:
            self.obsrv_size = obs.obsrv_size
            
        if load_file is not None:
            self.params, values = self.load(load_file, assign=False)
        else:
            self.params = params
        
        self.inputs, self.output_ops, self.weights, self.loss_ops, self.sync_ops = self.build_network()
        
        if sess is None:
            self.sess = tf.Session()
            if debug:
                self.sess = tf_debug.LocalCLIDebugWrapperSession(self.sess)
            self.sess.run(tf.global_variables_initializer())
        else:
            self.sess = sess
        
        if load_file is not None:
            self.assign_weights(values)
        
    def build_network(self, inputs=None):
        with tf.device('/cpu:0'):
            with tf.variable_scope(self.name):
                if inputs is None:
                    inputs = {}
                    inputs['s0'] = tf.placeholder('float', (None,) + self.state_size, 's0') # state
                    inputs['p'] = tf.placeholder('float', (None,) + self.param_size, 'p') # model parameters
                    inputs['k'] = tf.placeholder('float', (), 'k') # step number
                    
                    self.gamma = tf.constant(self.params['gamma'], name='gamma')
                    self.inc = tf.ones((), name='inc')
                    
                assert 's0' in inputs.keys()
                assert 'p' in inputs.keys()
                assert 'k' in inputs.keys()
                
                weights = {}
                if self.is_global:
                    weights['pol'] = self.policy.weights
                    weights['val'] = self.value.weights
                else:
                    with tf.variable_scope('pol'):
                        weights['pol'] = self.policy.setup_weights()
                    with tf.variable_scope('val'):
                        weights['val'] = self.value.setup_weights()
                
                weights['pol_list'] = []
                weights['pol_key'] = []
                weights['pol_ind'] = {}
                i = 0
                for key in weights['pol'].keys():
                    weights['pol_list'].append(weights['pol'][key])
                    weights['pol_key'].append(key)
                    weights['pol_ind'][key] = i
                    i += 1
                    
                weights['val_list'] = []
                weights['val_key'] = []
                weights['val_ind'] = {}
                i = 0
                for key in weights['val'].keys():
                    weights['val_list'].append(weights['val'][key])
                    weights['val_key'].append(key)
                    weights['val_ind'][key] = i
                    i += 1
                
                if self.obs is not None:
                    o0 = self.obs.build_obs(inputs['s0'], inputs['p'], inputs['k'])
                else:
                    o0 = inputs['s0']
                    
                states = []
                obsrvs = []
                actins = []
                values = []
                rewrds = []
                
                a_dists = []
                
                k = inputs['k']
                
                s = inputs['s0']
                o = o0
                
                with tf.variable_scope('forward'):
                    for i in range(self.params['block_size']):
                        if self.obs is not None:
                            a, a_dist = self.policy.build_policy(o, inputs['p'], k, weights['pol'])
                            v = self.value.build_value(o, inputs['p'], k, weights['val'])
                        else:
                            a, a_dist = self.policy.build_policy(s, inputs['p'], k, weights['pol'])
                            v = self.value.build_value(s, inputs['p'], k, weights['val'])
                            
                        s_new = self.dyn.build_dynamics(s, a, inputs['p'], k)
                        
                        if self.obs is not None:
                            o_new = self.obs.build_obs(s_new, inputs['p'], k)
                        else:
                            o_new = s_new
                        
                        r = self.rwd.build_reward(s, s_new, o, o_new, a, inputs['p'], k)
                        
                        states.append(s)
                        obsrvs.append(o)
                        actins.append(a)
                        values.append(v)
                        rewrds.append(r)
                        
                        a_dists.append(a_dist)
                        
                        s = s_new
                        o = o_new
                        k += self.inc
                
                with tf.variable_scope('backwards'):
                    R = [values[-1][:, 0]]
                    for r in rewrds[::-1]:
                        R.append(self.gamma*R[-1] + r)
                        
                    R.reverse()
                        
                with tf.variable_scope('loss'):
                    adv = []
                    for i in range(self.params['block_size']):
                        adv.append(tf.subtract(R[i], values[i][:, 0], name='advantage%d'%(i,)))
                        
                    loss_pol = []
                    loss_val = []
                    for i in range(self.params['block_size']):
                        l_p = self.policy.build_loss(actins[i], a_dists[i], adv[i], k, weights['pol'])
                        l_v = self.value.build_loss(values[i], adv[i], inputs['p'], weights['val'])
                            
                        loss_pol.append(l_p)
                        loss_val.append(l_v)
                        
                outputs = {}
                outputs['s'] = tf.stack(states[1:]+[s_new], axis=1, name='state_out')
                outputs['o'] = tf.stack(obsrvs, axis=1, name='obsrv_out')
                outputs['a'] = tf.stack(actins, axis=1, name='actin_out')
                outputs['v'] = tf.stack(values, axis=1, name='value_out')
                outputs['r'] = tf.stack(rewrds, axis=1, name='rewrd_out')
                outputs['l_p'] = tf.reduce_mean(tf.stack(loss_pol, axis=0, name='loss_pol'))
                outputs['l_v'] = tf.reduce_mean(tf.stack(loss_val, axis=0, name='loss_val'))
                
                loss_ops = {}
                loss_ops['pol'] = outputs['l_p']
                loss_ops['val'] = outputs['l_v']
                
                if not self.is_global:
                    sync_ops = {}
                    sync_ops['pull'] = {}
                    sync_ops['push'] = {}
                    
                    grads = {}
                    grads['pol'] = tf.gradients(loss_ops['pol'], weights['pol_list'])
                    grads['val'] = tf.gradients(loss_ops['val'], weights['val_list'])
                    
                    glb_weight_pol = []
                    for i in range(len(weights['pol_list'])):
                        glb_weight_pol.append(self.glb_actor.weights['pol'][weights['pol_key'][i]])
                    
                    glb_weight_val = []
                    for i in range(len(weights['val_list'])):
                        glb_weight_val.append(self.glb_actor.weights['val'][weights['val_key'][i]])
                    
                    with tf.name_scope('sync'):
                        with tf.name_scope('pull'):
                            for key in weights['pol'].keys():
                                sync_ops['pull']['pol_%s'%key] = weights['pol'][key].assign(self.glb_actor.weights['pol'][key])
                            for key in weights['val'].keys():
                                sync_ops['pull']['val_%s'%key] = weights['val'][key].assign(self.glb_actor.weights['val'][key])
                                
                        with tf.name_scope('push'):
                            sync_ops['push']['pol'] = self.params['pol_trainer'].apply_gradients(zip(grads['pol'], glb_weight_pol))
                            sync_ops['push']['val'] = self.params['val_trainer'].apply_gradients(zip(grads['val'], glb_weight_val))
                else:
                    sync_ops = None
                
        return inputs, outputs, weights, loss_ops, sync_ops
    
    def gen_init_states(self):
        s = self.params['get_init_state'](self.params['batch_size'])
        return s
    
    def gen_params(self):
        p = self.params['get_params'](self.params['batch_size'])
        return p
    
    def get_weights(self):
        return self.weights
    
    def get_T(self):
        return Actor.T
        
    def pull(self):
        if not self.is_global:
            self.sess.run(self.sync_ops['pull'])
    
    def push(self, s, p, t):
        if not self.is_global:
            self.sess.run(self.sync_ops['push'], feed_dict={self.inputs['s0']:s, self.inputs['p']:p, self.inputs['k']:t})
            Actor.UPDATE += 1
        
    def sync(self, s, p, t):
        self.push(s, p, t)
        self.pull()
        
    def end(self):
        return Actor.T >= self.params['T_max'] or self.coord.should_stop()
    
    def tick(self):
        Actor.T += 1
        
    def load_best(self):
        self.glb_actor.policy.assign_weights(self.best_weights_pol)
        self.glb_actor.value.assign_weights(self.best_value_pol)
        
    def spawn(self, n_actors, coord=None, sess=None):
        if coord is not None:
            self.coord = coord
        if sess is not None:
            self.sess = sess
            
        actors = []
        for i in range(n_actors):
            actors.append(self.__class__(self.dyn, self.rwd, self.policy, self.value, self.obs, self.params, home=self.home, name=self.name+'_%d'%(i,), glb_actor=self, coord=self.coord, sess=self.sess))
            
        return actors
    
    def validate(self):
        self.hist['ep_r'] = []
        self.hist['running_ep_r'] = None
        self.hist['ep_stamp'] = None
        self.hist['s'] = []
        self.hist['o'] = []
        self.hist['a'] = []
        self.hist['v'] = []
        self.hist['r'] = []
        this_ep = -1
        self.training = False
        best_r = -np.inf
        self.best_weights_pol = self.policy.save_weights()
        self.best_weights_val = self.value.save_weights()
        es_counter = 0
        
        if self.params['verbosity'] >= 1:
            print('validation actor started!')
            
        while not self.end():
            while Actor.UPDATE < self.params['validation_rate'] and not self.end():
                pass
            
            self.pull()
            this_ep = Actor.T
            t = 0
            Actor.UPDATE = 0
            
            s = self.gen_init_states()
            p = self.gen_params()
            o = self.obs.observe(s, p, 0)
            
            s_hist = []
            o_hist = []
            a_hist = []
            v_hist = []
            r_hist = []
            
            ep_r = 0.0
            
            while t <= self.params['t_max']:
                if self.obs is not None:
                    a = self.policy.pol(o, p, t)
                    v = self.value.val(o, p, t)
                else:
                    a = self.policy.pol(s, p, t)
                    v = self.value.val(s, p, t)
                
                s1 = self.dyn.step(s, a, p, t)
                o1 = self.obs.observe(s, p, t)
                r = self.rwd.get_rwd(s, s1, o, o1, a, p, t)
                
                ep_r += r
                
                s_hist.append(s)
                o_hist.append(o)
                a_hist.append(a)
                v_hist.append(v)
                r_hist.append(r)
                
                t += 1
                s = s1
                o = o1
            
            self.hist['s'].append(np.stack(s_hist, axis=1))
            self.hist['o'].append(np.stack(o_hist, axis=1))
            self.hist['a'].append(np.stack(a_hist, axis=1))
            self.hist['v'].append(np.stack(v_hist, axis=1))
            self.hist['r'].append(np.array(r_hist))
            self.hist['ep_r'].append(ep_r)
            
            if self.hist['running_ep_r'] is None:
                self.hist['running_ep_r'] = [np.mean(ep_r)]
                self.hist['ep_stamp'] = [this_ep]
            else:
                self.hist['running_ep_r'].append(0.9*self.hist['running_ep_r'][-1] + 0.1*np.mean(ep_r))
                self.hist['ep_stamp'].append(this_ep)
                
            if self.hist['running_ep_r'][-1] > best_r:
                best_r = self.hist['running_ep_r'][-1]
                self.best_weights_pol = self.policy.save_weights()
                self.best_weights_val = self.value.save_weights()
                es_counter = 0
            else:
                es_counter += 1
            
            if 'validation_patience' in self.params.keys() and es_counter >= self.params['validation_patience']:
                if self.params['verbosity'] >= 1:
                    print('Early Stop! Validation has stopped increasing')
                self.coord.request_stop()
                
            if self.params['verbosity'] >= 2:
                print('Validation has completed at episode %d, ep_r: %f'%(this_ep, self.hist['running_ep_r'][-1]))
            
        if self.params['verbosity'] >= 1:
            print('Validation is finished!')
        self.training = False
        
    def train(self):
        self.training = True
        self.hist = {}
        self.hist['s'] = []
        self.hist['o'] = []
        self.hist['a'] = []
        self.hist['v'] = []
        self.hist['r'] = []
        self.hist['ep_r'] = []
        self.hist['running_ep_r'] = None
        self.hist['l_p'] = []
        self.hist['l_v'] = []
        self.hist['p'] = []
        
        ep_n = 0
        
        self.pull()
        
        if self.params['verbosity'] >= 1:
            print('Actor %s is started!'%(self.name,))
        
        while not self.end():                
            t = 0
            s = self.gen_init_states()
            
            if t%self.params['new_param_rate'] == 0:
                p = self.gen_params()
            
            ep_r = np.zeros(self.params['batch_size'])
            s_hist = [np.expand_dims(s, axis=1)]
            o_hist = []
            a_hist = []
            v_hist = []
            r_hist = []
            
            while t <= self.params['t_max']:
                #self.sync(s, p, t)
                outputs = self.sess.run(self.output_ops, feed_dict={self.inputs['s0']:s, self.inputs['p']:p, self.inputs['k']:t})
                
                s_hist.append(outputs['s'])
                o_hist.append(outputs['o'])
                a_hist.append(outputs['a'])
                v_hist.append(outputs['v'])
                r_hist.append(outputs['r'])
                
                self.hist['l_p'].append(outputs['l_p'])
                self.hist['l_v'].append(outputs['l_v'])
                
                ep_r += np.sum(outputs['r'], axis=1)
                
                self.sync(s, p, t)
                
                s = s_hist[-1][:, -1]
                t += self.params['block_size']
                #s = s_hist[-1][:, 0]
                #t += 1
            
            self.hist['ep_r'].append(ep_r)
            self.hist['s'].append(np.concatenate(s_hist, axis=1))
            self.hist['o'].append(np.concatenate(o_hist, axis=1))
            self.hist['a'].append(np.concatenate(a_hist, axis=1))
            self.hist['v'].append(np.concatenate(v_hist, axis=1))
            self.hist['r'].append(np.concatenate(r_hist, axis=1))
            
            if self.hist['running_ep_r'] is None:
                self.hist['running_ep_r'] = [np.mean(ep_r)]
            else:
                self.hist['running_ep_r'].append(0.9*self.hist['running_ep_r'][-1] + 0.1*np.mean(ep_r))
            
            if len(Actor.GLB_RUN_R) == 0:
                Actor.GLB_RUN_R.append(np.mean(ep_r))
            else:
                Actor.GLB_RUN_R.append(Actor.GLB_RUN_R[-1]*0.9 + np.mean(ep_r)*0.1)
                
            if self.params['verbosity'] >= 2:
                print('Actor %s completed episode %d, ep_r: %f'%(self.name, Actor.T, Actor.GLB_RUN_R[-1]))
            self.tick()
            
            ep_n += 1
            
        if self.params['verbosity'] >= 1:
            print('Actor %s is finished!'%(self.name))
        self.training = False
