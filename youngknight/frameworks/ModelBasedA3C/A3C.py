"""
Asychronous Advantage Actor Critic (A3C)

This class impliments a model based A3C learner
"""

import tensorflow as tf
from tensorflow.python import debug as tf_debug
import multiprocessing
import threading
import os
import errno
from .actors.Actor import Actor

class A3C(object):
    def __init__(self, actor, n_workers=-1, home='', name='A3C', debug=False, log_dir=None, sess=None):
        self.n_workers = n_workers
        self.home = home
        self.name = name
        self.glb_actor = actor
        
        if sess is None:
            if debug:
                self.sess = tf_debug.LocalCLIDebugWrapperSession(self.sess)
            else:
                self.sess = tf.Session()
        else:
            self.sess = sess
            
        if self.n_workers == -1:
            self.n_workers = multiprocessing.cpu_count()
        
        self.coord = tf.train.Coordinator()
        self.actors = self.glb_actor.spawn(self.n_workers, self.coord, self.sess)
        
        self.sess.run(tf.global_variables_initializer())
        
        if log_dir is not None:
            try:
                os.makedirs(self.home+log_dir, exist_ok=True)
            except TypeError:
                try:
                    os.makedirs(self.home+log_dir)
                except OSError as exception:
                    if exception.errno != errno.EEXIST:
                        raise
            tf.summary.FileWriter(self.home+log_dir, self.sess.graph)
                    
    def start_actors(self, with_val=True):
        self.actor_threads = []
        self.start = 0
        
        if with_val:
            job = lambda: self.actors[0].validate()
            t = threading.Thread(target=job)
            t.start()
            self.actor_threads.append(t)
            self.start = 1
            
        for actor in self.actors[self.start:]:
            job = lambda: actor.train()
            t = threading.Thread(target=job)
            t.start()
            self.actor_threads.append(t)
            
    def join(self, use_best=False):
        self.coord.join(self.actor_threads)
        if use_best:
            self.actors[0].load_best()
        
    def get_histories(self):
        hists = []
        
        for actor in self.actors[self.start:]:
            hists.append(actor.hist)
            
        return hists
    
    def get_val_history(self):
        return self.actors[0].hist
    
    def get_glb_r(self):
        return Actor.GLB_RUN_R
    