"""
BasicAdv.py

Basic MLP Policy trained using advantage

params:
h_size
use_bias
activ
a_min
a_max
sig_min
beta_entropy
init_var
"""

import tensorflow as tf

from .Policy import Policy
from ....core import activ_deserialize

class BasicAdv(Policy):
    def setup_weights(self):
        w_init = tf.random_normal_initializer(0, self.params['init_var'])
        weights = {}
        weights['W1'] = tf.get_variable('W1', (self.state_size, self.params['h_size']), initializer=w_init)
        weights['W2_mu'] = tf.get_variable('W2_mu', (self.params['h_size'], self.actin_size), initializer=w_init)
        weights['W2_sig'] = tf.get_variable('W2_sig', (self.params['h_size'], self.actin_size), initializer=w_init)
        weights['b2_sig'] = tf.get_variable('b2_sig', self.actin_size, initializer=w_init)
        if self.params['use_bias']:
            weights['b1'] = tf.get_variable('b1', self.params['h_size'], initializer=w_init)
            weights['b2_mu'] = tf.get_variable('b2', self.actin_size, initializer=w_init)

        return weights

    def build_policy(self, s, p, k, weights=None):
        if weights is None:
            weights = self.weights

        if 'a_min' in self.params.keys() and 'a_max' in self.params.keys():
            limited = True
            offset = []
            scale = []
            for i in range(self.actin_size):
                offset.append(tf.constant((self.params['a_max'][i] + self.params['a_min'][i])/2.0, name='a_offset%d'%(i,)))
                scale.append(tf.constant((self.params['a_max'][i] - self.params['a_min'][i])/2.0, name='a_scale%d'%(i,)))
        else:
            limited = False

        h = tf.matmul(s, weights['W1'])
        if self.params['use_bias']:
            h = tf.add(h, weights['b1'])
        h = activ_deserialize(self.params['activ'])(h)

        mu = tf.matmul(h, weights['W2_mu'])
        if self.params['use_bias']:
            mu = tf.add(mu, weights['b2_mu'])

        if limited:
            mu = tf.nn.tanh(mu)
            mu_sep = []
            for i in range(self.actin_size):
                mu_sep.append(mu[:, i]*scale[i] + offset[i])
            mu = tf.stack(mu_sep, axis=1, name='limited_mu')

        sig = tf.nn.softplus(tf.add(tf.matmul(h, weights['W2_sig']), weights['b2_sig'])) + self.params['sig_min']

        a_dist = tf.distributions.Normal(mu, sig)

        a = a_dist.sample(1)[0]

        if limited:
            a = tf.clip_by_value(a, self.params['a_min'], self.params['a_max'])

        return a, mu, a_dist

    def build_loss(self, a, a_dist, adv, weights=None):
        if weights is None:
            weights = self.weights

        adv = tf.tile(tf.expand_dims(adv, axis=1), (1, self.actin_size))
        adv_pen = -a_dist.log_prob(a)*tf.stop_gradient(adv)
        ent_pen = -self.params['beta_entropy']*a_dist.entropy()

        return adv_pen + ent_pen
