"""
JacobianPol.py

estimate inverse jacobian

JacobianPol
h_size
use_bias
activ
a_min
a_max
n_ders
individualized
parameterized
max_swarm_size
init_var
K
dt
"""

import tensorflow as tf

from .Policy import Policy_AbsTrack
from ....core import activ_deserialize
import math

class JacobianPol(Policy_AbsTrack):
    #bn_hidden = None
    #bn_output = None

    def setup_weights(self):
        max_swarm_size = self.params['max_swarm_size']
        state_size = self.state_size[1]
        actin_size = self.actin_size[1]
        param_size = self.param_size[1]
        abstr_size = self.abstr_size

        w_init = tf.random_normal_initializer(0, self.params['init_var'])
        weights = {}

        if 'individualized' in self.params.keys() and self.params['individualized']:
            for i in range(max_swarm_size):
                weights['W1_%d'%i] = tf.get_variable('W1_%d'%i, (state_size, self.params['h_size']), initializer=w_init)
                weights['W2_%d'%i] = tf.get_variable('W2_%d'%i, (self.params['h_size'], actin_size*abstr_size), initializer=w_init)
                if self.params['use_bias']:
                    weights['b1_%d'%i] = tf.get_variable('b1_%d'%i, self.params['h_size'], initializer=w_init)
                    weights['b2_%d'%i] = tf.get_variable('b2_%d'%i, actin_size(abstr_size), initializer=w_init)
        elif 'parameterized' in self.params.keys() and self.params['parameterized']:
            weights['W1'] = tf.get_variable('W1', (state_size+param_size, self.params['h_size']), initializer=w_init)
            weights['W2'] = tf.get_variable('W2', (self.params['h_size'], actin_size*abstr_size), initializer=w_init)
            if self.params['use_bias']:
                weights['b1'] = tf.get_variable('b1', self.params['h_size'], initializer=w_init)
                weights['b2'] = tf.get_variable('b2', actin_size*abstr_size, initializer=w_init)
        else:
            weights['W1'] = tf.get_variable('W1', (state_size, self.params['h_size']), initializer=w_init)
            weights['W2'] = tf.get_variable('W2', (self.params['h_size'], actin_size*abstr_size), initializer=w_init)
            if self.params['use_bias']:
                weights['b1'] = tf.get_variable('b1', self.params['h_size'], initializer=w_init)
                weights['b2'] = tf.get_variable('b2', actin_size*abstr_size, initializer=w_init)

        #if self.bn_hidden is None:
            #self.bn_hidden = tf.layers.BatchNormalization(name='hidden_bn')
            #self.bn_output = tf.layers.BatchNormalization(name='output_bn')

        self.K = tf.constant(self.params['K'], dtype='float')
        self.dt = tf.constant(self.params['dt'], dtype='float')

        return weights

    def build_policy(self, s_swm, s_abs, s_des, p, k, weights=None, members=None, training=False):
        max_swarm_size = self.params['max_swarm_size']
        actin_size = self.actin_size[1]
        abstr_size = self.abstr_size
        n_ders = 0

        if members is None:
            members = range(max_swarm_size)

        if 'n_ders' in self.params.keys():
            n_ders = self.params['n_ders']

        if weights is None:
            weights = self.weights
        if 'a_min' in self.params.keys() and 'a_max' in self.params.keys():
            limited = True
            offset = []
            scale = []
            for i in range(actin_size):
                offset.append(tf.constant((self.params['a_max'][i] + self.params['a_min'][i])/2.0, name='a_offset%d'%(i,)))
                scale.append(tf.constant((self.params['a_max'][i] - self.params['a_min'][i])/2.0, name='a_scale%d'%(i,)))
        else:
            limited = False

        if n_ders != 0:
            s_des_ders = s_des[:, 1:, :]
            s_des = s_des[:, 0, :]

        err = s_des - s_abs

        del_abs = tf.matmul(err, self.K)

        if n_ders != 0:
            for i in range(n_ders):
                del_abs += s_des_ders[:, i, :]*tf.constant((self.params['dt']**i)/math.factorial(i), name='coeff%d'%(i+1), dtype='float32')

        del_abs *= self.dt

        if 'individualized' in self.params.keys() and self.params['individualized']:
            a = []
            for i in range(len(members)):
                h = tf.matmul(s_swm[:, i, :], weights['W1_%d'%members[i]])
                if self.params['use_bias']:
                    h = tf.add(h, weights['b1_%d'%members[i]])
                h = activ_deserialize(self.params['activ'])(h)
                j_flat = tf.matmul(h, weights['W2_%d'%members[i]])
                if self.params['use_bias']:
                    j_flat = tf.add(j_flat, weights['b2_%d'%members[i]])
                J = tf.reshape(j_flat, (-1, abstr_size, actin_size))
                a_i = tf.einsum('ij,ijk->ik', del_abs, J)
                if limited:
                    a_i = tf.nn.tanh(a_i)
                    a_sep = []
                    for j in range(actin_size):
                        a_sep.append(a_i[:, j]*scale[j] + offset[j])
                    a_i = tf.stack(a_sep, axis=1, name='limited_a')
                a.append(a_i)
            a = tf.stack(a, axis=1, name='a')
        else:
            if 'parameterized' in self.params.keys() and self.params['parameterized']:
                comb_in = tf.concat((s_swm, p), axis=2)
            else:
                comb_in = s_swm
            h = tf.einsum('ijk,kl->ijl', comb_in, weights['W1'])
            if self.params['use_bias']:
                h = tf.add(h, weights['b1'])
            #self.bn_hidden(h, training=training)
            h = activ_deserialize(self.params['activ'])(h)
            j_flat = tf.einsum('ijk,kl->ijl', h, weights['W2'])
            if self.params['use_bias']:
                j_flat = tf.add(j_flat, weights['b2'])
            J = []
            for i in range(actin_size):
                J.append([])
                for j in range(abstr_size):
                    J[i].append(j_flat[:, :, i*abstr_size+j])
                J[i] = tf.stack(J[i], axis=-1)
            J = tf.stack(J, axis=-1)
            a = tf.einsum('ij,ikjl->ikl', del_abs, J)
            if limited:
                #a = self.bn_output(a, training=training)
                a = tf.nn.tanh(a)
                a_sep = []
                for i in range(actin_size):
                    a_sep.append(a[:, :, i]*scale[i] + offset[i])
                a = tf.stack(a_sep, axis=2)

        '''if 'gamma_hidden' not in weights.keys():
            weights['gamma_hidden'] = self.bn_hidden.gamma
        if 'beta_hidden' not in weights.keys():
            weights['beta_hidden'] = self.bn_hidden.beta
        if 'gamma_output' not in weights.keys():
            weights['gamma_output'] = self.bn_output.gamma
        if 'beta_output' not in weights.keys():
            weights['beta_output'] = self.bn_output.beta'''

        return a