"""
LinPol.py

Linear Policy

params:
use_bias
init_var
"""

import tensorflow as tf

from .Policy import Policy

class LinearPolicy(Policy):
    def setup_weights(self):
        w_init = tf.random_normal_initializer(0, self.params['init_var'])
        weights = {}
        weights['K'] = tf.get_variable('K', (self.state_size, self.actin_size), initializer=w_init)
        if self.params['use_bias']:
            weights['b'] = tf.get_variable('b', (self.actin_size), initializer=w_init)
            
        return weights
        
    def build_policy(self, s, p, k, weights=None):
        if weights is None:
            weights = self.weights
            
        a = tf.matmul(s, weights['K'])
        
        if self.params['use_bias']:
            a = tf.add(a, weights['b'])
            
        return a
