"""
BasicCTG.py

Basic MLP cost-to-go estimator

params:
h_size
use_bias
activ
init_var
squared
"""

import tensorflow as tf

from .CostToGo import CostToGo
from ....core import activ_deserialize

class BasicCTG(CostToGo):
    save_params = ['h_size', 'use_bias', 'activ', 'init_var', 'squared']
    
    def setup_weights(self):
        w_init = tf.random_normal_initializer(0, self.params['init_var'])
        weights = {}
        weights['W1'] = tf.get_variable('W1', (self.state_size, self.params['h_size']), initializer=w_init)
        weights['W2'] = tf.get_variable('W2', (self.params['h_size'], 1), initializer=w_init)
        if self.params['use_bias']:
            weights['b1'] = tf.get_variable('b1', (self.params['h_size']), initializer=w_init)
            weights['b2'] = tf.get_variable('b2', (1), initializer=w_init)

        return weights

    def build_ctg(self, s, p, k, weights=None):
        if weights is None:
            weights = self.weights

        h = tf.matmul(s, weights['W1'])
        if self.params['use_bias']:
            h = tf.add(h, weights['b1'])
        h = activ_deserialize(self.params['activ'])(h)

        ctg = tf.matmul(h, weights['W2'])
        if self.params['use_bias']:
            ctg = tf.add(ctg, weights['b2'])

        if self.params['squared']:
            ctg = tf.multiply(ctg, ctg)
            
        return ctg

    def build_loss(self, adv):
        loss = tf.square(adv)
        return loss
    
    def get_save_params(self):
        save_params = {}
        for key in BasicCTG.save_params:
            save_params[key] = self.params[key]
            
        return save_params
