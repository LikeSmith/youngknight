"""
CostTogo.py

base class for cost-to-go learners.
"""

import tensorflow as tf
import numpy as np

from ....core.Model import Model

class CostToGo(Model):
    def __init__(self, state_size, param_size, dyn=None, pol=None, rwd=None, params={}, load_file=None, home='', name='Policy', sess=None, debug=False):
        self.state_size = state_size[0]
        self.param_size = param_size[0]

        self.dyn = dyn
        self.pol = pol
        self.rwd = rwd

        super(CostToGo, self).__init__(params=params, load_file=load_file, home=home, name=name, sess=sess, debug=debug)

    def setup_weights(self):
        raise NotImplementedError

    def build_ctg(self, s, p, k, weights=None):
        raise NotImplementedError

    def build_loss(self, adv):
       raise NotImplementedError

    def build_network(self, inputs=None, weights=None):
        with tf.variable_scope(self.name):
            if inputs is None:
                inputs = {}
                inputs['s'] = tf.placeholder('float', (None, self.state_size), 's0')
                inputs['p'] = tf.placeholder('float', (None, self.param_size), 'p')
                inputs['k'] = tf.placeholder('float', (None), 'k')

            assert 's' in inputs.keys()
            assert 'p' in inputs.keys()
            assert 'k' in inputs.keys()

            if weights is None:
                try:
                    weights = self.weights
                except AttributeError:
                    weights = self.setup_weights()

            v = self.build_ctg(inputs['s'], inputs['p'], inputs['k'], weights)

            loss_ops = {}
            train_ops = {}

            if self.dyn is not None and self.rwd is not None:
                if self.pol is not None:
                    pol_weights = {}

                    for key in self.pol.weights.keys():
                        W = self.pol.sess.run(self.pol.weights[key])
                        pol_weights[key] = tf.constant(W, name='pol/%s'%key)

                    a = self.pol.build_policy(inputs['s'], inputs['p'], inputs['k'], weights=pol_weights)
                    s_ = self.dyn.build_dynamics(inputs['s'], a, inputs['p'], inputs['k'])
                else:
                    s_ = self.dyn.build_dynamics(inputs['s'], inputs['p'], inputs['k'])
                    a = None

                c = -self.rwd.build_reward(inputs['s'], None, None, None, a, inputs['p'], inputs['k'])
                v_ = self.build_ctg(s_, inputs['p'], inputs['k']+1, weights)

                with tf.variable_scope('loss'):
                    loss_ops['loss'] = tf.reduce_mean(tf.squared_difference(v, c + v_))

                    if self.params['use_l1_loss']:
                        if 'gamma_l1' in self.params.keys():
                            gamma_l1 = self.params['gamma_l1']
                        else:
                            gamma_l1 = 1.0

                        for key in weights.keys():
                            loss_ops['loss'] += gamma_l1*tf.reduce_sum(tf.abs(weights[key]))

                    if self.params['use_l2_loss']:
                        if 'gamma_l2' in self.params.keys():
                            gamma_l2 = self.params['gamma_l2']
                        else:
                            gamma_l2 = 1.0

                        for key in weights.keys():
                            loss_ops['loss'] += gamma_l2*tf.nn.l2_loss(weights[key])

                with tf.variable_scope('train'):
                    train_ops['train'] = self.params['trainer'].minimize(loss_ops['loss'])

            output_ops = {}
            output_ops['ctg'] = v

            return inputs, output_ops, weights, loss_ops, train_ops

    def train(self, n_epochs, n_batches, batch_size, val_size):
        train_dat = {}
        train_dat['s'] = lambda i, batch_size, n_batches, n_samps: self.params['init_state_gen'](batch_size)
        train_dat['p'] = lambda i, batch_size, n_batches, n_samps: self.params['param_gen'](batch_size)
        train_dat['k'] = lambda i, batch_size, n_batches, n_samps: np.random.rand(batch_size)*self.params['n_steps']

        val_dat = {}
        val_dat['s'] = self.params['init_state_gen'](val_size)
        val_dat['p'] = self.params['param_gen'](val_size)
        val_dat['k'] = np.random.rand(val_size)*self.params['n_steps']

        return super(CostToGo, self).train(train_dat, val_dat, val_size=val_size, n_epochs=n_epochs, n_batches=n_batches, batch_size=batch_size, patience=self.params['patience'], verbosity=self.params['verbosity'], use_best=self.params['use_best'], shuffle=False, no_regen=self.params['no_regen'])

    def __call__(self, s, p, k):
        return self.sess.run(self.output_ops['ctg'], feed_dict={self.inputs['s']:s, self.inputs['p']:p, self.inputs['k']:k})

    def ctg(self, s, p, k):
        return self.sess.run(self.output_ops['ctg'], feed_dict={self.inputs['s']:s, self.inputs['p']:p, self.inputs['k']:k})
