"""
AbsTrack.py

network for learning controller that forces specific dynamics in the abstract
space

parameters:
n_ders
K
dt
p_max
p_min
use_l1_loss
use_l2_loss
gamma_l1
gamma_l2
trainer
var_swarm_size
min_swarm_size
max_swarm_size
init_state_gen
des_state_gen
param_gen
regen_data
verbosity
autosave
patience
use_best
"""

import tensorflow as tf
import numpy as np
import math
import os
import errno
from tqdm import trange

from ...core import Model

class AbsTrack(Model):
    def __init__(self, dyn, pol, abs_enc, params={}, load_file=None, home='', name='shooting', debug=False, sess=None, log_dir=None):
        self.dyn = dyn
        self.pol = pol
        self.abs_enc = abs_enc

        self.state_size = dyn.state_size[1]
        self.actin_size = dyn.actin_size[1]
        self.param_size = dyn.param_size[1]
        self.abstr_size = abs_enc.abs_size

        super(AbsTrack, self).__init__(params=params, load_file=load_file, home=home, name=name, sess=sess, debug=debug)

        self.dyn.sess = self.sess
        self.pol.sess = self.sess
        self.abs_enc.sess = self.sess

        self.sess.run(tf.global_variables_initializer())

        if log_dir is not None:
            try:
                os.makedirs(self.home+log_dir, exist_ok=True)
            except TypeError:
                try:
                    os.makedirs(self.home+log_dir)
                except OSError as exception:
                    if exception.errno != errno.EEXIST:
                        raise
            tf.summary.FileWriter(self.home+log_dir, self.sess.graph)

    def build_network(self, inputs=None, weights=None):
        with tf.variable_scope(self.name):
            if inputs is None:
                inputs = {}
                inputs['s_cur'] = tf.placeholder('float', (None, None, self.state_size), name='s_cur')
                inputs['p'] = tf.placeholder('float', (None, None, self.param_size), name='p')
                if 'n_ders' in self.params.keys() and self.params['n_ders'] > 0:
                    inputs['s_des'] = tf.placeholder('float', (None, self.params['n_ders']+1, self.abstr_size), 's_des')
                else:
                    inputs['s_des'] = tf.placeholder('float', (None, self.abstr_size), 's_des')

            if weights is None:
                try:
                    weights = self.weights
                except AttributeError:
                    weights = self.pol.weights

            if 'n_ders' in self.params.keys() and self.params['n_ders'] > 0:
                s_des_ders = inputs['s_des'][:, 1:, :]
                s_des = inputs['s_des'][:, 0, :]
            else:
                s_des = inputs['s_des']

            s_abs = self.abs_enc.build_abs(inputs['s_cur'], inputs['p'])
            err = s_des - s_abs

            K = tf.constant(self.params['K'], name='K', dtype='float32')
            dt = tf.constant(self.params['dt'], name='dt', dtype='float32')

            s_abs_del_des = tf.matmul(err, K)

            if 'n_ders' in self.params.keys() and self.params['n_ders'] > 0:
                for i in range(self.params['n_ders']):
                    s_abs_del_des += s_des_ders[:, i, :]*tf.constant((self.params['dt']**i)/math.factorial(i), name='coeff%d'%i, dtype='float32')

            s_abs_del_des *= dt

            a_cur = self.pol.build_policy(inputs['s_cur'], s_abs, inputs['s_des'], inputs['p'], 0)
            s_nxt = self.dyn.build_dynamics(inputs['s_cur'], a_cur, inputs['p'], 0)
            s_abs_nxt = self.abs_enc.build_abs(s_nxt, inputs['p'])

            s_abs_del = s_abs_nxt - s_abs

            output_ops = {}
            output_ops['a_cur'] = a_cur
            output_ops['s_abs'] = s_abs
            output_ops['s_nxt'] = s_nxt
            output_ops['s_abs_nxt'] = s_abs_nxt
            output_ops['s_abs_del_des'] = s_abs_del_des

            center = tf.constant(0.5*(self.params['p_max'] + self.params['p_min']), name='center', dtype='float32')
            radius = tf.constant(0.5*(self.params['p_max'] - self.params['p_min']), name='radius', dtype='float32')

            loss_ops = {}
            loss = tf.reduce_sum(tf.squared_difference(s_abs_del, s_abs_del_des), axis=1)
            loss += tf.reduce_sum(tf.square(tf.maximum(tf.abs(s_nxt[:, :, 0]-center)-radius, 0.0)), axis=1)

            loss = tf.reduce_mean(loss)

            if self.params['use_l1_loss']:
                if 'gamma_l1' in self.params.keys():
                    gamma_l1 = self.params['gamma_l1']
                else:
                    gamma_l1 = 1.0

                for key in weights.keys():
                    loss += gamma_l1*tf.reduce_sum(tf.abs(weights[key]))

            if self.params['use_l2_loss']:
                if 'gamma_l2' in self.params.keys():
                    gamma_l2 = self.params['gamma_l2']
                else:
                    gamma_l2 = 1.0

                for key in weights.keys():
                    loss += gamma_l2*tf.nn.l2_loss(weights[key])

            loss_ops['loss'] = loss

            train_ops = {}
            train_ops['train'] = self.params['trainer'].minimize(loss)

        return inputs, output_ops, weights, loss_ops, train_ops

    def train(self, n_epochs, n_batches, batch_size, val_size):
        hist = {}

        for key in self.loss_ops.keys():
            hist['ave_%s'%key] = np.zeros(n_epochs)
            hist['val_%s'%key] = np.zeros(n_epochs)

        min_weights={}
        min_loss = math.inf
        min_loss_epoch = -1
        min_count = 0

        if 'var_swarm_size' in self.params.keys() and self.params['var_swarm_size']:
            swarm_size = self.params['min_swarm_size'] + (self.params['max_swarm_size'] - self.params['min_swarm_size'])*np.random.rand(1)
        else:
            swarm_size = self.params['max_swarm_size']

        s_cur_val = self.params['init_state_gen'](val_size, swarm_size)
        s_des_val = self.params['des_state_gen'](val_size)
        p_val = self.params['param_gen'](val_size, swarm_size)

        if not self.params['regen_data']:
            s_cur_list = []
            s_des_list = []
            p_list = []

            if self.params['verbosity'] > 0:
                print('Generating Training data...')
            if self.params['verbosity'] in [2, 3]:
                batches = trange(n_batches)
            else:
                batches = range(n_batches)

            for i in batches:
                if 'var_swarm_size' in self.params.keys() and self.params['var_swarm_size']:
                    swarm_size = self.params['min_swarm_size'] + (self.params['max_swarm_size'] - self.params['min_swarm_size'])*np.random.rand(1)
                else:
                    swarm_size = self.params['max_swarm_size']
                s_cur_list.append(self.params['init_state_gen'](batch_size, swarm_size))
                s_des_list.append(self.params['des_state_gen'](batch_size))
                p_list.append(self.params['param_gen'](batch_size, swarm_size))

        if self.params['verbosity'] > 0:
            print('Training...')
        if self.params['verbosity'] == 3:
            epochs = trange(n_epochs)
            epochs.set_description('Val. Loss: N/A')
        else:
            epochs = range(n_epochs)

        for epoch in epochs:
            if self.params['verbosity'] in [1, 2]:
                print('Epoch %d/%d'%(epoch+1, n_epochs))

            if self.params['verbosity'] in [2, 3]:
                batches = trange(n_batches)
                batches.set_description('Ave. Loss: N/A')
            else:
                batches = range(n_batches)

            for batch in batches:
                if self.params['regen_data']:
                    if 'var_swarm_size' in self.params.keys() and self.params['var_swarm_size']:
                        swarm_size = self.params['min_swarm_size'] + (self.params['max_swarm_size'] - self.params['min_swarm_size'])*np.random.rand(1)
                    else:
                        swarm_size = self.params['max_swarm_size']
                    s_cur = self.params['init_state_gen'](batch_size, swarm_size)
                    s_des = self.params['des_state_gen'](batch_size)
                    p = self.params['param_gen'](batch_size, swarm_size)
                else:
                    s_cur = s_cur_list[batch]
                    s_des = s_des_list[batch]
                    p = p_list[batch]

                loss, _ = self.sess.run([self.loss_ops, self.train_ops], feed_dict={self.inputs['s_cur']:s_cur, self.inputs['p']:p, self.inputs['s_des']:s_des})

                for key in self.loss_ops.keys():
                    hist['ave_%s'%key][epoch] += loss[key]

                if self.params['verbosity'] in [2, 3]:
                    msg = ''
                    for key in self.loss_ops.keys():
                        msg += '(ave_%s: %f)'%(key, hist['ave_%s'%key][epoch]/(batch+1))

                    batches.set_description(msg)

            val_loss = self.sess.run(self.loss_ops, feed_dict={self.inputs['s_cur']:s_cur_val, self.inputs['p']:p_val, self.inputs['s_des']:s_des_val})

            total_val_loss = 0

            for key in self.loss_ops.keys():
                hist['ave_%s'%key][epoch] /= n_batches
                hist['val_%s'%key][epoch] = val_loss[key]
                total_val_loss += val_loss[key]

            if self.params['verbosity'] in [1, 2]:
                msg = ''
                for key in self.loss_ops.keys():
                    msg += '(ave_%s: %f, val_%s: %f)'%(key, hist['ave_%s'%key][epoch], key, hist['val_%s'%key][epoch])

                print('Epoch %d complete! %s'%(epoch+1, msg))

            elif self.params['verbosity'] == 3:
                msg = ''
                for key in self.loss_ops.keys():
                    msg += '(val_%s: %f)'%(key, hist['val_%s'%key][epoch])
                epochs.set_description(msg)

            if total_val_loss < min_loss:
                min_loss = total_val_loss
                min_loss_epoch = epoch
                min_weights = self.save_weights()
                min_count = 0
                if 'autosave' in self.params.keys():
                    self.pol.save('%s_epoch%d.pkl'%(self.params['autosave'][:-4], epoch))
            else:
                min_count += 1

            if 'patience' in self.params.keys() and min_count >= self.params['patience']:
                print('Validation loss has stopped decreassing, stopping training early.')

                for key in self.loss_ops.keys():
                    hist['ave_%s'%key] = hist['ave_%s'%key][:epoch+1]
                    hist['val_%s'%key] = hist['val_%s'%key][:epoch+1]

                break;

        if self.params['use_best']:
            self.assign_weights(min_weights)

        hist['min_loss'] = min_loss
        hist['min_loss_epoch'] = min_loss_epoch

        return hist
