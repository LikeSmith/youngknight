"""
Shooting.py

Policy learner with shooting method

params:
n_steps
use_l1_loss
use_l2_loss
gamma_l1
gamma_l2
trainer
verbosity
init_state_gen
param_gen
regen_data
patience
use_best
is_swarm
var_swarm_size
min_swarm_size
max_swarm_size

ShootingCTG:
Shooting method with cost-to-go estimation
additional params:
gamma
use_l1_loss_ctg
use_l2_loss_ctg
gamma_l1_ctg
gamma_l2_ctg
trainer_ctg

ShootingAdv:
Shooting method trained with advantage
params:
n_steps
gamma
use_l1_loss_pol
use_l2_loss_pol
use_l1_loss_ctg
use_l2_loss_ctg
gamma_l1_pol
gamma_l2_pol
gamma_l1_ctg
gamma_l2_ctg
trainer_pol
trainer_ctg
verbosity
init_state_gen
param_gen
regen_data
patience
use_best
"""

import tensorflow as tf
import numpy as np
import math
import _pickle as pkl
from tqdm import trange

from ...core import Model

class Shooting(Model):
    def __init__(self, dyn, rwd, policy, obs=None, params={}, load_file=None, home='', name='shooting', debug=False, sess=None):
        self.dyn = dyn
        self.rwd = rwd
        self.policy = policy
        self.obs = obs

        self.state_size = dyn.state_size
        self.actin_size = dyn.actin_size
        self.param_size = dyn.param_size
        if obs is not None:
            self.obsrv_size = obs.obsrv_size
        else:
            self.obsrv_size = self.state_size

        self.use_des_traj = False
        if 'des_traj_filename' in params.keys():
            data, _ = pkl.load(open(home+params['des_traj_filename'], 'rb'))
            self.des_traj_array = tf.constant(data, name='des_traj', dtype='float')
            self.n_traj = data.shape[0]
            self.use_des_traj = True

        super(Shooting, self).__init__(params=params, load_file=load_file, home=home, name=name, sess=sess, debug=debug)

        self.dyn.sess = self.sess
        self.rwd.sess = self.sess
        self.policy.sess = self.sess

        if self.obs is not None:
            self.obs.sess = self.sess

        self.sess.run(tf.global_variables_initializer())

    def build_network(self, inputs=None, weights=None):
        with tf.variable_scope(self.name):
            if inputs is None:
                inputs = {}
                inputs['s0'] = tf.placeholder('float', (None,) + self.state_size, 's0')
                inputs['p'] = tf.placeholder('float', (None,) + self.param_size, 'p')

                if self.use_des_traj:
                    inputs['i'] = tf.placeholder('int32', (), 'i')

                self.k_0 = tf.zeros((), dtype='int32')
                self.inc = tf.ones((), dtype='int32')

            if weights is None:
                try:
                    weights = self.weights
                except AttributeError:
                    weights = self.policy.weights

            if self.obs is not None:
                o0 = self.obs.build_obs(inputs['s0'], inputs['p'], self.k_0)
            else:
                o0 = inputs['s0']

            states = [inputs['s0']]
            obsrvs = [o0]
            actins = []
            rewrds = []

            multiples = [tf.shape(inputs['s0'])[0]]
            for i in range(len(inputs['s0'].shape)-1):
                multiples.append(1)
            k = self.k_0
            with tf.variable_scope('forward'):
                for i in range(self.params['n_steps']):
                    s = states[i]
                    o = obsrvs[i]
                    if self.use_des_traj:
                        s_des = self.des_traj_array[inputs['i'], i]
                        s_des = tf.expand_dims(s_des, axis=0)
                        s_des = tf.tile(s_des, multiples)
                        a = self.policy.build_policy(o, inputs['p'], k, s_des=s_des)
                    else:
                        a = self.policy.build_policy(o, inputs['p'], k)
                    s_ = self.dyn.build_dynamics(s, a, inputs['p'], k)

                    if self.obs is not None:
                        o_ = self.obs.build_obs(s_, inputs['p'], k)
                    else:
                        o_ = s_

                    if self.use_des_traj:
                        r = self.rwd.build_reward(s, s_, o, o_, a, inputs['p'], k, s_des=s_des)
                    else:
                        r = self.rwd.build_reward(s, s_, o, o_, a, inputs['p'], k)

                    states.append(s_)
                    obsrvs.append(o_)
                    actins.append(a)
                    rewrds.append(r)

                    k += self.inc

            output_ops = {}
            output_ops['s'] = tf.stack(states, axis=1)
            output_ops['o'] = tf.stack(obsrvs, axis=1)
            output_ops['a'] = tf.stack(actins, axis=1)
            output_ops['r'] = tf.stack(rewrds, axis=1)
            output_ops['ep_r'] = tf.reduce_sum(output_ops['r'], 1)

            with tf.variable_scope('loss'):
                loss_ops = {}
                loss_ops['loss_pol'] = -tf.reduce_mean(output_ops['r'])

                if self.params['use_l1_loss']:
                    if 'gamma_l1' in self.params.keys():
                        gamma_l1 = self.params['gamma_l1']
                    else:
                        gamma_l1 = 1.0

                    for key in weights.keys():
                        loss_ops['loss_pol'] += gamma_l1*tf.reduce_sum(tf.abs(weights[key]))

                if self.params['use_l2_loss']:
                    if 'gamma_l2' in self.params.keys():
                        gamma_l2 = self.params['gamma_l2']
                    else:
                        gamma_l2 = 1.0

                    for key in weights.keys():
                        loss_ops['loss_pol'] += gamma_l2*tf.nn.l2_loss(weights[key])

            train_ops = {}
            with tf.variable_scope('train'):
                train_ops['train_pol'] = self.params['trainer'].minimize(loss_ops['loss_pol'])

        return inputs, output_ops, weights, loss_ops, train_ops

    def train(self, n_epochs, n_batches, batch_size, val_size):
        hist = {}

        if not 'hist_mode' in self.params.keys():
            self.params['hist_mode'] = 1

        for key in self.loss_ops.keys():
            hist['ave_%s'%key] = np.zeros(n_epochs)
            hist['val_%s'%key] = np.zeros(n_epochs)

        if self.params['hist_mode'] == 2:
            for key in self.output_ops.keys():
                shape_arr = self.sess.run(tf.shape(self.output_ops[key][0]))
                shape = ()
                for dim in shape_arr:
                    if dim > 0:
                        shape += (dim,)

                hist[key] = np.zeros((val_size)+shape)
        elif self.params['hist_mode'] == 1:
            for key in self.output_ops.keys():
                shape_arr = self.sess.run(tf.shape(self.output_ops[key][0]))
                shape = ()
                for dim in shape_arr:
                    if dim > 0:
                        shape += (dim,)

            hist[key] = np.zeros((n_epochs, val_size)+shape)

        min_weights={}
        min_loss = math.inf
        min_loss_epoch = -1
        min_count = 0

        if 'is_swarm' in self.params.keys() and self.params['is_swarm']:
            if 'var_swarm_size' in self.params.keys() and self.params['var_swarm_size']:
                swarm_size = self.params['min_swarm_size'] + (self.params['max_swarm_size'] - self.params['min_swarm_size'])*np.random.rand(1)
            else:
                swarm_size = self.params['max_swarm_size']

            s0_val = self.params['init_state_gen'](val_size, swarm_size)
            p_val = self.params['param_gen'](val_size, swarm_size)
        else:
            s0_val = self.params['init_state_gen'](val_size)
            p_val = self.params['param_gen'](val_size)

        if not self.params['regen_data']:
            s0_list = []
            p_list = []

            if self.params['verbosity'] > 0:
                print('Generating Training data...')
            if self.params['verbosity'] in [2, 3]:
                batches = trange(n_batches)
            else:
                batches = range(n_batches)

            for i in batches:
                if 'is_swarm' in self.params.keys() and self.params['is_swarm']:
                    if 'var_swarm_size' in self.params.keys() and self.params['var_swarm_size']:
                        swarm_size = self.params['min_swarm_size'] + (self.params['max_swarm_size'] - self.params['min_swarm_size'])*np.random.rand(1)
                    else:
                        swarm_size = self.params['max_swarm_size']
                    s0_list.append(self.params['init_state_gen'](batch_size, swarm_size))
                    p_list.append(self.params['param_gen'](batch_size, swarm_size))
                else:
                    s0_list.append(self.params['init_state_gen'](batch_size))
                    p_list.append(self.params['param_gen'](batch_size))

        if self.params['verbosity'] > 0:
            print('Training...')
        if self.params['verbosity'] == 3:
            epochs = trange(n_epochs)
            epochs.set_description('Val. Loss: N/A')
        else:
            epochs = range(n_epochs)

        for epoch in epochs:
            if self.params['verbosity'] in [1, 2]:
                print('Epoch %d/%d'%(epoch+1, n_epochs))

            if self.params['verbosity'] in [2, 3]:
                batches = trange(n_batches)
                batches.set_description('Ave. Loss: N/A')
            else:
                batches = range(n_batches)

            for batch in batches:
                if self.params['regen_data']:
                    if 'is_swarm' in self.params.keys() and self.params['is_swarm']:
                        if 'var_swarm_size' in self.params.keys() and self.params['var_swarm_size']:
                            swarm_size = self.params['min_swarm_size'] + (self.params['max_swarm_size'] - self.params['min_swarm_size'])*np.random.rand(1)
                        else:
                            swarm_size = self.params['max_swarm_size']

                        s0 = self.params['init_state_gen'](batch_size, swarm_size)
                        p = self.params['param_gen'](batch_size, swarm_size)
                    else:
                        s0 = self.params['init_state_gen'](batch_size)
                        p = self.params['param_gen'](batch_size)
                else:
                    s0 = s0_list[batch]
                    p = p_list[batch]

                if self.use_des_traj:
                    i = np.random.randint(self.n_traj-1)
                    loss, _ = self.sess.run([self.loss_ops, self.train_ops], feed_dict={self.inputs['s0']:s0, self.inputs['p']:p, self.inputs['i']:i})
                else:
                    loss, _ = self.sess.run([self.loss_ops, self.train_ops], feed_dict={self.inputs['s0']:s0, self.inputs['p']:p})

                for key in self.loss_ops.keys():
                    hist['ave_%s'%key][epoch] += loss[key]

                if self.params['verbosity'] in [2, 3]:
                    msg = ''
                    for key in self.loss_ops.keys():
                        msg += '(ave_%s: %f)'%(key, hist['ave_%s'%key][epoch]/(batch+1))

                    batches.set_description(msg)

            if self.use_des_traj:
                val_outputs, val_loss = self.sess.run([self.output_ops, self.loss_ops], feed_dict={self.inputs['s0']:s0_val, self.inputs['p']:p_val, self.inputs['i']:-1})
            else:
                val_outputs, val_loss = self.sess.run([self.output_ops, self.loss_ops], feed_dict={self.inputs['s0']:s0_val, self.inputs['p']:p_val})

            total_val_loss = 0

            if self.params['hist_mode'] == 1:
                for key in self.output_ops.keys():
                    hist[key][epoch, :] = val_outputs[key]

            for key in self.loss_ops.keys():
                hist['ave_%s'%key][epoch] /= n_batches
                hist['val_%s'%key][epoch] = val_loss[key]
                total_val_loss += val_loss[key]

            if self.params['verbosity'] in [1, 2]:
                msg = ''
                for key in self.loss_ops.keys():
                    msg += '(ave_%s: %f, val_%s: %f)'%(key, hist['ave_%s'%key][epoch], key, hist['val_%s'%key][epoch])

                print('Epoch %d complete! %s'%(epoch+1, msg))

            elif self.params['verbosity'] == 3:
                msg = ''
                for key in self.loss_ops.keys():
                    msg += '(val_%s: %f)'%(key, hist['val_%s'%key][epoch])
                epochs.set_description(msg)

            if total_val_loss < min_loss:
                min_loss = total_val_loss
                min_loss_epoch = epoch
                min_weights = self.save_weights()
                min_count = 0
                if self.params['hist_mode'] == 2:
                    hist[key] = val_outputs[key]
                if 'autosave' in self.params.keys():
                    self.policy.save('%s_epoch%d.pkl'%(self.params['autosave'][:-4], epoch))
                    pkl.dump([val_outputs, val_loss, s0_val, p_val], open(self.home+'val_results_epoch%d.pkl'%epoch, 'wb'))
            else:
                min_count += 1

            if 'patience' in self.params.keys() and min_count > self.params['patience']:
                print('Validation loss has stopped decreassing, stopping training early.')

                if self.params['hist_mode'] == 1:
                    for key in self.output_ops.keys():
                        hist[key] = hist[key][:epoch+1, :]

                for key in self.loss_ops.keys():
                    hist['ave_%s'%key] = hist['ave_%s'%key][:epoch+1]
                    hist['val_%s'%key] = hist['val_%s'%key][:epoch+1]

                break;

        if self.params['use_best']:
            self.assign_weights(min_weights)

        hist['min_loss'] = min_loss
        hist['min_loss_epoch'] = min_loss_epoch

        return hist

class ShootingCTG(Shooting):
    def __init__(self, dyn, rwd, policy, ctg, obs=None, params={}, load_file=None, home='', name='shooting', debug=False, sess=None):
        self.ctg = ctg

        super(ShootingCTG, self).__init__(dyn, rwd, policy, obs=obs, params=params, load_file=load_file, home=home, name=name, sess=sess, debug=debug)

    def build_network(self, inputs=None, weights=None):
        inputs, output_ops, weights, loss_ops, train_ops = super(ShootingCTG, self).build_network(inputs, weights)

        for key in self.ctg.weights.keys():
            weights['ctg_%s'%key] = self.ctg.weights[key]

        with tf.variable_scope(self.name):
            ctg = []
            ctg_est = []

            with tf.variable_scope('ctg'):
                ctg_forward = self.ctg.build_ctg(output_ops['o'][:, -1, :], inputs['p'], self.params['n_steps'])
                c = tf.concat((-output_ops['r'], ctg_forward), axis=1)

                for i in range(self.params['n_steps'] + 1):
                    gamma_arr = []
                    for j in range(self.params['n_steps'] + 1 - i):
                        gamma_arr.append(self.params['gamma']**j)

                    gamma_arr = tf.constant(gamma_arr, shape=(self.params['n_steps'] + 1 - i, 1), name='discount_array%d'%i)

                    ctg.append(tf.matmul(c[:, i:], gamma_arr))
                    ctg_est.append(self.ctg.build_ctg(output_ops['o'][:, i, :], inputs['p'], i))

                ctg = tf.concat(ctg, axis=1)
                ctg_est = tf.concat(ctg_est, axis=1)

                output_ops['ctg'] = ctg
                output_ops['ctg_est'] = ctg_est

                with tf.variable_scope('loss'):
                    loss_ops['loss_ctg'] = tf.reduce_mean(tf.squared_difference(ctg, ctg_est))

                    if self.params['use_l1_loss_ctg']:
                        if 'gamma_l1_ctg' in self.params.keys():
                            gamma_l1 = self.params['gamma_l1_ctg']
                        else:
                            gamma_l1 = 1.0

                        for key in self.ctg.weights.keys():
                            loss_ops['loss_ctg'] += gamma_l1*tf.reduce_sum(tf.abs(self.ctg.weights[key]))

                    if self.params['use_l2_loss_ctg']:
                        if 'gamma_l2_ctg' in self.params.keys():
                            gamma_l2 = self.params['gamma_l2_ctg']
                        else:
                            gamma_l2 = 1.0

                        for key in self.ctg.weights.keys():
                            loss_ops['loss_ctg'] += gamma_l2*tf.nn.l2_loss(self.ctg.weights[key])

                with tf.variable_scope('train'):
                    train_ops['train_ctg'] = self.params['trainer_ctg'].minimize(loss_ops['loss_ctg'])

        return inputs, output_ops, weights, loss_ops, train_ops

class ShootingAdv(ShootingCTG):
    def build_network(self, inputs=None, weights=None):
        with tf.variable_scope(self.name):
            if inputs is None:
                inputs = {}
                inputs['s0'] = tf.placeholder('float', (None,) + self.state_size, 's0')
                inputs['p'] = tf.placeholder('float', (None,) + self.param_size, 'p')

                self.k_0 = tf.zeros_like(inputs['s0'][:, 0])
                self.inc = tf.ones_like(self.k_0)

            if weights is None:
                try:
                    weights = self.weights
                except AttributeError:
                    pol_weights = self.policy.weights
                    ctg_weights = self.ctg.weights

                    weights = {}
                    for key in pol_weights.keys():
                        weights['pol_%s'] = pol_weights[key]
                    for key in ctg_weights.keys():
                        weights['ctg_%s'] = ctg_weights[key]

            if self.obs is not None:
                o0 = self.obs.build_obs(inputs['s0'], inputs['p'], self.k_0)
            else:
                o0 = inputs['s0']

            states = [inputs['s0']]
            obsrvs = [o0]
            actins = []
            a_dists = []
            rewrds = []

            k = self.k_0
            with tf.variable_scope('forward'):
                for i in range(self.params['n_steps']):
                    s = states[i]
                    o = obsrvs[i]

                    a, _, a_dist = self.policy.build_policy(o, inputs['p'], k)
                    s_ = self.dyn.build_dynamics(s, a, inputs['p'], k)

                    if self.obs is not None:
                        o_ = self.obs.build_obs(s_, inputs['p'], k)
                    else:
                        o_ = s_

                    r = self.rwd.build_reward(s, s_, o, o_, a, inputs['p'], k)

                    states.append(s_)
                    obsrvs.append(o_)
                    actins.append(a)
                    a_dists.append(a_dist)
                    rewrds.append(r)

                    k += self.inc


            states = tf.stack(states, axis=1)
            obsrvs = tf.stack(obsrvs, axis=1)
            actins = tf.stack(actins, axis=1)
            rewrds = tf.stack(rewrds, axis=1)

            ctg = []
            ctg_est = []

            with tf.variable_scope('ctg'):
                ctg_forward = self.ctg.build_ctg(obsrvs[:, -1, :], inputs['p'], self.params['n_steps'])
                c = tf.concat((-rewrds, ctg_forward), axis=1)

                for i in range(self.params['n_steps'] + 1):
                    gamma_arr = []
                    for j in range(self.params['n_steps'] + 1 - i):
                        gamma_arr.append(self.params['gamma']**j)

                    gamma_arr = tf.constant(gamma_arr, shape=(self.params['n_steps'] + 1 - i, 1), name='discount_array%d'%i)

                    ctg.append(tf.matmul(c[:, i:], gamma_arr))
                    ctg_est.append(self.ctg.build_ctg(obsrvs[:, i, :], inputs['p'], i))

                ctg = tf.concat(ctg, axis=1)
                ctg_est = tf.concat(ctg_est, axis=1)

            output_ops = {}
            output_ops['s'] = states
            output_ops['o'] = obsrvs
            output_ops['a'] = actins
            output_ops['r'] = rewrds
            output_ops['ctg'] = ctg
            output_ops['ctg_est'] = ctg_est
            output_ops['ep_r'] = tf.reduce_sum(output_ops['r'], 1)

            loss_ops = {}
            with tf.variable_scope('loss'):
                adv = ctg_est - ctg

                pol_loss = []
                ctg_loss = []
                for i in range(self.params['n_steps']):
                    pol_loss.append(self.policy.build_loss(actins[:, i, :], a_dists[i], adv[:, i]))
                    ctg_loss.append(self.ctg.build_loss(adv))

                pol_loss = tf.reduce_mean(tf.stack(pol_loss, axis=1))
                ctg_loss = tf.reduce_mean(tf.stack(ctg_loss, axis=1))
                loss_ops['ep_c'] = -tf.reduce_mean(output_ops['ep_r'])

                if self.params['use_l1_loss_pol']:
                    if 'gamma_l1_pol' in self.params.keys():
                        gamma_l1 = self.params['gamma_l1_pol']
                    else:
                        gamma_l1 = 1.0

                    for key in pol_weights.keys():
                        pol_loss += gamma_l1*tf.reduce_sum(tf.abs(pol_weights[key]))

                if self.params['use_l2_loss_pol']:
                    if 'gamma_l2_pol' in self.params.keys():
                        gamma_l2 = self.params['gamma_l2_pol']
                    else:
                        gamma_l2 = 1.0

                    for key in pol_weights.keys():
                        pol_loss += gamma_l2*tf.nn.l2_loss(pol_weights[key])

                if self.params['use_l1_loss_ctg']:
                    if 'gamma_l1_ctg' in self.params.keys():
                        gamma_l1 = self.params['gamma_l1_ctg']
                    else:
                        gamma_l1 = 1.0

                    for key in ctg_weights.keys():
                        ctg_loss += gamma_l1*tf.reduce_sum(tf.abs(ctg_weights[key]))

                if self.params['use_l2_loss_ctg']:
                    if 'gamma_l2' in self.params.keys():
                        gamma_l2 = self.params['gamma_l2_ctg']
                    else:
                        gamma_l2 = 1.0

                    for key in ctg_weights.keys():
                        ctg_loss += gamma_l2*tf.nn.l2_loss(ctg_weights[key])

            train_ops = {}
            with tf.variable_scope('train'):
                train_ops['train_pol'] = self.params['trainer_pol'].minimize(pol_loss)
                train_ops['train_ctg'] = self.params['trainer_ctg'].minimize(ctg_loss)

        return inputs, output_ops, weights, loss_ops, train_ops
