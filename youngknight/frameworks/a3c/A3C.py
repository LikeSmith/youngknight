"""
Asychronous Advantage Actor Critic (A3C)

This class impliments an A3C learner
"""

import tensorflow as tf
from tensorflow.python import debug as tf_debug
import multiprocessing
import threading
import os
import errno
from tqdm import tqdm
from .actors.Actor import Actor

class A3C(object):
    def __init__(self, sim, actor, actor_params, n_workers=-1, use_gym=False, use_obs=False, home='', name='A3C', debug=False, log_dir=None):
        self.sim = sim
        self.actor_params = actor_params
        self.n_workers = n_workers
        self.home = home
        self.name = name
        self.sess = tf.Session()
        
        if debug:
            self.sess = tf_debug.LocalCLIDebugWrapperSession(self.sess)
            
        if self.n_workers == -1:
            self.n_workers = multiprocessing.cpu_count()
        
        self.coord = tf.train.Coordinator()
        self.actors = []
        
        with tf.device('/cpu:0'):
            with tf.variable_scope(self.name):
                if use_gym:
                    self.global_actor = actor(env_name=sim, use_obs=use_obs, coord=self.coord, sess=self.sess, params=actor_params, home=home, name='%s_G'%(self.name,), debug=debug, is_global=True)
                    for i in range(self.n_workers):
                        self.actors += [actor(env_name=sim, use_obs=use_obs, coord=self.coord, sess=self.sess, params=actor_params, home=home, name='%s_w%d'%(self.name, i), debug=debug, glb_actor=self.global_actor)]
                else:
                    self.global_actor = actor(sim=sim, use_obs=use_obs, coord=self.coord, sess=self.sess, params=actor_params, home=home, name='%s_G'%(self.name,), debug=debug, is_global=True)
                    for i in range(self.n_workers):
                        self.actors += [actor(sim=sim, use_obs=use_obs, coord=self.coord, sess=self.sess, params=actor_params, home=home, name='%s_w%d'%(self.name, i), debug=debug, glb_actor=self.global_actor)]
        
        self.sess.run(tf.global_variables_initializer())
        
        if log_dir is not None:
            try:
                os.makedirs(self.home+log_dir, exist_ok=True)
            except TypeError:
                try:
                    os.makedirs(self.home+log_dir)
                except OSError as exception:
                    if exception.errno != errno.EEXIST:
                        raise
            tf.summary.FileWriter(self.home+log_dir, self.sess.graph)
                    
    def start_actors(self, with_val=False):
        self.actor_threads = []
        self.start = 0
        
        if with_val:
            job = lambda: self.actors[0].validate()
            t = threading.Thread(target=job)
            t.start()
            self.actor_threads.append(t)
            self.start = 1
            
        for actor in self.actors[self.start:]:
            job = lambda: actor.train()
            t = threading.Thread(target=job)
            t.start()
            self.actor_threads.append(t)
            
    def join(self, use_best=False):
        self.coord.join(self.actor_threads)
        if use_best:
            self.actors[0].load_best()
        
    def get_histories(self):
        hists = []
        
        for actor in self.actors[self.start:]:
            hists.append(actor.hist)
            
        return hists
    
    def get_val_history(self):
        return self.actors[0].hist
    
    def save_results(self, filename):
        self.global_actor.save(filename)
    
    def save_actors(self, filename):
        for i in range(len(self.actors)):
            self.actors[i].save(filename%(i,))
            
    def get_final_actor(self):
        return self.global_actor
    
    def get_glb_r(self):
        return Actor.GLB_RUN_R
    