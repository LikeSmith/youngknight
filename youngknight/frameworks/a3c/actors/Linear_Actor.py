"""
Linear_Actor.py

Actor that impliments a Linear policy network
"""

import tensorflow as tf

from .Actor import Actor
from ....core import activ_deserialize

class Linear_Actor(Actor):
    def build_network(self, inputs=None):        
        with tf.variable_scope(self.name) as scope:
            if inputs is None:
                inputs = {}
                inputs['s'] = tf.placeholder('float', [None, self.state_size[0]], 's')
                inputs['R'] = tf.placeholder('float', [None, 1], 'R')
                inputs['a'] = tf.placeholder('float', [None, self.action_size[0]], 'a')
            assert 's' in inputs.keys() and 'R' in inputs.keys() and 'a' in inputs.keys()
            
            K_init = tf.random_normal_initializer(-0.0, 0.5)
            w_init = tf.random_normal_initializer(0.0, 0.01)
            outputs = {}
            losses = {}
            grads = {}
            
            with tf.variable_scope('policy'):
                mu = tf.layers.dense(inputs['s'], self.action_size[0], kernel_initializer=K_init, name='mu', use_bias=self.params['use_bias'])
                if 'hidden_size_var' in self.params.keys():
                    h_sig = tf.layers.dense(inputs['s'], self.params['hidden_size_var'], activ_deserialize(self.params['activ_var']), kernel_initializer=w_init, name='h_pol', use_bias=self.params['use_bias'])
                    sig = tf.layers.dense(h_sig, self.action_size[0], tf.nn.softplus, kernel_initializer=w_init, name='sig')
                else:
                    sig = tf.layers.dense(inputs['s'], self.action_size[0], tf.nn.softplus, kernel_initializer=w_init, name='sig')
                
            with tf.variable_scope('value'):
                h_val = tf.layers.dense(inputs['s'], self.params['hidden_size_value'], activ_deserialize(self.params['activ_value']), kernel_initializer=w_init, name='h_val')
                if 'dropout_rate_val' in self.params:
                    h_val = tf.nn.dropout(h_val, self.params['dropout_rate_val'])
                outputs['val'] = tf.layers.dense(h_val, 1, kernel_initializer=w_init, name='val')
            
            adv = tf.subtract(inputs['R'], outputs['val'], name='Advantage')
            
            with tf.name_scope('setup_dist'):
                sig = sig + self.params['sig_min']
            
            self.a_dist = tf.distributions.Normal(mu, sig)
            
            with tf.name_scope('choose_a'):
                outputs['pol'] = mu
                #outputs['pol_train'] = mu
                outputs['pol_train'] = tf.squeeze(self.a_dist.sample(1), axis=[0])
            
            weights = {}                
            weights['pol'] = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES, scope.name+'/policy')
            weights['val'] = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES, scope.name+'/value')
            
            with tf.name_scope('pol_loss'):
                log_prob = self.a_dist.log_prob(inputs['a'])
                exp_v = log_prob*tf.stop_gradient(adv)
                entropy = self.a_dist.entropy()
                loss_pol = self.params['beta_entropy']*entropy + exp_v
                losses['pol'] = tf.reduce_mean(-loss_pol)
                if self.params['use_l1_loss']:
                    for i in range(len(weights['pol'])):
                        losses['pol'] += 0.001*tf.reduce_sum(tf.abs(weights['pol'][i]))
                if self.params['use_l2_loss']:
                    for i in range(len(weights['pol'])):
                        losses['pol'] += tf.nn.l2_loss(weights['pol'][i])
            with tf.name_scope('val_loss'):
                losses['val'] = tf.reduce_mean(tf.square(adv))
                if self.params['use_l1_loss']:
                    for i in range(len(weights['val'])):
                        losses['pol'] += 0.001*tf.reduce_sum(tf.abs(weights['val'][i]))
                if self.params['use_l2_loss']:
                    for i in range(len(weights['val'])):
                        losses['val'] += tf.nn.l2_loss(weights['val'][i])
                
            with tf.name_scope('local_grads'):
                grads['pol'] = tf.gradients(losses['pol'], weights['pol'])
                grads['val'] = tf.gradients(losses['val'], weights['val'])
            
        return inputs, outputs, weights, losses, grads
    
    def build_sync(self, grads, trainers, glb_weights):
        assert len(self.weights['pol']) == len(glb_weights['pol'])
        assert len(self.weights['val']) == len(glb_weights['val'])
        
        pull_ops = {}
        push_ops = {}
        with tf.name_scope('sync'):
            with tf.name_scope('pull'):
                for i in range(len(self.weights['pol'])):
                    pull_ops['pol_%d'%i] = self.weights['pol'][i].assign(glb_weights['pol'][i])
                for i in range(len(self.weights['val'])):
                    pull_ops['val_%d'%i] = self.weights['val'][i].assign(glb_weights['val'][i])
                    
            with tf.name_scope('push'):
                push_ops['pol'] = self.trainers['pol'].apply_gradients(zip(grads['pol'], glb_weights['pol']))
                push_ops['val'] = self.trainers['val'].apply_gradients(zip(grads['val'], glb_weights['val']))
            
        return pull_ops, push_ops
        
    def loss_feed(self, R, s, a, s_1, s_int, k):
        return {self.inputs['s']:s, self.inputs['R']:R, self.inputs['a']:a}
        
    def set_trainers(self):
        trainers = {}
        trainers['pol'] = tf.train.RMSPropOptimizer(learning_rate=self.params['pol_learning_rate'])
        trainers['val'] = tf.train.RMSPropOptimizer(learning_rate=self.params['val_learning_rate'])
        return trainers
    
    def save_weights(self):
        weight_vals = {}
        for key, val in self.weights.items():
            weight_vals[key] = []
            for weight in val:
                weight_vals[key].append(self.sess.run(weight))
        return weight_vals
    
    def assign_weights(self, values):
        assign_ops = []
        for key in self.weights.keys():
            for i in range(len(self.weights[key])):
                assign_ops.append(self.weights[key][i].assign(values[key][i]))
        
        self.sess.run(assign_ops)