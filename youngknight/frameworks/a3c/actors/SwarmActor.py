"""
SwarmActor.py

Actor for swarm type systems
"""

import numpy as np
import tensorflow as tf

from .Actor import Actor
from ....core import Swarm

class SwarmActor(Actor):
    def __init__(self, sim, use_obs=False, coord=tf.train.Coordinator(), sess=None, params={}, load_file=None, home='', name='base_agent', debug=False, is_global=False, glb_actor=None):
        assert isinstance(sim, Swarm)
        
        self.abs_size = sim.abs_size
        self.aux_size = sim.aux_size
        
        if 'sim_params' not in params.keys():
            if 'swarm_size' in params.keys():
                params['sim_params'] = sim.param_gen(1, params['swarm_size'])
            elif 'max_swarm_size' in params.keys():
                params['sim_params'] = sim.param_gen(1, params['max_swarm_size'])
        
        super(SwarmActor, self).__init__(sim, use_obs=use_obs, coord=coord, sess=sess, params=params, load_file=load_file, home=home, name=name, debug=debug, is_global=is_global, glb_actor=glb_actor)
    
    def pol_feed(self, s, k, i=None):
        if i is None:
            return super(SwarmActor, self).pol_feed(s, k)
        else:
            return {self.inputs['s_i']:s}
        
    def gen_init_states(self):
        if 'swarm_size' in self.params.keys():
            s = self.sim.init_state_gen(1, self.params['swarm_size'])
        elif 'max_swarm_size' in self.params.keys() and 'min_swarm_size' in self.params.keys():
            s = self.sim.init_state_gen(1, np.random.randint(self.params['min_swarm_size'], self.params['max_swarm_size']))
        else:
            s = self.sim.init_state_gen(1, 10)
        o = self.sim.obs_gen(s, k=0, params=self.params['sim_params'])
        return (s, o)
            
    def policy(self, s, k, i=None):
        if i is None:
            if self.training:
                return self.sess.run(self.output_ops['pol_train'], feed_dict=self.pol_feed(s, k))
            else:
                return self.sess.run(self.output_ops['pol'], feed_dict=self.pol_feed(s, k))
        else:
            if self.training:
                if isinstance(self.output_ops['pol_i_train'], list):
                    return self.sess.run(self.output_ops['pol_i_train'][i], feed_dict=self.pol_feed(s, k, i))
                else:
                    return self.sess.run(self.output_ops['pol_i_train'], feed_dict=self.pol_feed(s, k, i))
            else:
                if isinstance(self.output_ops['pol_i'], list):
                    return self.sess.run(self.output_ops['pol_i'][i], feed_dict=self.pol_feed(s, k, i))
                else:
                    return self.sess.run(self.output_ops['pol_i'], feed_dict=self.pol_feed(s, k, i))
                