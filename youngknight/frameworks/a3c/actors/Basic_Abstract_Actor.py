'''
Basic Abstract Actor

This version of the abstract controller is dependent on the size of the swarm
parameters:
abs_size
aux_size
hidden_size_prnt_ctrl
activ_prnt_ctrl_h
activ_prnt_ctrl_f
hidden_size_abs_cur
activ_abs_cur_h
activ_abs_cur_f
hidden_size_abs_aux
activ_abs_aux_h
activ_abs_aux_f
hidden_size_pred
activ_pred_h
activ_pred_f
hidden_size_chld_ctrl
activ_chld_ctrl_h
activ_chld_ctrl_mu
activ_chld_ctrl_sig
hidden_size_val
activ_val_h
activ_val_f
sig_min
gamma
beta_1
beta_2
beta_3
beta_4
pol_learning_rate
val_learning_rate
verbosity
t_max
T_max
'''

import numpy as np
import tensorflow as tf

from .AbstractActor import AbstractActor
from ....core import activ_deserialize

class Basic_Abs_Actor(AbstractActor):
    def build_network(self, inputs=None):
        swarm_size = self.state_size[1][0]
        with tf.variable_scope(self.name) as scope:
            if inputs is None:
                inputs = {}
                inputs['s_prnt'] = tf.placeholder('float32', [None, self.state_size[0][0]], 's_prnt')
                inputs['s_chld'] = tf.placeholder('float32', [None, swarm_size, self.state_size[1][1]], 's_chld')
                inputs['v_t'] = tf.placeholder('float32', [None, 1], 'v_t')
                inputs['a'] = tf.placeholder('float32', [None, swarm_size, self.action_size[1]], 'a')
                inputs['s_p_1'] = tf.placeholder('float32', [None, self.state_size[0][0]], 's_p_1')
                
            w_init = tf.random_normal_initializer(0.0, 0.1)
            abs_states = {}
            outputs = {}
            losses = {}
            grads = {}
            
            with tf.variable_scope('policy'):
                # Calculate abstract states
                h_des = tf.layers.dense(inputs['s_prnt'], self.params['hidden_size_prnt_ctrl'], activ_deserialize(self.params['activ_prnt_ctrl_h']), kernel_initializer=w_init, name='h_des')
                abs_states['des'] = tf.layers.dense(h_des, self.params['abs_size'], activ_deserialize(self.params['activ_prnt_ctrl_f']), kernel_initializer=w_init, name='abs_des')
                
                s_c_flat = tf.layers.Flatten()(inputs['s_chld'])
                h_cur = tf.layers.dense(s_c_flat, self.params['hidden_size_abs_cur'], activ_deserialize(self.params['activ_abs_cur_h']), kernel_initializer=w_init, name='h_cur')
                abs_states['cur'] = tf.layers.dense(h_cur, self.params['abs_size'], activ_deserialize(self.params['activ_abs_cur_f']), kernel_initializer=w_init, name='abs_cur')
                
                h_aux = tf.layers.dense(s_c_flat, self.params['hidden_size_abs_aux'], activ_deserialize(self.params['activ_abs_aux_h']), kernel_initializer=w_init, name='h_aux')
                abs_states['aux'] = tf.layers.dense(h_aux, self.params['aux_size'], activ_deserialize(self.params['activ_abs_aux_f']), kernel_initializer=w_init, name='abs_aux')
                
                # Use abstract state to predict next parent state
                comb_state = tf.concat([inputs['s_prnt'], abs_states['cur']], axis=1, name='prd_comb')
                h_pred = tf.layers.dense(comb_state, self.params['hidden_size_pred'], activ_deserialize(self.params['activ_pred_h']), kernel_initializer=w_init, name='h_pred')
                s_pred = tf.layers.dense(h_pred, self.state_size[0][0], activ_deserialize(self.params['activ_pred_f']), kernel_initializer=w_init, name='s_pred')
                
                # Calculate policy now
                mu = []
                sig = []
                for i in range(swarm_size):
                    if self.params['use_err']:
                        concat = tf.concat([inputs['s_chld'][:, i, :], abs_states['cur']-abs_states['des'], abs_states['aux']], axis=1, name='chld_comb_%d'%(i,))
                    else:
                        concat = tf.concat([inputs['s_chld'][:, i, :], abs_states['cur'], abs_states['des'], abs_states['aux']], axis=1, name='chld_comb_%d'%(i,))
                    h_i = tf.layers.dense(concat, self.params['hidden_size_chld_ctrl'], activ_deserialize(self.params['activ_chld_ctrl_h']), kernel_initializer=w_init, name='h_%d'%(i,))
                    mu.append(tf.layers.dense(h_i, self.action_size[1], activ_deserialize(self.params['activ_chld_ctrl_mu']), kernel_initializer=w_init, name='mu_%d'%(i,)))
                    sig.append(tf.layers.dense(h_i, self.action_size[1], activ_deserialize(self.params['activ_chld_ctrl_sig']), kernel_initializer=w_init, name='sig_%d'%(i,)) + self.params['sig_min'])
                    
            mu = tf.stack(mu, axis=1, name='mu_stacked')
            sig = tf.stack(sig, axis=1, name='sig_stacked')
            self.a_dist = tf.distributions.Normal(mu, sig)
                
            with tf.variable_scope('choose_a'):
                outputs['pol_train'] = tf.squeeze(self.a_dist.sample(1), axis=0)*self.params['beta_4']
                outputs['pol'] = mu*self.params['beta_4']
                
            # Calculate Value
            with tf.variable_scope('value'):
                full_state = tf.concat([inputs['s_prnt'], tf.layers.Flatten()(inputs['s_chld'])], axis=1)
                h_val = tf.layers.dense(full_state, self.params['hidden_size_val'], activ_deserialize(self.params['activ_val_h']))
                outputs['val'] = tf.layers.dense(h_val, 1, activ_deserialize(self.params['activ_val_f']))
                
            # get weights
            weights = {}                
            weights['pol'] = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES, scope.name+'/policy')
            weights['val'] = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES, scope.name+'/value')
            
            # Calculate Loss
            adv = tf.subtract(inputs['v_t'], outputs['val'], name='adv')
            adv = tf.tile(tf.expand_dims(adv, axis=1), (1, swarm_size, 1))
            
            with tf.name_scope('pol_loss'):
                log_prob = self.a_dist.log_prob(inputs['a'])
                exp_v = log_prob*tf.stop_gradient(adv)
                entropy = self.a_dist.entropy()
                loss_pol = self.params['beta_1']*entropy + exp_v
                abs_err = self.params['beta_2']*tf.squared_difference(abs_states['cur'], abs_states['des'], name='abs_err')
                prd_err = self.params['beta_3']*tf.squared_difference(s_pred, inputs['s_p_1'], name='prd_err')
                losses['pol'] = tf.reduce_mean(-loss_pol) + tf.reduce_mean(abs_err) + tf.reduce_mean(prd_err)
            with tf.name_scope('val_loss'):
                losses['val'] = tf.reduce_mean(tf.square(adv))
                
            with tf.name_scope('local_grads'):
                grads['pol'] = tf.gradients(losses['pol'], weights['pol'])
                grads['val'] = tf.gradients(losses['val'], weights['val'])
                
        self.abs_states = abs_states
        return inputs, outputs, weights, losses, grads
        
    def build_sync(self, grads, trainers, glb_weights):
        assert len(self.weights['pol']) == len(glb_weights['pol'])
        assert len(self.weights['val']) == len(glb_weights['val'])
        
        pull_ops = {}
        push_ops = {}
        with tf.name_scope('sync'):
            with tf.name_scope('pull'):
                for i in range(len(self.weights['pol'])):
                    pull_ops['pol_%d'%i] = self.weights['pol'][i].assign(glb_weights['pol'][i])
                for i in range(len(self.weights['val'])):
                    pull_ops['val_%d'%i] = self.weights['val'][i].assign(glb_weights['val'][i])
                    
            with tf.name_scope('push'):
                push_ops['pol'] = trainers['pol'].apply_gradients(zip(grads['pol'], glb_weights['pol']))
                push_ops['val'] = trainers['val'].apply_gradients(zip(grads['val'], glb_weights['val']))
            
        return pull_ops, push_ops
        
    def loss_feed(self, v_t, s, a, s_1, s_int):
        return {self.inputs['s_prnt']:s[0], self.inputs['s_chld']:s[1], self.inputs['v_t']:v_t, self.inputs['a']:a, self.inputs['s_p_1']:s_1[0]}
    
    def set_trainers(self):
        trainers = {}
        trainers['pol'] = tf.train.AdamOptimizer(learning_rate=self.params['pol_learning_rate'])
        trainers['val'] = tf.train.AdamOptimizer(learning_rate=self.params['val_learning_rate'])
        return trainers

