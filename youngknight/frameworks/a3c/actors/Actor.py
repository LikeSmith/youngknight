"""
Agent.py

base class for Agents in the A3C framework
"""

import numpy as np
import tensorflow as tf
from tensorflow.python import debug as tf_debug
import gym

from youngknight.core import Model

class Actor(Model):
    T = 0
    UPDATE = 0
    GLB_RUN_R = []
    
    def __init__(self, sim=None, env_name=None, use_obs=False, coord=tf.train.Coordinator(), sess=None, params={}, load_file=None, home='', name='base_agent', debug=False, is_global=False, glb_actor=None):
        self.home = home
        self.name = name
        self.coord = coord
        self.is_global = is_global
        self.use_obs = use_obs
        self.hist = {}
        self.glb_actor = glb_actor
        
        if sim is not None:
            self.sim = sim
            self.use_gym = False
            if self.use_obs:
                self.state_size = sim.obs_size
            else:
                self.state_size = sim.state_size
            self.action_size = sim.action_size
        elif env_name is not None:
            self.env = gym.make(env_name).unwrapped
            self.use_gym = True
            self.state_size = (self.env.observation_space.shape[0],)
            self.action_size = (self.env.action_space.shape[0],)
        else:
            raise NoSimDefined()
            
        if load_file is not None:
            self.params, values = self.load(load_file, assign=False)
        else:
            self.params = params
            
        if glb_actor is None:
            self.trainers = self.set_trainers()
        else:
            self.trainers = glb_actor.get_trainers()
        
        if 'sim_params' not in self.params.keys() and not self.use_gym:
            self.params['sim_params'] = self.sim.param_gen(1)
        
        self.inputs, self.output_ops, self.weights, self.loss_ops, self.grads = self.build_network()
        
        if glb_actor is not None:
            self.glb_weights = glb_actor.get_weights()
        else:
            self.glb_weights = self.weights
            
        self.pull_ops, self.push_ops = self.build_sync(self.grads, self.trainers, self.glb_weights)
            
        if 'pol_train' not in self.output_ops.keys():
            self.output_ops['pol_train'] = self.output_ops['pol']
        if 'val_train' not in self.output_ops.keys():
            self.output_ops['val_train'] = self.output_ops['val']
        
        if sess is None:
            self.sess = tf.Session()
            if debug:
                self.sess = tf_debug.LocalCLIDebugWrapperSession(self.sess)
            self.sess.run(tf.global_variables_initializer())
        else:
            self.sess = sess
        
        if load_file is not None:
            self.assign_weights(values)
        self.training = False
        self.reset()
        
    def build_network(self, inputs=None,):
        raise NotImplementedError
        
    def build_sync(self, grads, trainers, glb_weights):
        raise NotImplementedError
        
    def loss_feed(self, R, s, a, s_1, s_int, k):
        raise NotImplementedError
    
    def set_trainers(self):
        raise NotImplementedError
        
    def val_feed(self, s, k):
        return {self.inputs['s']:s}
    
    def pol_feed(self, s, k):
        return {self.inputs['s']:s}
    
    def get_internal_states(self):
        return None
    
    def set_internal_states(self, states):
        pass
    
    def reset(self, ep_n=0):
        pass
    
    def gen_init_states(self):
        if self.use_gym:
            s = np.array([self.env.reset()])
            o = s
        else:
            s = self.sim.init_state_gen(1)
            o = self.sim.obs_gen(s, k=0, params=self.params['sim_params'])
        return (s, o)
    
    def get_trainers(self):
        return self.trainers
        
    def get_weights(self):
        return self.weights
    
    def get_T(self):
        return Actor.T
    
    def value(self, s, k):
        if self.training:
            return self.sess.run(self.output_ops['val_train'], feed_dict=self.val_feed(s, k))
        else:
            return self.sess.run(self.output_ops['val'], feed_dict=self.val_feed(s, k))
        
    def policy(self, s, k):
        if self.training:
            return self.sess.run(self.output_ops['pol_train'], feed_dict=self.pol_feed(s, k))
        else:
            return self.sess.run(self.output_ops['pol'], feed_dict=self.pol_feed(s, k))
        
    def pull(self):
        if not self.is_global:
            self.sess.run(self.pull_ops)
    
    def push(self, R, s, a, s_1, s_int, k):
        if not self.is_global:
            self.sess.run(self.push_ops, feed_dict=self.loss_feed(R, s, a, s_1, s_int, k))
            Actor.UPDATE += 1
        
    def sync(self, R, s, a, s_1, s_int, k):
        self.push(R, s, a, s_1, s_int, k)
        self.pull()
        
    def end(self):
        return Actor.T >= self.params['T_max'] or self.coord.should_stop()
    
    def tick(self):
        Actor.T += 1
        
    def load_best(self):
        self.glb_actor.assign_weights(self.best_weights)
    
    def validate(self):
        self.hist['ep_r'] = []
        self.hist['running_ep_r'] = None
        self.hist['ep_stamp'] = None
        self.hist['abs_cur'] = []
        self.hist['abs_des'] = []
        self.hist['abs_aux'] = []
        self.hist['s'] = []
        self.hist['o'] = []
        self.hist['a'] = []
        self.hist['r'] = []
        this_ep = -1
        self.training = False
        best_r = -np.inf
        self.best_weights = self.save_weights()
        es_counter = 0
        
        if self.params['verbosity'] >= 1:
            print('validation actor started!')
            
        while not self.end():
            while Actor.UPDATE < self.params['validation_rate'] and not self.end():
                pass
            
            self.reset()
            self.pull()
            this_ep = Actor.T
            t = 0
            Actor.UPDATE = 0
            
            s, o = self.gen_init_states()
            
            s_hist = []
            o_hist = []
            r_hist = []
            a_hist = []
            
            ep_r = 0.0
            
            done = False
            
            while not done:
                if self.use_obs:
                    a = self.policy(o, t)
                else:
                    a = self.policy(s, t)
                
                if self.use_gym:
                    s_, r, d, _ = self.env.step(a[0, :])
                    s_ = np.array([s_])
                    r = np.array([r])
                    d = np.array([d])
                    o_ = s_
                elif hasattr(self, 'des_traj'):
                    s_, o_, r, d = self.sim.step(s, a, k=t, params=self.params['sim_params'], use_noise=self.params['use_noise'], des_traj=self.des_traj)
                else:
                    s_, o_, r, d = self.sim.step(s, a, k=t, params=self.params['sim_params'], use_noise=self.params['use_noise'])
                
                done = d[0] or t == self.params['t_max']-1
                
                ep_r += r[0]
                
                s_hist.append(s)
                o_hist.append(o)
                r_hist.append(r[0])
                a_hist.append(a)
                
                t += 1
                s = s_
                o = o_
            
            self.hist['ep_r'].append(ep_r)
            self.hist['a'].append(np.stack(a_hist, axis=0))
            self.hist['s'].append(np.stack(s_hist, axis=0))
            self.hist['o'].append(np.stack(o_hist, axis=0))
            self.hist['r'].append(np.array(r_hist))
            
            if self.hist['running_ep_r'] is None:
                self.hist['running_ep_r'] = [ep_r]
                self.hist['ep_stamp'] = [this_ep]
            else:
                self.hist['running_ep_r'].append(0.9*self.hist['running_ep_r'][-1] + 0.1*ep_r)
                self.hist['ep_stamp'].append(this_ep)
                
            if self.hist['running_ep_r'][-1] > best_r:
                best_r = self.hist['running_ep_r'][-1]
                self.best_weights = self.save_weights()
                es_counter = 0
            else:
                es_counter += 1
            
            if 'validation_patience' in self.params.keys() and es_counter >= self.params['validation_patience']:
                if self.params['verbosity'] >= 1:
                    print('Early Stop! Validation has stopped increasing')
                self.coord.request_stop()
                
            if self.params['verbosity'] >= 2:
                print('Validation has completed at episode %d, ep_r: %f'%(this_ep, self.hist['running_ep_r'][-1]))
            
        if self.params['verbosity'] >= 1:
            print('Validation is finished!')
        self.training = False
        
    def train(self):
        t = 1
        self.training = True
        self.hist = {}
        self.hist['ep_r'] = []
        self.hist['running_ep_r'] = None
        self.hist['a'] = []
        self.hist['s'] = []
        self.hist['o'] = []
        self.hist['r'] = []
        self.hist['l_p'] = []
        self.hist['l_v'] = []
        
        s_buff = []
        o_buff = []
        r_buff = []
        a_buff = []
        s_1_buff = []
        o_1_buff = []
        s_intern_buff = []
        k_buff = []
        
        ep_n = 0
        
        if self.params['verbosity'] >= 1:
            print('Actor %s is started!'%(self.name,))
        
        while not self.end():
            self.reset(ep_n)
                
            t_start = t
            s, o = self.gen_init_states()
            
            ep_r = 0.0
            s_hist = []
            o_hist = []
            a_hist = []
            r_hist = []
            
            done = False
            
            while not done:
                if self.use_obs:
                    a = self.policy(o, t-t_start)
                else:
                    a = self.policy(s, t-t_start)
                
                s_intern = self.get_internal_states()
                
                if self.use_gym:
                    s_, r, d, _ = self.env.step(a[0, :])
                    s_ = np.array([s_])
                    r = np.array([r])
                    d = np.array([d])
                    o_ = s_
                elif hasattr(self, 'des_traj'):
                    s_, o_, r, d = self.sim.step(s, a, k=t-t_start, params=self.params['sim_params'], use_noise=self.params['use_noise'], des_traj=self.des_traj)
                else:
                    s_, o_, r, d = self.sim.step(s, a, k=t-t_start, params=self.params['sim_params'], use_noise=self.params['use_noise'])
                    
                done = d[0] or t-t_start == self.params['t_max']-1
                
                s_hist.append(np.squeeze(s, axis=0))
                o_hist.append(np.squeeze(o, axis=0))
                a_hist.append(np.squeeze(a, axis=0))
                r_hist.append(r[0])
                
                ep_r += r[0]
                s_buff.append(np.squeeze(s, axis=0))
                o_buff.append(np.squeeze(o, axis=0))
                r_buff.append(r[0])
                a_buff.append(np.squeeze(a, axis=0))
                s_1_buff.append(np.squeeze(s_, axis=0))
                o_1_buff.append(np.squeeze(o_, axis=0))
                s_intern_buff.append(s_intern)
                k_buff.append(t-t_start)
                
                if t % self.params['global_update_rate'] == 0 or done:
                    if done:
                        R = 0.0
                    elif self.use_obs:
                        R = self.value(o, t-t_start)[0, 0]
                    else:
                        R = self.value(s, t-t_start)[0, 0]
                        
                    R_buff = []
                    
                    for r in r_buff[::-1]:
                        R = r + self.params['gamma']*R
                        R_buff.append(np.array([R]))
                        
                    R_buff.reverse()
                    
                    R_buff = np.stack(R_buff, axis=0)
                    s_buff = np.stack(s_buff, axis=0)
                    o_buff = np.stack(o_buff, axis=0)
                    a_buff = np.stack(a_buff, axis=0)
                    s_1_buff = np.stack(s_1_buff, axis=0)
                    o_1_buff = np.stack(o_1_buff, axis=0)
                    s_intern_buff = np.stack(s_intern_buff, axis=0)
                    
                    if self.use_obs:
                        self.sync(R_buff, o_buff, a_buff, o_1_buff, s_intern_buff, k_buff)
                        losses = self.sess.run(self.loss_ops, feed_dict=self.loss_feed(R_buff, o_buff, a_buff, o_1_buff, s_intern_buff, k_buff))
                    else:
                        self.sync(R_buff, s_buff, a_buff, s_1_buff, s_intern_buff, k_buff)
                        losses = self.sess.run(self.loss_ops, feed_dict=self.loss_feed(R_buff, s_buff, a_buff, s_1_buff, s_intern_buff, k_buff))
                    
                    s_buff = []
                    o_buff = []
                    r_buff = []
                    a_buff = []
                    s_1_buff = []
                    o_1_buff = []
                    s_intern_buff = []
                    k_buff = []
                    
                    self.hist['l_p'].append(losses['pol'])
                    self.hist['l_v'].append(losses['val'])
                t += 1
                s = s_
                o = o_
                s_intern = self.get_internal_states()
            
            self.hist['ep_r'].append(ep_r)
            self.hist['s'].append(s_hist)
            self.hist['o'].append(o_hist)
            self.hist['a'].append(a_hist)
            self.hist['r'].append(r_hist)
            
            if self.hist['running_ep_r'] is None:
                self.hist['running_ep_r'] = [ep_r]
            else:
                self.hist['running_ep_r'] += [0.9*self.hist['running_ep_r'][-1] + 0.1*ep_r]
            
            if len(Actor.GLB_RUN_R) == 0:
                Actor.GLB_RUN_R += [ep_r]
            else:
                Actor.GLB_RUN_R += [Actor.GLB_RUN_R[-1]*0.9 + ep_r*0.1]
                
            if self.params['verbosity'] >= 2:
                print('Actor %s completed episode %d, ep_r: %f'%(self.name, Actor.T, Actor.GLB_RUN_R[-1]))
            self.tick()
            
            ep_n += 1
            
        if self.params['verbosity'] >= 1:
            print('Actor %s is finished after %d timesteps!'%(self.name, t))
        self.training = False
    
class NoSimDefined(Exception):
    pass
