"""
Linear_SwarmActor.py

Learns linear controller for swarm
"""

import tensorflow as tf
import numpy as np

from .SwarmActor import SwarmActor
from ....core import activ_deserialize

class Linear_SwarmActor_AbsTrack(SwarmActor):
    def build_network(self, inputs=None):
        self.des_traj = self.params['des_traj'].spawn_trajectory()
            
        with tf.variable_scope(self.name) as scope:
            if inputs is None:
                inputs = {}
                inputs['s'] = tf.placeholder('float', [None, self.params['swarm_size'], self.state_size[0]], 's')
                inputs['a_cur'] = tf.placeholder('float', [None, self.abs_size[0]], 'a_cur')
                inputs['a_des'] = tf.placeholder('float', [None, self.abs_size[0]*(self.des_traj.dirs+1)], 'a_des')
                inputs['R'] = tf.placeholder('float', [None, 1], 'R')
                inputs['a'] = tf.placeholder('float', [None, None, self.action_size[0]], 'a')
                inputs['s_i'] = tf.placeholder('float', [None, self.state_size[0]], 's_i')
            assert 's' in inputs.keys() and 'R' in inputs.keys() and 'a' in inputs.keys() and 's_i' in inputs.keys()
            
            w_init = tf.random_normal_initializer(0.0, 0.1)
            outputs = {}
            losses = {}
            grads = {}
            
            mu = []
            sig = []
            mu_i = []
            sig_i = []
            
            a_err = tf.subtract(inputs['a_des'][:, 0::self.abs_size[0]], inputs['a_cur'], name='abstract_error')
            dirs = []
            for i in range(self.params['dirs']):
                dirs.append(inputs['a_des'][:, i+1::self.abs_size[0]])
            dirs_all = tf.concat(dirs, axis=1)
            
            with tf.variable_scope('policy'):
                for i in range(self.params['swarm_size']):
                    concat_state = tf.concat((inputs['s'][:, i, :], a_err, dirs_all), axis=1, name='concat%d'%(i,))
                    concat_state_i = tf.concat((inputs['s_i'], a_err, dirs_all), axis=1, name='concat%d'%(i,))
                    
                    mu.append(tf.layers.dense(concat_state, self.action_size[0], kernel_initializer=w_init, name='mu%d'%(i,), use_bias=self.params['use_bias_pol']))
                    sig.append(tf.layers.dense(concat_state, self.action_size[0], tf.nn.softplus, kernel_initializer=w_init, name='sig%d'%(i,), use_bias=self.params['use_bias_pol']))
                    
                    mu_i.append(tf.layers.dense(concat_state_i, self.action_size[0], kernel_initializer=w_init, name='mu%d'%(i,), use_bias=self.params['use_bias_pol'], reuse=True))
                    sig_i.append(tf.layers.dense(concat_state_i, self.action_size[0], tf.nn.softplus, kernel_initializer=w_init, name='sig%d'%(i,), use_bias=self.params['use_bias_pol'], reuse=True))
                    
            
                    
            with tf.variable_scope('value'):
                concat_val = tf.concat((a_err, dirs_all), axis = 1, name='concat_val')
                
                h_val = tf.layers.dense(concat_val, self.params['hidden_size_value'], activ_deserialize(self.params['activ_value']), kernel_initializer=w_init, name='h_val', use_bias=self.params['use_bias_val'])
                outputs['val'] = tf.layers.dense(h_val, 1, kernel_initializer=w_init, name='val', use_bias=self.params['use_bias_val'])
            
            with tf.name_scope('setup_dist'):
                for i in range(self.params['swarm_size']):
                    sig[i] = sig[i] + self.params['sig_min']
                    sig_i[i] = sig_i[i] + self.params['sig_min']
                    
            mu = tf.stack(mu, axis=1, name='mu')
            sig = tf.stack(sig, axis=1, name='sig')
        
            self.a_dist = tf.distributions.Normal(mu, sig)
            self.a_i_dist = []
            for i in range(self.params['swarm_size']):
                self.a_i_dist.append(tf.distributions.Normal(mu_i[i], sig_i[i]))
            
            with tf.name_scope('choose_a'):
                outputs['pol'] = mu
                outputs['pol_i'] = mu_i
                
                a = tf.squeeze(self.a_dist.sample(1), axis=[0])
                a_i = []
                for i in range(self.params['swarm_size']):
                    a_i.append(tf.squeeze(self.a_i_dist[i].sample(1), axis=[0]))
                
                outputs['pol_train'] = a
                outputs['pol_i_train'] = a_i
            
            weights = {}
            weights['pol'] = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES, scope.name+'/policy')
            weights['val'] = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES, scope.name+'/value')
            
            adv = tf.subtract(inputs['R'], outputs['val'], name='Advantage')
            adv_stack = []
            for i in range(self.params['swarm_size']):
                adv_stack.append(tf.expand_dims(adv, axis=1))
            adv_stack = tf.concat(adv_stack, axis=1)
            
            with tf.name_scope('pol_loss'):
                log_prob = self.a_dist.log_prob(inputs['a'])
                exp_v = log_prob*tf.stop_gradient(adv_stack)
                entropy = self.a_dist.entropy()
                loss_pol = self.params['beta_entropy']*entropy + exp_v
                losses['pol'] = tf.reduce_mean(-loss_pol)
                if self.params['use_l1_loss_pol']:
                    for i in range(len(weights['pol'])):
                        losses['pol'] += self.params['reg_param']*tf.reduce_sum(tf.abs(weights['pol'][i]))
                if self.params['use_l2_loss_pol']:
                    for i in range(len(weights['pol'])):
                        losses['pol'] += self.params['reg_param']*tf.nn.l2_loss(weights['pol'][i])
            with tf.name_scope('val_loss'):
                losses['val'] = tf.reduce_mean(tf.square(adv))
                if self.params['use_l1_loss_val']:
                    for i in range(len(weights['val'])):
                        losses['pol'] += self.params['reg_param']*tf.reduce_sum(tf.abs(weights['val'][i]))
                if self.params['use_l2_loss_val']:
                    for i in range(len(weights['val'])):
                        losses['val'] += self.params['reg_param']*tf.nn.l2_loss(weights['val'][i])
                        
            with tf.name_scope('local_grads'):
                grads['pol'] = tf.gradients(losses['pol'], weights['pol'])
                grads['val'] = tf.gradients(losses['val'], weights['val'])
            
        return inputs, outputs, weights, losses, grads
    
    def build_sync(self, grads, trainers, glb_weights):
        assert len(self.weights['pol']) == len(glb_weights['pol'])
        assert len(self.weights['val']) == len(glb_weights['val'])
        
        pull_ops = {}
        push_ops = {}
        with tf.name_scope('sync'):
            with tf.name_scope('pull'):
                for i in range(len(self.weights['pol'])):
                    pull_ops['pol_%d'%i] = self.weights['pol'][i].assign(glb_weights['pol'][i])
                for i in range(len(self.weights['val'])):
                    pull_ops['val_%d'%i] = self.weights['val'][i].assign(glb_weights['val'][i])
                    
            with tf.name_scope('push'):
                push_ops['pol'] = self.trainers['pol'].apply_gradients(zip(grads['pol'], glb_weights['pol']))
                push_ops['val'] = self.trainers['val'].apply_gradients(zip(grads['val'], glb_weights['val']))
            
        return pull_ops, push_ops
        
    def loss_feed(self, R, s, a, s_1, s_int, k):
        a_des = np.zeros((len(k), self.abs_size[0]*(self.des_traj.dirs+1)))
        a_cur = np.zeros((len(k), self.abs_size[0]))
        
        for i in range(len(k)):
            a_des[i, :] = self.des_traj(k[i])
            a_cur[i, :] = self.sim.calc_abs_state(s[i:i+1, :, :], k[i], self.params['sim_params'])
        
        return {self.inputs['s']:s, self.inputs['R']:R, self.inputs['a']:a, self.inputs['a_des']:a_des, self.inputs['a_cur']:a_cur}
    
    def set_trainers(self):
        trainers = {}
        trainers['pol'] = tf.train.RMSPropOptimizer(learning_rate=self.params['pol_learning_rate'])
        trainers['val'] = tf.train.RMSPropOptimizer(learning_rate=self.params['val_learning_rate'])
        return trainers
    
    def save_weights(self):
        weight_vals = {}
        for key, val in self.weights.items():
            weight_vals[key] = []
            for weight in val:
                weight_vals[key].append(self.sess.run(weight))
        return weight_vals
    
    def assign_weights(self, values):
        assign_ops = []
        for key in self.weights.keys():
            for i in range(len(self.weights[key])):
                assign_ops.append(self.weights[key][i].assign(values[key][i]))
        
        self.sess.run(assign_ops)
        
    def pol_feed(self, s, k, i=None, abs_state=None, aux_state=None, des_traj=None):
        if i is None:
            if des_traj is None:
                return {self.inputs['s']:s, self.inputs['a_cur']:self.sim.calc_abs_state(s, k, self.params['sim_params']), self.inputs['a_des']:self.des_traj(k)}
            else:
                return {self.inputs['s']:s, self.inputs['a_cur']:self.sim.calc_abs_state(s, k, self.params['sim_params']), self.inputs['a_des']:des_traj(k)}
        elif aux_state is None:
            if des_traj is None:
                return {self.inputs['s_i']:s, self.inputs['a_cur']:abs_state, self.inputs['a_des']:self.des_traj(k)}
            else:
                return {self.inputs['s_i']:s, self.inputs['a_cur']:abs_state, self.inputs['a_des']:des_traj(k)}
        else:
            if des_traj is None:
                return {self.inputs['s_i']:s, self.inputs['a_cur']:abs_state, self.inputs['a_des']:self.des_traj(k), self.inputs['a_aux']:aux_state}
            else:
                return {self.inputs['s_i']:s, self.inputs['a_cur']:abs_state, self.inputs['a_des']:des_traj(k), self.inputs['a_aux']:aux_state}
        
    def val_feed(self, s, k, des_traj=None):
        if des_traj is None:
            return {self.inputs['a_cur']:self.sim.calc_abs_state(s, k, self.params['sim_params']), self.inputs['a_des']:self.des_traj(k)}
        else:
            return {self.inputs['a_cur']:self.sim.calc_abs_state(s, k, self.params['sim_params']), self.inputs['a_des']:des_traj(k)}
    
    def policy(self, s, k, i=None, abs_state=None, aux_state=None, des_traj=None):
        if i is None:
            if self.training:
                return self.sess.run(self.output_ops['pol_train'], feed_dict=self.pol_feed(s, k, des_traj=des_traj))
            else:
                return self.sess.run(self.output_ops['pol'], feed_dict=self.pol_feed(s, k, des_traj=des_traj))
        elif aux_state is None:
            if self.training:
                if isinstance(self.output_ops['pol_i_train'], list):
                    return self.sess.run(self.output_ops['pol_i_train'][i], feed_dict=self.pol_feed(s, k, i, abs_state, des_traj=des_traj))
                else:
                    return self.sess.run(self.output_ops['pol_i_train'], feed_dict=self.pol_feed(s, k, i, abs_state, des_traj=des_traj))
            else:
                if isinstance(self.output_ops['pol_i'], list):
                    return self.sess.run(self.output_ops['pol_i'][i], feed_dict=self.pol_feed(s, k, i, abs_state, des_traj=des_traj))
                else:
                    return self.sess.run(self.output_ops['pol_i'], feed_dict=self.pol_feed(s, k, i, abs_state, des_traj=des_traj))
        else:
            if self.training:
                if isinstance(self.output_ops['pol_i_train'], list):
                    return self.sess.run(self.output_ops['pol_i_train'][i], feed_dict=self.pol_feed(s, k, i, abs_state, aux_state, des_traj=des_traj))
                else:
                    return self.sess.run(self.output_ops['pol_i_train'], feed_dict=self.pol_feed(s, k, i, abs_state, aux_state, des_traj=des_traj))
            else:
                if isinstance(self.output_ops['pol_i'], list):
                    return self.sess.run(self.output_ops['pol_i'][i], feed_dict=self.pol_feed(s, k, i, abs_state, aux_state, des_traj=des_traj))
                else:
                    return self.sess.run(self.output_ops['pol_i'], feed_dict=self.pol_feed(s, k, i, abs_state, aux_state, des_traj=des_traj))
    
    def value(self, s, k, des_traj=None):
        if self.training:
            return self.sess.run(self.output_ops['val_train'], feed_dict=self.val_feed(s, k, des_traj))
        else:
            return self.sess.run(self.output_ops['val'], feed_dict=self.val_feed(s, k, des_traj))
    
    def reset(self):
        self.des_traj.reset()