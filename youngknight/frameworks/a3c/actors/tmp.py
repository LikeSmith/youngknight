'''
test
'''

import numpy as np
import tensorflow as tf

swarm_size = 10
batch_size = 3
state_size = 2

mu = tf.placeholder('float32', [None, swarm_size, state_size], 'mu')
sig = tf.placeholder('float32', [None, swarm_size, state_size], 'sig')
smp = tf.placeholder('float32', [None, swarm_size, state_size], 'smp')
'''
dists = []
a = []
log_probs = []
for i in range(swarm_size):
    dists.append(tf.distributions.Normal(mu[:, i, :], sig[:, i, :]))
    a.append(tf.squeeze(dists[i].sample(1), axis=0))
    log_probs.append(dists[i].log_prob(smp[:, i, :]))
    
samps = tf.stack(a, axis=1)
log_prob = tf.reduce_mean(tf.stack(log_probs, axis=1))
'''

dist = tf.distributions.Normal(mu, sig)
samps = tf.squeeze(dist.sample(1), axis=0)
log_prob = dist.log_prob(smp)
entropy = dist.entropy()

mu_val = np.arange(batch_size*swarm_size*state_size).reshape(batch_size, swarm_size, state_size)
sig_val = np.arange(batch_size*swarm_size*state_size).reshape(batch_size, swarm_size, state_size)*1e-3+1e-4

sess = tf.Session()

samp_val = sess.run(samps, feed_dict={mu:mu_val, sig:sig_val})
log_prob_val, entropy_val = sess.run([log_prob, entropy], feed_dict={mu:mu_val, sig:sig_val, smp:samp_val})

print(log_prob_val)
print(entropy_val)
