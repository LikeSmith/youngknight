"""
Abstraction Actor

This Actor acts on a parent child type system.  It generates two policies  and
two values, one for the parent system, one for the child swarm.  The child 
swarm states are channeled through abstract states that allow the parent system
to be discribed in terms of an abstraction of the swarm rather than directly
by the swarm state.
"""

import numpy as np
import tensorflow as tf

from .Actor import Actor
from ....core import ParentChild

class AbstractActor(Actor):
    def __init__(self, sim, use_obs=False, coord=tf.train.Coordinator(), sess=None, params={}, load_file=None, home='', name='base_agent', debug=False, is_global=False, glb_actor=None):
        assert isinstance(sim, ParentChild)
        
        super(AbstractActor, self).__init__(sim, use_obs=use_obs, coord=coord, sess=sess, params=params, load_file=load_file, home=home, name=name, debug=debug, is_global=is_global, glb_actor=glb_actor)
        
        if 'sim_prnt_params' not in self.params.keys():
            self.params['sim_prnt_params'] = self.sim.parent_param_gen(1)
        if 'sim_chld_params' not in self.params.keys():
            self.params['sim_chld_params'] = self.sim.child_param_gen(1)
        
    def abstract(self, s_p, s_c):
        return self.sess.run(self.abs_states, feed_dict=self.pol_feed([s_p, s_c]))
    
    def val_feed(self, s):
        s_p, s_c = s
        return {self.inputs['s_prnt']:s_p, self.inputs['s_chld']:s_c}
    
    def pol_feed(self, s):
        s_p, s_c = s
        return {self.inputs['s_prnt']:s_p, self.inputs['s_chld']:s_c}
        
    def train(self):
        t = 1
        self.training = True
        self.hist = {}
        self.hist['ep_r'] = []
        self.hist['running_ep_r'] = None
        self.hist['abs_cur'] = []
        self.hist['abs_des'] = []
        self.hist['abs_aux'] = []
        self.hist['s_p'] = []
        self.hist['s_c'] = []
        self.hist['a'] = []
        self.hist['r'] = []
        self.hist['l_p'] = []
        self.hist['l_v'] = []
        
        s_p_buff = []
        s_c_buff = []
        o_p_buff = []
        o_c_buff = []
        s_p_1_buff = []
        s_c_1_buff = []
        o_p_1_buff = []
        o_c_1_buff = []
        r_buff = []
        a_buff = []
        s_intern_buff = []
        
        if self.params['verbosity'] >= 1:
            print('Actor %s is started!'%(self.name,))
        
        while not self.end():
            self.reset()
            
            t_start = t
            s_p = self.sim.parent_init_state_gen(1)
            s_c = self.sim.child_init_state_gen(1)
            o_p = self.sim.parent_sim.obs_gen(s_p, k=0, params=self.params['sim_prnt_params'])
            o_c = self.sim.child_sim.obs_gen(s_c, k=0, params=self.params['sim_chld_params'])
            
            ep_r = 0.0
            
            done = False
            
            s_p_hist = [s_p]
            s_c_hist = [s_c]
            abs_cur_hist = []
            abs_des_hist = []
            abs_aux_hist = []
            r_hist = []
            a_hist = []
            
            while not done:
                if self.use_obs:
                    a = self.policy([o_p, o_c])
                else:
                    a = self.policy([s_p, s_c])
                
                if self.use_obs:
                    s_abs = self.abstract(o_p, o_c)
                else:
                    s_abs = self.abstract(s_p, s_c)
                
                s_intern = self.get_internal_states()
                
                s_p_, s_c_, o_p_, o_c_, r, d = self.sim.step(s_p, s_c, a, k=t-t_start, parent_params=self.params['sim_prnt_params'], child_params=self.params['sim_chld_params'], use_noise=self.params['use_noise'])
                
                done = d[0] or t-t_start == self.params['t_max']-1
                
                ep_r += r[0]
                s_p_buff.append(s_p)
                s_c_buff.append(s_c)
                o_p_buff.append(o_p)
                o_c_buff.append(o_c)
                s_p_1_buff.append(s_p_)
                s_c_1_buff.append(s_c_)
                o_p_1_buff.append(o_p_)
                o_c_1_buff.append(o_c_)
                r_buff.append(r[0])
                a_buff.append(a)
                s_intern_buff.append(s_intern)
                
                s_p_hist.append(s_p)
                s_c_hist.append(s_c)
                abs_cur_hist.append(s_abs['cur'])
                abs_des_hist.append(s_abs['des'])
                abs_aux_hist.append(s_abs['aux'])
                r_hist.append(r[0])
                a_hist.append(a)
                
                if t % self.params['global_update_rate'] == 0 or done:
                    if done:
                        R = 0.0
                    elif self.use_obs:
                        R = self.value([o_p, o_c])[0, 0]
                    else:
                        R = self.value([s_p, s_c])[0, 0]
                        
                    R_buff = []
                    
                    for r in r_buff[::-1]:
                        R = r + self.params['gamma']*R
                        R_buff.append(R)
                        
                    R_buff.reverse()
                    
                    R_buff = np.vstack(R_buff)
                    s_p_buff = np.vstack(s_p_buff)
                    s_c_buff = np.vstack(s_c_buff)
                    o_p_buff = np.vstack(o_p_buff)
                    o_c_buff = np.vstack(o_c_buff)
                    s_p_1_buff = np.vstack(s_p_1_buff)
                    s_c_1_buff = np.vstack(s_c_1_buff)
                    o_p_1_buff = np.vstack(o_p_1_buff)
                    o_c_1_buff = np.vstack(o_c_1_buff)
                    a_buff = np.vstack(a_buff)
                    s_intern_buff = np.vstack(s_intern_buff)
                    
                    if self.use_obs:
                        self.sync(R_buff, [o_p_buff, o_c_buff], a_buff, [o_p_1_buff, o_c_1_buff], s_intern_buff)
                        losses = self.sess.run(self.loss_ops, feed_dict=self.loss_feed(R_buff, [o_p_buff, o_c_buff], a_buff, [o_p_1_buff, o_c_1_buff], s_intern_buff))
                    else:
                        self.sync(R_buff, [s_p_buff, s_c_buff], a_buff, [s_p_1_buff, s_c_1_buff], s_intern_buff)
                        losses = self.sess.run(self.loss_ops, feed_dict=self.loss_feed(R_buff, [s_p_buff, s_c_buff], a_buff, [s_p_1_buff, s_c_1_buff], s_intern_buff))
                    
                    s_p_buff = []
                    s_c_buff = []
                    o_p_buff = []
                    o_c_buff = []
                    s_p_1_buff = []
                    s_c_1_buff = []
                    o_p_1_buff = []
                    o_c_1_buff = []
                    r_buff = []
                    a_buff = []
                    s_intern_buff = []
                    
                    self.hist['l_p'].append(losses['pol'])
                    self.hist['l_v'].append(losses['val'])
                t += 1
                s_p = s_p_
                s_c = s_c_
                o_p = o_p_
                o_c = o_c_
                s_intern = self.get_internal_states()
            
            self.hist['ep_r'].append(ep_r)
            self.hist['a'].append(np.stack(a_hist, axis=0))
            self.hist['s_p'].append(np.stack(s_p_hist, axis=0))
            self.hist['s_c'].append(np.stack(s_c_hist, axis=0))
            self.hist['abs_cur'].append(np.stack(abs_cur_hist, axis=0))
            self.hist['abs_des'].append(np.stack(abs_des_hist, axis=0))
            self.hist['abs_aux'].append(np.stack(abs_aux_hist, axis=0))
            self.hist['r'].append(np.array(r_hist))
            
            if self.hist['running_ep_r'] is None:
                self.hist['running_ep_r'] = [ep_r]
            else:
                self.hist['running_ep_r'] += [0.9*self.hist['running_ep_r'][-1] + 0.1*ep_r]
            
            if len(Actor.GLB_RUN_R) == 0:
                Actor.GLB_RUN_R += [ep_r]
            else:
                Actor.GLB_RUN_R += [Actor.GLB_RUN_R[-1]*0.9 + ep_r*0.1]
                
            if self.params['verbosity'] >= 2:
                print('Actor %s completed episode %d, ep_r: %f'%(self.name, Actor.T, Actor.GLB_RUN_R[-1]))
            self.tick()
            
        if self.params['verbosity'] >= 1:
            print('Actor %s is finished after %d timesteps!'%(self.name, t))
        self.training = False
        
    def validate(self):
        self.hist['ep_r'] = []
        self.hist['running_ep_r'] = None
        self.hist['ep_stamp'] = None
        self.hist['abs_cur'] = []
        self.hist['abs_des'] = []
        self.hist['abs_aux'] = []
        self.hist['s_p'] = []
        self.hist['s_c'] = []
        self.hist['a'] = []
        self.hist['r'] = []
        this_ep = -1
        self.training = False
        
        if self.params['verbosity'] >= 1:
            print('validation actor started!')
            
        while not self.end():
            while Actor.UPDATE < self.params['validation_rate'] and not self.end():
                pass
            
            self.reset()
            self.pull()
            this_ep = Actor.T
            t = 0
            Actor.UPDATE = 0
            
            s_p = self.sim.parent_init_state_gen(1)
            s_c = self.sim.child_init_state_gen(1)
            o_p = self.sim.parent_sim.obs_gen(s_p, k=0, params=self.params['sim_prnt_params'])
            o_c = self.sim.child_sim.obs_gen(s_c, k=0, params=self.params['sim_chld_params'])
            
            s_p_hist = [s_p]
            s_c_hist = [s_c]
            abs_cur_hist = []
            abs_des_hist = []
            abs_aux_hist = []
            r_hist = []
            a_hist = []
            
            ep_r = 0.0
            
            done = False
            
            while not done:
                if self.use_obs:
                    a = self.policy([o_p, o_c])
                else:
                    a = self.policy([s_p, s_c])
                
                if self.use_obs:
                    s_abs = self.abstract(o_p, o_c)
                else:
                    s_abs = self.abstract(s_p, s_c)
                
                s_p_, s_c_, o_p_, o_c_, r, d = self.sim.step(s_p, s_c, a, k=t, parent_params=self.params['sim_prnt_params'], child_params=self.params['sim_chld_params'], use_noise=self.params['use_noise'])
                
                done = d[0] or t == self.params['t_max']-1
                
                ep_r += r[0]
                
                s_p_hist.append(s_p)
                s_c_hist.append(s_c)
                abs_cur_hist.append(s_abs['cur'])
                abs_des_hist.append(s_abs['des'])
                abs_aux_hist.append(s_abs['aux'])
                r_hist.append(r[0])
                a_hist.append(a)
                
                t += 1
                s_p = s_p_
                s_c = s_c_
                o_p = o_p_
                o_c = o_c_
            
            self.hist['ep_r'].append(ep_r)
            self.hist['a'].append(np.stack(a_hist, axis=0))
            self.hist['s_p'].append(np.stack(s_p_hist, axis=0))
            self.hist['s_c'].append(np.stack(s_c_hist, axis=0))
            self.hist['abs_cur'].append(np.stack(abs_cur_hist, axis=0))
            self.hist['abs_des'].append(np.stack(abs_des_hist, axis=0))
            self.hist['abs_aux'].append(np.stack(abs_aux_hist, axis=0))
            self.hist['r'].append(np.array(r_hist))
            
            if self.hist['running_ep_r'] is None:
                self.hist['running_ep_r'] = [ep_r]
                self.hist['ep_stamp'] = [this_ep]
            else:
                self.hist['running_ep_r'].append(0.9*self.hist['running_ep_r'][-1] + 0.1*ep_r)
                self.hist['ep_stamp'].append(this_ep)
                
            if self.params['verbosity'] >= 2:
                print('Validation has completed at episode %d, ep_r: %f'%(this_ep, self.hist['running_ep_r'][-1]))
            self.tick()
            
        if self.params['verbosity'] >= 1:
            print('Validation is finished!')
        self.training = False