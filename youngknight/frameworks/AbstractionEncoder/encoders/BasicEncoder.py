"""
BasicEncoder.py

Basic method of encoding abstract state
"""

import tensorflow as tf

from .AbstractEncoder import AbstractEncoder
from ....core import activ_deserialize

class BasicEncoder(AbstractEncoder):
    def build_network(self, inputs=None, weights=None, scope=None):
        if scope is None:
            scope = self.name
        else:
            scope = scope.name + self.name

        with tf.variable_scope(scope) as local_scope:
            if inputs is None:
                inputs = {}
                inputs['swrm_states'] = tf.placeholder('float', [None, self.params['max_swarm_size'], self.state_size], 'swrm_s')
                inputs['swrm_params'] = tf.placeholder('float', [None, self.params['max_swarm_size'], self.param_size], 'swrm_p')

                self.local_scope = local_scope
            else:
                assert 'swrm_states' in inputs.keys()
                assert 'swrm_params' in inputs.keys()

            if weights is None:
                try:
                    weights = self.weights
                except AttributeError:
                    w_init = tf.random_normal_initializer(0, self.params['init_var'])
                    weights = {}
                    weights['W1'] = tf.get_variable('W1', [(self.state_size+self.param_size)*self.params['max_swarm_size'], self.params['h_size']], initializer=w_init)
                    weights['b1'] = tf.get_variable('b1', self.params['h_size'], initializer=w_init)
                    weights['W2'] = tf.get_variable('W2', [self.params['h_size'], self.abs_size], initializer=w_init)
                    weights['b2'] = tf.get_variable('b2', self.abs_size, initializer=w_init)

            outputs = {}

            comb_in = tf.layers.flatten(tf.concat((inputs['swrm_states'], inputs['swrm_params']), axis=2))
            h = activ_deserialize(self.params['activ'])(tf.add(tf.matmul(comb_in, weights['W1']), weights['b1']))
            outputs['abst_states'] = tf.add(tf.matmul(h, weights['W2']), weights['b2'])

        return inputs, outputs, weights, {}, {}

    def build_abs(self, s, p):
        _, outputs, _, _, _ = self.build_network({'swrm_states':s, 'swrm_params':p}, self.weights)
        return outputs['abst_states']
