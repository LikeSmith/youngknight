"""
IdentityEncoder.py

Basic method of encoding abstract state
"""

import tensorflow as tf

from ..AbstractEncoder import AbstractEncoder

class IdentityEncoder(AbstractEncoder):
    def build_network(self, inputs=None, weights=None, scope=None):
        if scope is None:
            scope = self.name
        else:
            scope = scope.name + self.name

        self.abs_size =self.params['max_swarm_size']

        with tf.variable_scope(scope) as local_scope:
            if inputs is None:
                inputs = {}
                inputs['swrm_states'] = tf.placeholder('float', [None, self.params['max_swarm_size'], self.state_size], 'swrm_s')
                inputs['swrm_params'] = tf.placeholder('float', [None, self.params['max_swarm_size'], self.param_size], 'swrm_p')

                self.local_scope = local_scope
            else:
                assert 'swrm_states' in inputs.keys()
                assert 'swrm_params' in inputs.keys()

            if weights is None:
                try:
                    weights = self.weights
                except AttributeError:
                    weights = {}

            outputs = {}

            outputs['abst_states'] = inputs['swrm_states'][:, :, 0]

        return inputs, outputs, weights, {}, {}

    def build_abs(self, s, p):
        _, outputs, _, _, _ = self.build_network({'swrm_states':s, 'swrm_params':p}, self.weights)
        return outputs['abst_states']
