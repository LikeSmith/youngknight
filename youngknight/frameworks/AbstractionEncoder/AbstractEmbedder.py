"""
AbstractEmbedder.py

Network for learning an abstract state

need two models:
    abs_enc:
        must have member abs_size that is the size of the abstract state space
        must have member state_size that is the size of the swarm state space
        must have member param_size that is the number of parameters expected
        optional function disect_swarm(swarm_states, swarm_params) generates necessary subswarms
        inputs:
            swrm_states
        outputs:
            abst_states

    abs_dyn:
        must have member abs_size that is the size of the abstract state space
        must have member state_size that is the size of the parent state space
        inputs:
            prnt_states
            abst_states
        outputs:
            prnt_states

Parameters:
    max_swarm_size: The maximum swarm size to consider
    min_swarm_size: The minimum swarm size to consider (def=2)
    swarm_update_rate: number of epochs to use same swarm parameters and size (def=1)
    multiplicity: train dynamics on several different parent states for each given abstract state (optional)
    learning_rate: The learning rate
    n_epochs: number of epochs to train for
    n_batches: number of batches per epoch
    batch_size: size of each batch
    val_size: size of validation data
    variable_swarm_size: True if swarm size should vary, otherwise just uses max_swarm_size (def=False)
    swrm_state_gen: function for generating swarm state, takes in parameters batch_size and swarm_size
    swrm_param_gen: function for generating swarm parameters.  Takes in same parameters
    use_best: If True, will load wieghts that gave best validation result instead of keeping last values (def=False)
    verbosity: Reporting mode:
        0 - silent
        1 - only report summary after each epoch
        2 - use progress bar for each epoch, report at end
        3 - use progress bars for overall training and per epoch
"""

import tensorflow as tf
import numpy as np
from tqdm import trange
import math

class AbstractEmbedder(object):
    def __init__(self, abs_enc, abs_dyn, parent_step, params, name='AbstractEncoder'):
        self.abs_enc = abs_enc
        self.abs_dyn = abs_dyn
        self.parent_step = parent_step
        self.params = params
        self.name = name

        self.abst_state_size = self.abs_enc.abs_size
        self.swrm_state_size = self.abs_enc.state_size
        self.swrm_param_size = self.abs_enc.param_size
        self.prnt_state_size = self.abs_dyn.state_size
        self.prnt_param_size = self.abs_dyn.param_size

        if 'min_swarm_size' not in self.params.keys():
            self.params['min_swarm_size'] = 2
        if 'swarm_update_rate' not in self.params.keys():
            self.params['swarm_update_rate'] = 1
        if 'variable_swarm_size' not in self.params.keys():
            self.params['variable_swarm_size'] = False
        if 'use_best' not in self.params.keys():
            self.params['use_best'] = False
        if 'regen_data' not in self.params.keys():
            self.params['regen_data'] = True;

        self.setup_learning()

        self.sess = tf.Session()
        self.abs_enc.sess = self.sess
        self.abs_dyn.sess = self.sess
        self.sess.run(tf.global_variables_initializer())

    def setup_learning(self):
        with tf.variable_scope(self.name) as scope:
            self.cur_swrm_states = tf.placeholder('float', [None, self.params['max_swarm_size'], self.swrm_state_size], 's_swrm')
            self.sta_swrm_params = tf.placeholder('float', [None, self.params['max_swarm_size'], self.swrm_param_size], 'p_swrm')
            self.cur_prnt_states = tf.placeholder('float', [None, self.prnt_state_size], 's_prnt_0')
            self.nxt_prnt_states = tf.placeholder('float', [None, self.prnt_state_size], 's_prnt_1')
            self.sta_prnt_params = tf.placeholder('float', [None, self.prnt_param_size], 'p_prnt')
            self.training = tf.placeholder('bool', name='training')

            enc_inputs = {}
            enc_inputs['swrm_states'] = self.cur_swrm_states
            enc_inputs['swrm_params'] = self.sta_swrm_params
            enc_inputs['training'] = self.training

            _, enc_out, self.enc_weights, enc_losses, _ = self.abs_enc.build_network(enc_inputs, scope=scope)

            if 'sample_points' in self.abs_enc.params.keys():
                self.cur_abst_states = []
                for i in range(self.abs_enc.params['sample_points'].shape[0]+1):
                    self.cur_abst_states.append(enc_out['abst_states'][:, i, :])
                self.cur_abst_states = tf.concat(self.cur_abst_states, axis=0)
            else:
                self.cur_abst_states = enc_out['abst_states']

            if 'multiplicity' in self.params.keys():
                self.cur_abst_states = tf.tile(self.cur_abst_states, (self.params['multiplicity'], 1))

            dyn_inputs = {}
            dyn_inputs['prnt_states'] = self.cur_prnt_states
            dyn_inputs['prnt_params'] = self.sta_prnt_params
            dyn_inputs['abst_states'] = self.cur_abst_states
            dyn_inputs['training'] = self.training

            _, dyn_out, self.dyn_weights, dyn_losses, _ = self.abs_dyn.build_network(dyn_inputs, scope=scope)
            self.nxt_prnt_states_pred = dyn_out['prnt_states']

            self.loss = tf.reduce_mean(tf.squared_difference(self.nxt_prnt_states_pred, self.nxt_prnt_states))

            for key in enc_losses.keys():
                self.loss += enc_losses[key]
            for key in dyn_losses.keys():
                self.loss += dyn_losses[key]

            self.optimizer = tf.train.AdamOptimizer(learning_rate=self.params['learning_rate'])
            self.train_op = self.optimizer.minimize(self.loss)

    def train(self):
        hist = {}
        hist['ave_loss'] = np.zeros(self.params['n_epochs'])
        hist['val_loss'] = np.zeros(self.params['n_epochs'])

        min_weights_abs = {}
        min_weights_dyn = {}
        min_loss = math.inf
        min_count = 0

        if self.params['verbosity'] == 3:
            epochs = trange(self.params['n_epochs'])
            epochs.set_description('Val. Loss: N/A')
        else:
            epochs = range(self.params['n_epochs'])

        val_cur_swrm_states, val_cur_prnt_states, val_nxt_prnt_states, val_swrm_params, val_prnt_params = self.gen_dataset(self.params['val_size'], self.params['max_swarm_size'])

        if not self.params['regen_data']:
            cur_swrm_states = []
            cur_prnt_states = []
            nxt_prnt_states = []
            swrm_params = []
            prnt_params = []

            if self.params['verbosity'] > 0:
                print('Generating Training data...')
            if self.params['verbosity'] in [2, 3]:
                batches = trange(self.params['n_batches'])
            else:
                batches = range(self.params['n_batches'])

            for i in batches:
                if self.params['variable_swarm_size']:
                    swarm_size = np.random.rand(1)[0]*(self.params['max_swarm_size'] - self.params['min_swarm_size']) + self.params['min_swarm_size']
                else:
                    swarm_size = self.params['max_swarm_size']

                s_s_c, s_p_c, s_p_n, p_s, p_p = self.gen_dataset(self.params['batch_size'], swarm_size)
                cur_swrm_states.append(s_s_c)
                cur_prnt_states.append(s_p_c)
                nxt_prnt_states.append(s_p_n)
                swrm_params.append(p_s)
                prnt_params.append(p_p)

        for epoch in epochs:
            if self.params['verbosity'] in [1, 2]:
                print('Epoch %d/%d'%(epoch+1, self.params['n_epochs']))

            if self.params['verbosity'] in [2, 3]:
                batches = trange(self.params['n_batches'])
                batches.set_description('Ave. Loss: N/A')
            else:
                batches = range(self.params['n_batches'])

            for batch in batches:
                if self.params['regen_data']:
                    if batch%self.params['swarm_update_rate'] == 0:
                        if self.params['variable_swarm_size']:
                            swarm_size = np.random.rand(1)[0]*(self.params['max_swarm_size'] - self.params['min_swarm_size']) + self.params['min_swarm_size']
                        else:
                            swarm_size = self.params['max_swarm_size']

                        cur_swrm_states, cur_prnt_states, nxt_prnt_states, swrm_params, prnt_params = self.gen_dataset(self.params['batch_size'], swarm_size)
                    else:
                        cur_swrm_states, cur_prnt_states, nxt_prnt_states, swrm_params, prnt_params = self.gen_dataset(self.params['batch_size'], swarm_size, swrm_params, prnt_params)

                    loss, _ = self.sess.run([self.loss, self.train_op], feed_dict={self.cur_swrm_states:cur_swrm_states, self.sta_swrm_params:swrm_params, self.cur_prnt_states:cur_prnt_states, self.nxt_prnt_states:nxt_prnt_states, self.sta_prnt_params:prnt_params, self.training:True})
                else:
                    loss, _ = self.sess.run([self.loss, self.train_op], feed_dict={self.cur_swrm_states:cur_swrm_states[batch], self.sta_swrm_params:swrm_params[batch], self.cur_prnt_states:cur_prnt_states[batch], self.nxt_prnt_states:nxt_prnt_states[batch], self.sta_prnt_params:prnt_params[batch], self.training:True})

                hist['ave_loss'][epoch] += loss
                if self.params['verbosity'] in [2, 3]:
                    batches.set_description('Ave. Loss: %f'%(hist['ave_loss'][epoch]/(batch+1)))

            hist['ave_loss'][epoch] /= self.params['n_batches']
            hist['val_loss'][epoch] = self.sess.run(self.loss, feed_dict={self.cur_swrm_states:val_cur_swrm_states, self.sta_swrm_params:val_swrm_params, self.cur_prnt_states:val_cur_prnt_states, self.nxt_prnt_states:val_nxt_prnt_states, self.sta_prnt_params:val_prnt_params, self.training:False})
            if self.params['verbosity'] in [1, 2]:
                print('Epoch %d complete! Ave. Loss: %f, Val. Loss: %f'%(epoch+1, hist['ave_loss'][epoch], hist['val_loss'][epoch]))
            elif self.params['verbosity'] == 3:
                epochs.set_description('Val. Loss: %f'%hist['val_loss'][epoch])

            if hist['val_loss'][epoch] < min_loss:
                min_loss = hist['val_loss'][epoch]
                min_weights_abs = self.abs_enc.save_weights()
                min_weights_dyn = self.abs_dyn.save_weights()
                min_count = 0
            else:
                min_count += 1

            if 'patience' in self.params.keys() and min_count > self.params['patience']:
                print('Validation loss has stopped decreassing, stopping training early.')
                hist['ave_loss'] = hist['ave_loss'][:epoch+1]
                hist['val_loss'] = hist['val_loss'][:epoch+1]
                break;

        if self.params['use_best']:
            self.abs_enc.assign_weights(min_weights_abs)
            self.abs_dyn.assign_weights(min_weights_dyn)

        return hist

    def gen_dataset(self, batch_size, swrm_size, swrm_params=None, prnt_params=None):
        cur_swrm_states = np.zeros((batch_size, self.params['max_swarm_size'], self.swrm_state_size))
        cur_swrm_states[:, :swrm_size, :] = self.params['swrm_state_gen'](batch_size, swrm_size)

        if swrm_params is None:
            swrm_params = np.zeros((batch_size, self.params['max_swarm_size'], self.swrm_param_size))
            swrm_params[:, :swrm_size, :] = self.params['swrm_param_gen'](batch_size, swrm_size)

        sub_swrm_states, sub_swrm_params = self.abs_enc.disect_swarm(cur_swrm_states, swrm_params)
        n = len(sub_swrm_states)

        cur_prnt_states = []
        nxt_prnt_states = []

        if 'multiplicity' in self.params.keys():
            for i in range(self.params['multiplicity']):
                s_p = self.params['prnt_state_gen'](batch_size)
                if prnt_params is None:
                    p = self.params['prnt_param_gen'](batch_size)
                    prnt_params = []
                else:
                    p = prnt_params[i*batch_size:(i+1)*batch_size, :]

                for j in range(n):
                    s_p_1 = self.parent_step(s_p, sub_swrm_states[j], p, sub_swrm_params[j])
                    cur_prnt_states.append(s_p)
                    nxt_prnt_states.append(s_p_1)
                    if isinstance(prnt_params, list):
                        prnt_params.append(p)
        else:
            s_p = self.params['prnt_state_gen'](batch_size)
            if prnt_params is None:
                p = self.params['prnt_param_gen'](batch_size)
                prnt_params = []
            else:
                p = prnt_params[0:batch_size, :]
            for i in range(n):
                s_p_1 = self.parent_step(s_p, sub_swrm_states[i], p, sub_swrm_params[i])
                cur_prnt_states.append(s_p)
                nxt_prnt_states.append(s_p_1)
                if isinstance(prnt_params, list):
                        prnt_params.append(p)

        cur_prnt_states = np.concatenate(cur_prnt_states, axis=0)
        nxt_prnt_states = np.concatenate(nxt_prnt_states, axis=0)
        if isinstance(prnt_params, list):
            prnt_params = np.concatenate(prnt_params, axis=0)

        return (cur_swrm_states, cur_prnt_states, nxt_prnt_states, swrm_params, prnt_params)

    def encode(self, swrm_states, swrm_params):
        return self.abs_enc.encode(swrm_states, swrm_params)

    def step(self, prnt_states, abst_states):
        return self.abs_dyn.step(prnt_states, abst_states)
