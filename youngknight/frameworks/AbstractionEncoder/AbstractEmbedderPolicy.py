"""
AbstractEmbedder.py

Network for learning an abstract state

Parameters:
max_swarm_size
n_ders
K
dt
p_max
p_min
l1_loss_abs
l2_loss_abs
l1_loss_pol
l2_loss_pol
pol_scale
trainer
trainer_abs
trainer_pol
swrm_state_gen
swrm_param_gen
prnt_state_gen
prnt_param_gen
abst_state_gen
verbosity
regen_data
patience
use_best
"""

import tensorflow as tf
import numpy as np
from tqdm import trange
import math

class AbstractEmbedderPolicy(object):
    def __init__(self, abs_enc, abs_pol, abs_dyn, prt_dyn, swm_dyn, params, name='AbstractEncoder'):
        self.abs_enc = abs_enc
        self.abs_pol = abs_pol
        self.abs_dyn = abs_dyn
        self.prt_dyn = prt_dyn
        self.swm_dyn = swm_dyn
        self.params = params
        self.name = name

        self.abst_state_size = self.abs_enc.abs_size
        self.swrm_state_size = self.swm_dyn.state_size[1]
        self.swrm_param_size = self.swm_dyn.param_size[1]
        self.prnt_state_size = self.prt_dyn.state_size[0]
        self.prnt_param_size = self.prt_dyn.param_size[0]

        if 'use_best' not in self.params.keys():
            self.params['use_best'] = False
        if 'regen_data' not in self.params.keys():
            self.params['regen_data'] = True;

        self.setup_learning()

        self.sess = tf.Session()
        self.abs_enc.sess = self.sess
        self.abs_pol.sess = self.sess
        self.abs_dyn.sess = self.sess
        self.prt_dyn.sess = self.sess
        self.swm_dyn.sess = self.sess
        self.sess.run(tf.global_variables_initializer())

    def setup_learning(self):
        with tf.variable_scope(self.name) as scope:
            self.cur_swrm_states = tf.placeholder('float', [None, self.params['max_swarm_size'], self.swrm_state_size], 's_swrm')
            self.sta_swrm_params = tf.placeholder('float', [None, self.params['max_swarm_size'], self.swrm_param_size], 'p_swrm')
            self.cur_prnt_states = tf.placeholder('float', [None, self.prnt_state_size], 's_prnt')
            self.sta_prnt_params = tf.placeholder('float', [None, self.prnt_param_size], 'p_prnt')
            self.des_abst_states = tf.placeholder('float', [None, self.params['n_ders']+1, self.abst_state_size], 's_abst')
            self.training = tf.placeholder('bool', name='training')

            enc_inputs = {}
            enc_inputs['swrm_states'] = self.cur_swrm_states
            enc_inputs['swrm_params'] = self.sta_swrm_params
            enc_inputs['training'] = self.training

            _, enc_out, _, _, _ = self.abs_enc.build_network(enc_inputs, scope=scope)

            self.cur_abst_states = enc_out['abst_states']
            self.constituents = self.abs_enc.get_constituents_list()
            self.loss_abs = []
            self.loss_pol = []
            self.sub_swrm_states = []
            self.sub_swrm_params = []
            self.sub_swrm_actins = []
            self.nxt_swrm_states = []
            self.nxt_prnt_states = []
            self.prd_prnt_states = []
            self.nxt_abst_states = []

            dyn_inputs = {}
            dyn_inputs['prnt_states'] = self.cur_prnt_states
            dyn_inputs['prnt_params'] = self.sta_prnt_params
            dyn_inputs['training'] = self.training

            K = tf.constant(self.params['K'], name='K', dtype='float32')
            dt = tf.constant(self.params['dt'], name='dt', dtype='float32')
            center = tf.constant(0.5*(self.params['p_max'] + self.params['p_min']), name='center', dtype='float32')
            radius = tf.constant(0.5*(self.params['p_max'] - self.params['p_min']), name='radius', dtype='float32')

            for i in range(len(self.cur_abst_states)):
                scale = tf.constant(float(len(self.constituents[i]))/float(self.params['max_swarm_size']), name='scale%d'%i, dtype='float32')
                self.sub_swrm_states.append([])
                self.sub_swrm_params.append([])
                for j in range(len(self.constituents[i])):
                    self.sub_swrm_states[i].append(self.cur_swrm_states[:, self.constituents[i][j], :])
                    self.sub_swrm_params[i].append(self.sta_swrm_params[:, self.constituents[i][j], :])

                self.sub_swrm_states[i] = tf.stack(self.sub_swrm_states[i], axis=1)
                self.sub_swrm_params[i] = tf.stack(self.sub_swrm_params[i], axis=1)

                with tf.variable_scope('policy'):
                    self.sub_swrm_actins.append(self.abs_pol.build_policy(self.sub_swrm_states[i], self.cur_abst_states[i], self.des_abst_states, self.sub_swrm_params[i], 0, members=self.constituents[i]))

                with tf.variable_scope('step'):
                    self.nxt_swrm_states.append(self.swm_dyn.build_dynamics(self.sub_swrm_states[i], self.sub_swrm_actins[i], self.sub_swrm_params[i], 0))
                    self.nxt_prnt_states.append(self.prt_dyn.build_dynamics(self.cur_prnt_states, tf.concat([self.sub_swrm_states[i], self.sub_swrm_params[i]], axis=2), self.sta_prnt_params, 0))
                    self.nxt_abst_states.append(self.abs_enc.build_abs(self.nxt_swrm_states[i], self.sub_swrm_params[i], swarm_size=len(self.constituents[i])))

                dyn_inputs['abst_states'] = self.cur_abst_states[i]
                _, dyn_out, _, _, _ = self.abs_dyn.build_network(dyn_inputs, scope=scope)
                self.prd_prnt_states.append(dyn_out['prnt_states'])

                with tf.variable_scope('pol_loss'):
                    s_des_ders = self.des_abst_states[:, 1:, :]
                    s_des = self.des_abst_states[:, 0, :]

                    err = s_des - self.cur_abst_states[i]
                    del_abst_states_des = tf.matmul(err, K)
                    for j in range(self.params['n_ders']):
                        del_abst_states_des += s_des_ders[:, j, :]*tf.constant((self.params['dt']**j)/math.factorial(j), name='coeff%d'%j, dtype='float32')

                    del_abst_states_des *= dt
                    del_abst_states = self.nxt_abst_states[i] - self.cur_abst_states[i]

                    loss = tf.reduce_sum(tf.squared_difference(del_abst_states, del_abst_states_des), axis=1)
                    loss += tf.reduce_sum(tf.square(tf.maximum(tf.abs(self.nxt_swrm_states[i][:, :, 0]-center)-radius, 0.0)), axis=1)

                    self.loss_pol.append(scale*tf.reduce_mean(loss))

                with tf.variable_scope('abs_loss'):
                    loss = tf.reduce_sum(tf.squared_difference(self.nxt_prnt_states[i], self.prd_prnt_states[i]), axis=1)
                    loss += tf.norm(del_abst_states, axis=1, ord=1)/tf.stop_gradient(tf.reduce_sum(tf.norm(self.sub_swrm_states[i]-self.nxt_swrm_states[i], axis=2, ord=1), axis=1))
                    self.loss_abs.append(scale*tf.reduce_mean(loss))


            self.loss_pol = tf.reduce_sum(tf.stack(self.loss_pol))
            self.loss_abs = tf.reduce_sum(tf.stack(self.loss_abs))

            self.enc_weights = self.abs_enc.weights
            self.dyn_weights = self.abs_dyn.weights
            self.pol_weights = self.abs_pol.weights

            if 'l1_loss_abs' in self.params.keys():
                gamma_l1 = tf.constant(self.params['l1_loss_abs'], name='gamma_l1_abs', dtype='float32')
                for key in self.enc_weights.keys():
                    self.loss_abs += gamma_l1*tf.reduce_sum(tf.abs(self.enc_weights[key]))
                for key in self.dyn_weights.keys():
                    self.loss_abs += gamma_l1*tf.reduce_sum(tf.abs(self.dyn_weights[key]))
            if 'l1_loss_pol' in self.params.keys():
                gamma_l1 = tf.constant(self.params['l1_loss_pol'], name='gamma_l1_pol', dtype='float32')
                for key in self.pol_weights.keys():
                    self.loss_pol += gamma_l1*tf.reduce_sum(tf.abs(self.pol_weights[key]))
            if 'l2_loss_abs' in self.params.keys():
                gamma_l2 = tf.constant(self.params['l2_loss_abs'], name='gamma_l2_abs', dtype='float32')
                for key in self.enc_weights.keys():
                    self.loss_abs += gamma_l2*tf.nn.l2_loss(self.enc_weights[key])
                for key in self.dyn_weights.keys():
                    self.loss_abs += gamma_l2*tf.nn.l2_loss(self.dyn_weights[key])
            if 'l2_loss_pol' in self.params.keys():
                gamma_l2 = tf.constant(self.params['l2_loss_pol'], name='gamma_l2_pol', dtype='float32')
                for key in self.pol_weights.keys():
                    self.loss_pol += gamma_l2*tf.nn.l2_loss(self.pol_weights[key])

            abs_weights_list = []
            pol_weights_list = []

            for key in self.enc_weights.keys():
                abs_weights_list.append(self.enc_weights[key])
            for key in self.dyn_weights.keys():
                abs_weights_list.append(self.dyn_weights[key])
            for key in self.pol_weights.keys():
                pol_weights_list.append(self.pol_weights[key])

            if 'trainer' in self.params.keys():
                self.trainer = self.params['trainer'].minimize(self.loss_abs+self.params['pol_scale']*self.loss_pol)
            else:
                self.train_abs = self.params['trainer_abs'].minimize(self.loss_abs+self.params['pol_scale']*self.loss_pol, var_list=abs_weights_list)
                self.train_pol = self.params['trainer_pol'].minimize(self.loss_pol, var_list=pol_weights_list)

    def train(self, n_epochs, n_batches, batch_size, val_size):
        hist = {}
        hist['ave_loss_abs'] = np.zeros(n_epochs)
        hist['val_loss_abs'] = np.zeros(n_epochs)
        hist['ave_loss_pol'] = np.zeros(n_epochs)
        hist['val_loss_pol'] = np.zeros(n_epochs)

        min_weights_abs = {}
        min_weights_dyn = {}
        min_weights_pol = {}
        min_loss = math.inf
        min_count = 0

        val_cur_swrm_states = self.params['swrm_state_gen'](val_size, self.params['max_swarm_size'])
        val_sta_swrm_params = self.params['swrm_param_gen'](val_size, self.params['max_swarm_size'])
        val_cur_prnt_states = self.params['prnt_state_gen'](val_size)
        val_sta_prnt_params = self.params['prnt_param_gen'](val_size)
        val_des_abst_states = self.params['abst_state_gen'](val_size)

        if not self.params['regen_data']:
            bat_cur_swrm_states_list = []
            bat_sta_swrm_params_list = []
            bat_cur_prnt_states_list = []
            bat_sta_prnt_params_list = []
            bat_des_abst_states_list = []

            if self.params['verbosity'] > 0:
                print('Generating Training data...')
            if self.params['verbosity'] > 1:
                batches = trange(n_batches)
            else:
                batches = range(n_batches)

            for i in batches:
                bat_cur_swrm_states_list.append(self.params['swrm_state_gen'](val_size, self.params['max_swarm_size']))
                bat_sta_swrm_params_list.append(self.params['swrm_param_gen'](val_size, self.params['max_swarm_size']))
                bat_cur_prnt_states_list.append(self.params['prnt_state_gen'](val_size))
                bat_sta_prnt_params_list.append(self.params['prnt_param_gen'](val_size))
                bat_des_abst_states_list.append(self.params['abst_state_gen'](val_size))

        for epoch in range(n_epochs):
            if self.params['verbosity'] > 0:
                print('Epoch %d/%d'%(epoch+1, n_epochs))

            if self.params['verbosity'] > 1:
                batches = trange(n_batches)
                batches.set_description('Ave_Loss: N/A')
            else:
                batches = range(n_batches)

            for batch in batches:
                if self.params['regen_data']:
                    bat_cur_swrm_states = self.params['swrm_state_gen'](val_size, self.params['max_swarm_size'])
                    bat_sta_swrm_params = self.params['swrm_param_gen'](val_size, self.params['max_swarm_size'])
                    bat_cur_prnt_states = self.params['prnt_state_gen'](val_size)
                    bat_sta_prnt_params = self.params['prnt_param_gen'](val_size)
                    bat_des_abst_states = self.params['abst_state_gen'](val_size)
                else:
                    #data needs to be shuffled
                    bat_cur_swrm_states = bat_cur_swrm_states_list[batch]
                    bat_sta_swrm_params = bat_sta_swrm_params_list[batch]
                    bat_cur_prnt_states = bat_cur_prnt_states_list[batch]
                    bat_sta_prnt_params = bat_sta_prnt_params_list[batch]
                    bat_des_abst_states = bat_des_abst_states_list[batch]

                if 'trainer' in self.params.keys():
                    loss_abs, loss_pol, _ = self.sess.run([self.loss_abs, self.loss_pol, self.trainer], feed_dict={self.cur_swrm_states:bat_cur_swrm_states, self.sta_swrm_params:bat_sta_swrm_params, self.cur_prnt_states:bat_cur_prnt_states, self.sta_prnt_params:bat_sta_prnt_params, self.des_abst_states:bat_des_abst_states, self.training:True})
                else:
                    loss_abs, loss_pol, _, _ = self.sess.run([self.loss_abs, self.loss_pol, self.train_abs, self.train_pol], feed_dict={self.cur_swrm_states:bat_cur_swrm_states, self.sta_swrm_params:bat_sta_swrm_params, self.cur_prnt_states:bat_cur_prnt_states, self.sta_prnt_params:bat_sta_prnt_params, self.des_abst_states:bat_des_abst_states, self.training:True})

                hist['ave_loss_abs'][epoch] += loss_abs
                hist['ave_loss_pol'][epoch] += loss_pol

                if self.params['verbosity'] > 1:
                    batches.set_description('Ave. Loss abs:%f, pol:%f'%(hist['ave_loss_abs'][epoch]/(batch+1), hist['ave_loss_pol'][epoch]/(batch+1)))

            hist['ave_loss_abs'][epoch] /= n_batches
            hist['ave_loss_pol'][epoch] /= n_batches

            hist['val_loss_abs'][epoch], hist['val_loss_pol'][epoch] = self.sess.run([self.loss_abs, self.loss_pol], feed_dict={self.cur_swrm_states:val_cur_swrm_states, self.sta_swrm_params:val_sta_swrm_params, self.cur_prnt_states:val_cur_prnt_states, self.sta_prnt_params:val_sta_prnt_params, self.des_abst_states:val_des_abst_states, self.training:False})

            if self.params['verbosity'] > 0:
                print('Epoch %d complete! Ave. Loss abs:%f, pol:%f; Val. Loss abs:%f, pol:%f'%(epoch+1, hist['ave_loss_abs'][epoch], hist['ave_loss_pol'][epoch], hist['val_loss_abs'][epoch], hist['val_loss_pol'][epoch]))

            if hist['val_loss_abs'][epoch] + hist['val_loss_pol'][epoch] < min_loss:
                min_loss = hist['val_loss_abs'][epoch] + hist['val_loss_pol'][epoch]
                min_weights_abs = self.abs_enc.save_weights()
                min_weights_dyn = self.abs_dyn.save_weights()
                min_weights_pol = self.abs_pol.save_weights()
                min_count = 0
            else:
                min_count += 1

            if 'patience' in self.params.keys() and min_count > self.params['patience']:
                print('Validation loss has stopped decreassing, stopping training early.')
                hist['ave_loss_abs'] = hist['ave_loss_abs'][:epoch+1]
                hist['ave_loss_pol'] = hist['ave_loss_pol'][:epoch+1]
                hist['val_loss_abs'] = hist['val_loss_abs'][:epoch+1]
                hist['val_loss_pol'] = hist['val_loss_pol'][:epoch+1]
                break;

        if self.params['use_best']:
            self.abs_enc.assign_weights(min_weights_abs)
            self.abs_dyn.assign_weights(min_weights_dyn)
            self.abs_pol.assign_weights(min_weights_pol)

        return hist
