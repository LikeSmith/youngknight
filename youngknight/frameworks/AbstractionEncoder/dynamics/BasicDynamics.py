"""
BasicDynamics.py

Basic MLP Dynamics estimation model
"""

import tensorflow as tf

from .AbstractDynamics import AbstractDynamics
from ....models.Dynamics import Dynamics
from ....core import activ_deserialize

class BasicDynamics(AbstractDynamics):
    def build_network(self, inputs=None, weights=None, scope=None):
        if scope is None:
            scope = self.name
        else:
            scope = scope.name + self.name

        with tf.variable_scope(scope) as local_scope:
            if inputs is None:
                inputs = {}
                inputs['abst_states'] = tf.placeholder('float', [None, self.abs_size], 'abst_states')
                inputs['prnt_states'] = tf.placeholder('float', [None, self.state_size], 'cur_state')
                inputs['prnt_params'] = tf.placeholder('float', [None, self.param_size], 'params')

                self.local_scope = local_scope
            else:
                assert 'abst_states' in inputs.keys() and 'prnt_states' in inputs.keys()

            if weights is None:
                try:
                    weights = self.weights
                except AttributeError:
                    w_init = tf.random_normal_initializer(0, self.params['init_var'])
                    weights = {}
                    weights['W1'] = tf.get_variable('W1', [self.abs_size+self.state_size+self.param_size, self.params['hidden_size']], initializer=w_init)
                    weights['b1'] = tf.get_variable('b1', self.params['hidden_size'], initializer=w_init)
                    weights['W2'] = tf.get_variable('W2', [self.params['hidden_size'], self.state_size], initializer=w_init)
                    weights['b2'] = tf.get_variable('b2', self.state_size, initializer=w_init)

            outputs = {}
            comb = tf.concat((inputs['abst_states'], inputs['prnt_states'], inputs['prnt_params']), axis=1)
            h = activ_deserialize(self.params['hidden_activ'])(tf.add(tf.matmul(comb, weights['W1']), weights['b1']))
            outputs['prnt_states'] = tf.add(tf.matmul(h, weights['W2']), weights['b2'])

            return inputs, outputs, weights, {}, {}

class BasicDynamics_const(Dynamics):
    def __init__(self, abs_size, state_size, param_size, load_file=None, home='', name='pend_model_dyn', sess=None, debug=False):
        mdl = BasicDynamics(abs_size, state_size, load_file=load_file, home=home, name='cpy_mdl', sess=sess, debug=debug)

        params = mdl.params
        params.weights = mdl.save_weights()

        super(BasicDynamics_const, self).__init__((state_size,), (abs_size,), (param_size,), params, load_file, home, name, sess, debug)

    def setup_weights(self):
        weights = {}
        for key in self.params.weights.keys():
            weights[key] = tf.constant(self.params.weights[key], name=key)

        return weights

    def build_dynamics(self, s, a, par, k, weights=None):
        if weights is None:
            weights = self.weights

        comb = tf.concat((a, s, par), axis=1)
        h = activ_deserialize(self.params['hidden_Activ'])(tf.add(tf.matmul((comb, weights['W1']))))
        s_ = tf.add(tf.matmul(h, weights['W2'], weights['b2']))

        return s_
