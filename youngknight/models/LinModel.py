"""
Linear Model

Creates a linear model
"""

import tensorflow as tf
from youngknight.core import Model

class LinModel(Model):
    def build_network(self, inputs=None, weights=None):
        if inputs is None:
            inputs = {}
            inputs['x'] = tf.placeholder("float", [None, self.params['input_size']])
            inputs['y_true'] = tf.placeholder("float", [None, self.params['output_size']])

        assert 'x' in inputs.keys() and 'y_true' in inputs.keys()

        if weights is None:
            try:
                weights = self.weights
            except AttributeError:
                weights = {}
                weights['W'] = tf.get_variable(self.name+'_W', [self.params['input_size'], self.params['output_size']], initializer=tf.random_normal_initializer)
                weights['b'] = tf.get_variable(self.name+'_b', [self.params['output_size']], initializer=tf.constant_initializer(0.0))

        outputs = {}
        losses = {}
        train_ops = {}

        outputs['y'] = tf.add(tf.matmul(inputs['x'], weights['W']), weights['b'])

        losses['loss'] = tf.reduce_mean(tf.squared_difference(outputs['y'], inputs['y_true']))

        self.optimizer = tf.train.AdamOptimizer(learning_rate=self.params['learning_rate'])
        train_ops['train'] = self.optimizer.minimize(losses['loss'])

        return inputs, outputs, weights, losses, train_ops

    def gen_train_feed(self, x, y):
        return {self.inputs['x']:x, self.inputs['y_true']:y}

    def gen_input_feed(self, x):
        return {self.inputs['x']:x}
