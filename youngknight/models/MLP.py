"""
MultiLayer Perceptron

Creates a MLP model
"""

import tensorflow as tf
from ..core import Model, activ_deserialize

class MLP(Model):
    def build_network(self, inputs=None, weights=None):
        if inputs is None:
            inputs = {}
            inputs['x'] = tf.placeholder("float", [None, self.params['layer_sizes'][0]])
            inputs['y_true'] = tf.placeholder("float", [None, self.params['layer_sizes'][-1]])
        assert 'x' in inputs.keys() and 'y_true' in inputs.keys()

        if not isinstance(self.params['bias'], list):
            self.params['bias'] = [self.params['bias']]*len(self.params['layer_sizes'])
        if not isinstance(self.params['activ'], list):
            self.params['activ'] = [self.params['activ']]*len(self.params['layer_sizes'])

        if weights is None:
            try:
                weights = self.weights
            except AttributeError:
                weights = {}
                for i in range(len(self.params['layer_sizes'])-1):
                    weights['W%d'%(i,)] = tf.get_variable(self.name+'_W%d'%(i,), [self.params['layer_sizes'][i], self.params['layer_sizes'][i+1]], initializer=tf.random_normal_initializer)
                    if self.params['bias'][i]:
                        weights['b%d'%(i,)] = tf.get_variable(self.name+'_b%d'%(i,), [self.params['layer_sizes'][i+1]], initializer=tf.random_normal_initializer)

        outputs = {}
        losses = {}
        train_ops = {}

        y = inputs['x']

        for i in range(len(self.params['layer_sizes'])-1):
            y = tf.matmul(y, weights['W%d'%(i,)])
            if self.params['bias'][i]:
                y = tf.add(y, weights['b%d'%(i,)])
            if self.params['activ'][i] is not None:
                y = activ_deserialize(self.params['activ'][i])(y)

        outputs['y'] = y
        losses['loss'] = tf.reduce_mean(tf.squared_difference(outputs['y'], inputs['y_true']))

        self.optimizer = tf.train.AdamOptimizer(learning_rate=self.params['learning_rate'])
        train_ops['train'] = self.optimizer.minimize(losses['loss'])

        return inputs, outputs, weights, losses, train_ops

    def gen_train_feed(self, x, y):
        return {self.inputs['x']:x, self.inputs['y_true']:y}

    def gen_input_feed(self, x):
        return {self.inputs['x']:x}
