"""
MultiLayer Perceptron

Creates a MLP model
"""

import tensorflow as tf
import numpy as np
from youngknight.core import Model
from youngknight.models import MLP

class VAE(Model):
    def gen_inputs(self):
        if isinstance(self.params['input_size'], list):
            self.inputs = {'x':tf.placeholder("float", [None]+self.params['input_size'])}
            self.x = tf.reshape(self.inputs['x'], [-1, np.prod(self.params['input_size'])])
            self.in_size_total = np.prod(self.params['input_size'])
        else:
            self.inputs = {'x':tf.placeholder("float", [None, self.params['input_size']])}
            self.x = self.inputs['x']
            self.in_size_total = self.params['input_size']
    
    def gen_weights(self):
        
        enc_params = {'layer_sizes':[self.in_size_total, self.params['int_size'], 2*self.params['lat_size']], 'activ':[tf.nn.relu, None], 'learning_rate':self.params['learning_rate']}
        dec_params = {'layer_sizes':[self.params['lat_size'], self.params['int_size'], self.in_size_total], 'activ':[tf.nn.relu, tf.sigmoid], 'learning_rate':self.params['learning_rate']}
        self.encoder = MLP(enc_params, home=self.home, name='%s_encoder'%(self.name,))
        self.decoder = MLP(dec_params, home=self.home, name='%s_decoder'%(self.name,))
        
        self.weights = dict(self.encoder.weights, **self.decoder.weights)
    
    def wire_network(self, inputs):
        h = self.encoder.wire_network({'x':self.x})
        self.z_mean = h['y'][:, :self.params['lat_size']]
        self.z_log_var = h['y'][:, self.params['lat_size']:]
        eps = tf.random_normal(shape=tf.shape(self.z_mean), mean=0.0, stddev=1.0, dtype=tf.float32, seed=self.params['seed'])
        self.z_samp = self.z_mean + tf.squeeze(tf.matmul(tf.matrix_diag(tf.exp(self.z_log_var)), tf.expand_dims(eps, -1)), [-1])
        self.x_dec_flat = self.decoder.wire_network({'x':self.z_samp})['y']
        if isinstance(self.params['input_size'], list):
            self.x_dec = tf.reshape(self.x_dec_flat, [-1]+self.params['input_size'])
        
        return {'z_mean':self.z_mean, 'z_log_var':self.z_log_var, 'z_samp':self.z_samp, 'x_dec':self.x_dec}
    
    def get_loss(self):
        #xent_loss = tf.squared_difference(self.x_dec, self.inputs['x'])
        epsilon = 1e-7
        x_dec_logits = tf.clip_by_value(self.x_dec_flat, epsilon, 1-epsilon)
        x_dec_logits = tf.log(x_dec_logits/(1 - x_dec_logits))
        xent_loss = tf.reduce_mean(tf.nn.sigmoid_cross_entropy_with_logits(labels=self.x, logits=x_dec_logits))*self.in_size_total
        kl_loss = -0.5*tf.reduce_sum(1 + self.z_log_var - tf.square(self.z_mean) - tf.exp(self.z_log_var), axis=-1)
        loss_op = tf.reduce_mean(xent_loss + kl_loss)
        return loss_op
        
    def get_train(self):
        self.optimizer = tf.train.AdamOptimizer(learning_rate=self.params['learning_rate'])
        train_op = self.optimizer.minimize(self.loss_op)
        return train_op
        
    def gen_feed(self, x):
        return {self.inputs['x']:x}
    
    def encode(self, x):
        return self.encoder({self.inputs['x']:x})
    
    def decode(self, z):
        return self.decoder({self.inputs['x']:z})
