"""
Pendulum.py

Dynamics model for an inverted pendulum
"""

import tensorflow as tf
from .Dynamics import Dynamics
from .Observer import Observer

class PendulumDyn(Dynamics):
    def __init__(self, params={}, load_file=None, home='', name='pend_model_dyn', sess=None, debug=False):
        super(PendulumDyn, self).__init__((2,), (1,), (4,), params, load_file, home, name, sess, debug)
        
    def setup_weights(self):
        weights = {}
        weights['del_t'] = tf.constant(self.params['del_t'], name='del_t')
        
        return weights
        
    def build_dynamics(self, s, a, p, k, weights=None):
        theta = s[:, 0]
        omega = s[:, 1]
        tau = a[:, 0]
        m = p[:, 0]
        l = p[:, 1]
        c = p[:, 2]
        g = p[:, 3]
        
        if weights is None:
            weights = self.weights
        
        if self.params['clip_tau']:
            tau = tf.clip_by_value(tau, self.params['tau_min'], self.params['tau_max'], name='tau_clipped')
            
        if self.params['inv']:
            alpha_new = 3*tau/(m*l**2) + 3*g/(2*l)*tf.math.sin(theta) - 3*omega*c/(m*l**2)
        else:
            alpha_new = 3*tau/(m*l**2) - 3*g/(2*l)*tf.math.sin(theta) - 3*omega*c/(m*l**2)
        
        omega_new = omega + weights['del_t']*alpha_new
        
        if self.params['clip_omega']:
            omega_new = tf.clip_by_value(omega_new, self.params['omega_min'], self.params['omega_max'], name='omega_new_clipped')
            
        theta_new = theta + weights['del_t']*omega_new
        
        if self.params['clip_theta']:
            theta_new = tf.clip_by_value(omega_new, self.params['theta_min'], self.params['theta_max'], name='theta_new_clipped')
            
        s1 = tf.stack((theta_new, omega_new), axis=1, name='new_state')
        
        return s1
    
class PendulumObs(Observer):
    def __init__(self, params={}, load_file=None, home='', name='pend_model_obs', sess=None, debug=False):
        super(PendulumObs, self).__init__((2,), (3,), (4,), params, load_file, home, name, sess, debug)
        
    def setup_weights(self):
        return {}
    
    def build_obs(self, s, p, k, weights=None):
        theta = s[:, 0]
        omega = s[:, 1]
        
        c_theta = tf.math.cos(theta)
        s_theta = tf.math.sin(theta)
        
        o = tf.stack((c_theta, s_theta, omega), axis=1)
        
        return o
    