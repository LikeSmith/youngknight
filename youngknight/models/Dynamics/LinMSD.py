"""
LinMSD.py

Dynamics model for an inverted pendulum
"""

import tensorflow as tf
from .Dynamics import Dynamics

class LinMSD(Dynamics):
    def __init__(self, params={}, load_file=None, home='', name='LinMSD', sess=None, debug=False):
        super(LinMSD, self).__init__((2,), (1,), (3,), params, load_file, home, name, sess, debug)

    def setup_weights(self):
        weights = {}
        weights['del_t'] = tf.constant(self.params['del_t'], name='del_t')

        return weights

    def build_dynamics(self, s, a, par, k, weights=None):
        if weights is None:
            weights = self.weights

        p = s[:, 0]
        v = s[:, 1]
        f = a[:, 0]
        m = par[:, 0]
        k = par[:, 1]
        c = par[:, 2]

        if weights is None:
            weights = self.weights

        if self.params['clip_f']:
            f = tf.clip_by_value(f, self.params['f_min'], self.params['f_max'], name='f_clipped')

        a_new = (f - k*p - c*v)/m

        v_new = v + weights['del_t']*a_new

        if self.params['clip_v']:
            v_new = tf.clip_by_value(v_new, self.params['v_min'], self.params['v_max'], name='v_new_clipped')

        p_new = p + weights['del_t']*v_new

        if self.params['clip_p']:
            p_new = tf.clip_by_value(p_new, self.params['p_min'], self.params['p_max'], name='p_new_clipped')

        s1 = tf.stack((p_new, v_new), axis=1, name='new_state')

        return s1
