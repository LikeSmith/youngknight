"""
Observer.py

Base class for dynamics model
"""

import tensorflow as tf
from ...core import Model

class Observer(Model):
    def __init__(self, state_size, obsrv_size, param_size, params={}, load_file=None, home='', name='base_model', sess=None, debug=False):
        self.state_size = state_size
        self.obsrv_size = obsrv_size
        self.param_size = param_size
        
        super(Observer, self).__init__(params=params, load_file=load_file, home=home, name=name, sess=sess, debug=debug)
       
    def setup_weights(self):
        raise NotImplementedError
        
    def build_obs(self, s, p, k, weights=None):
        raise NotImplementedError
        
    def build_network(self, inputs=None):
        with tf.variable_scope(self.name):
            if inputs is None:
                inputs = {}
                inputs['s'] = tf.placeholder('float', (None,) + self.state_size, 's0')
                inputs['p'] = tf.placeholder('float', (None,) + self.param_size, 'p')
                inputs['k'] = tf.placeholder('float', (), 'k')
                
            assert 's' in inputs.keys()
            assert 'p' in inputs.keys()
            assert 'k' in inputs.keys()
            
            weights = self.setup_weights()
            o = self.build_obs(inputs['s'], inputs['p'], inputs['k'], weights)
            
            outputs = {}
            outputs['o'] = o
            
            return inputs, outputs, weights, {}, {}
        
    def __call__(self, s, p, k):
        return self.sess.run(self.output_ops['o'], feed_dict={self.inputs['s']:s, self.inputs['p']:p, self.inputs['k']:k})
    
    def observe(self, s, p, k):
        return self.sess.run(self.output_ops['o'], feed_dict={self.inputs['s']:s, self.inputs['p']:p, self.inputs['k']:k})
    
    def train(self):
        pass
        