"""
Reward.py

Base class for reward models
"""

import tensorflow as tf
from ...core import Model

class Reward(Model):
    def __init__(self, state_size, obsrv_size, actin_size, param_size, params={}, load_file=None, home='', name='reward_fun', sess=None, debug=False):
        self.state_size = state_size
        self.obsrv_size = obsrv_size
        self.actin_size = actin_size
        self.param_size = param_size

        super(Reward, self).__init__(params=params, load_file=load_file, home=home, name=name, sess=sess, debug=debug)

    def setup_weights(self):
        raise NotImplementedError

    def build_reward(self, s0, s1, o0, o1, a, p, k, weights=None):
        raise NotImplementedError

    def build_network(self, inputs=None, weights=None):
        with tf.variable_scope(self.name):
            if inputs is None:
                inputs = {}
                inputs['s0'] = tf.placeholder('float', (None,) + self.state_size, 's0')
                inputs['s1'] = tf.placeholder('float', (None,) + self.state_size, 's1')
                inputs['o0'] = tf.placeholder('float', (None,) + self.obsrv_size, 'o0')
                inputs['o1'] = tf.placeholder('float', (None,) + self.obsrv_size, 'o1')
                inputs['a'] = tf.placeholder('float', (None,) + self.actin_size, 'a')
                inputs['p'] = tf.placeholder('float', (None,) + self.param_size, 'p')
                inputs['k'] = tf.placeholder('int32', (), 'k')

            assert 's0' in inputs.keys()
            assert 's1' in inputs.keys()
            assert 'o0' in inputs.keys()
            assert 'o1' in inputs.keys()
            assert 'a' in inputs.keys()
            assert 'p' in inputs.keys()
            assert 'k' in inputs.keys()

            if weights is None:
                try:
                    weights = self.weights
                except AttributeError:
                    weights = self.setup_weights()

            r = self.build_reward(inputs['s0'], inputs['s1'], inputs['o0'], inputs['o1'], inputs['a'], inputs['p'], inputs['k'], weights)

            outputs = {}
            outputs['r'] = r

            return inputs, outputs, weights, {}, {}

    def __call__(self, s0, s1, o0, o1, a, p, k):
        return self.sess.run(self.output_ops['r'], feed_dict={self.inputs['s0']:s0, self.inputs['s1']:s1, self.inputs['o0']:o0, self.inputs['o1']:o1, self.inputs['a']:a, self.inputs['p']:p, self.inputs['k']:k})

    def get_rwd(self, s0, s1, o0, o1, a, p, k):
        return self.sess.run(self.output_ops['r'], feed_dict={self.inputs['s0']:s0, self.inputs['s1']:s1, self.inputs['o0']:o0, self.inputs['o1']:o1, self.inputs['a']:a, self.inputs['p']:p, self.inputs['k']:k})

    def train(self):
        pass

class Reward_AbsTrack(Model):
    def __init__(self, state_size, actin_size, abstr_size, param_size, params={}, load_file=None, home='', name='reward_fun', sess=None, debug=False):
        self.state_size = state_size
        self.actin_size = actin_size
        self.abstr_size = abstr_size
        self.param_size = param_size

        super(Reward_AbsTrack, self).__init__(params=params, load_file=load_file, home=home, name=name, sess=sess, debug=debug)

    def setup_weights(self):
        raise NotImplementedError

    def build_reward(self, s_cur, s_abs, s_des, a, s_nxt, s_abs_nxt, p, k, weights=None):
        raise NotImplementedError

    def build_network(self, inputs=None, weights=None):
        with tf.variable_scope(self.name):
            if inputs is None:
                inputs = {}
                inputs['s_cur'] = tf.placeholder('float', (None,) + self.state_size, 's_cur')
                inputs['s_abs'] = tf.placeholder('float', (None, self.abstr_size), 's_abs')
                inputs['a'] = tf.placeholder('float', (None,) + self.actin_size, 'a')
                inputs['s_nxt'] = tf.placeholder('float', (None,) + self.state_size, 's_cur')
                inputs['s_abs_nxt'] = tf.placeholder('float', (None, self.abstr_size), 's_abs')
                if 'n_ders' in self.params.keys() and self.params['n_ders'] > 0:
                    inputs['s_des'] = tf.placeholder('float', (None, self.params['n_ders']+1, self.abstr_size), 's_des')
                else:
                    inputs['s_des'] = tf.placeholder('float', (None, self.abstr_size), 's_des')
                inputs['p'] = tf.placeholder('float', (None,) + self.param_size, 'p')
                inputs['k'] = tf.placeholder('int32', (), 'k')

            assert 's_cur' in inputs.keys()
            assert 's_abs' in inputs.keys()
            assert 's_des' in inputs.keys()
            assert 'a' in inputs.keys()
            assert 's_nxt' in inputs.keys()
            assert 's_abs_nxt' in inputs.keys()
            assert 'p' in inputs.keys()
            assert 'k' in inputs.keys()

            if weights is None:
                try:
                    weights = self.weights
                except AttributeError:
                    weights = self.setup_weights()

            r = self.build_reward(inputs['s_cur'], inputs['s_abs'], inputs['s_des'], inputs['a'], inputs['s_nxt'], inputs['s_abs_nxt'], inputs['p'], inputs['k'], weights)

            outputs = {}
            outputs['r'] = r

            return inputs, outputs, weights, {}, {}

    def __call__(self, s_cur, s_abs, s_des, a, p, k):
        return self.sess.run(self.output_ops['r'], feed_dict={self.inputs['s_cur']:s_cur, self.inputs['s_abs']:s_abs, self.inputs['s_des']:s_des, self.inputs['a']:a, self.inputs['p']:p, self.inputs['k']:k})

    def get_rwd(self, s_cur, s_abs, s_des, a, p, k):
        return self.sess.run(self.output_ops['r'], feed_dict={self.inputs['s_cur']:s_cur, self.inputs['s_abs']:s_abs, self.inputs['s_des']:s_des, self.inputs['a']:a, self.inputs['p']:p, self.inputs['k']:k})

    def train(self):
        pass