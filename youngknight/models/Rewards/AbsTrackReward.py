"""
AbsTrackReward.py


"""

import tensorflow as tf
from .Reward import Reward_AbsTrack
import math

class AbsTrack_Reward(Reward_AbsTrack):
    def setup_weights(self):
        weights = {}

        weights['K'] = tf.constant(self.params['K'], name='K', dtype='float32')

        return weights

    def build_reward(self, s_cur, s_abs, s_des, a, s_nxt, s_abs_nxt, p, k, weights=None):
        if weights is None:
            weights = self.weights

        if 'n_ders' in self.params.keys() and self.params['n_ders'] > 0:
            s_des_ders = s_des[:, 1:, :]
            s_des = s_des[:, 0, :]

        err = s_des - s_abs
        del_abs = s_abs_nxt - s_abs

        del_abs_des = tf.matmul(err, weights['K'])

        if 'n_ders' in self.params.keys() and self.params['n_ders'] > 0:
            for i in range(self.params['n_ders']):
                del_abs_des += s_des_ders[:, i, :]*tf.constant((self.params['dt']**i)/math.factorial(i), name='coeff%d'%i, dtype='float32')

        cost = tf.reduce_sum(tf.squared_difference(del_abs_des, del_abs), axis=1)

        S_term = 0.0
        if 'delta' in self.params.keys():
            pos = s_cur[:, :, 0]
            S_term = tf.reduce_sum(tf.square(tf.maximum(tf.abs(pos)-self.params['delta']/2.0, 0.0)), axis=1)
        if 'Q_p' in self.params.keys():
            S_term *= self.params['Q_p']

        cost += S_term

        return -cost