"""
LQRReward.py

LQR loss
"""

import tensorflow as tf
from .Reward import Reward, Reward_AbsTrack

class LQRReward(Reward):
    def setup_weights(self):
        weights = {}
        weights['Q'] = tf.constant(self.params['Q'], name='Q', dtype='float32')
        weights['R'] = tf.constant(self.params['R'], name='R', dtype='float32')

        if 'time_scale' in self.params.keys():
            weights['time_scale'] = tf.constant(self.params['time_scale'], name='time_scale')

        return weights

    def build_reward(self, s0, s1, o0, o1, a, p, k, weights=None):
        if weights is None:
            weights = self.weights

        Q_term = tf.einsum('ij, ij->i', tf.matmul(s0, weights['Q']), s0)
        R_term = tf.einsum('ij, ij->i', tf.matmul(a, weights['R']), a)

        r = -Q_term - R_term

        if 'time_scale' in self.params.keys():
            r = weights['time_scale']*k*r

        return r

class LQRReward_AbsTrack(Reward_AbsTrack):
    def setup_weights(self):
        weights = {}
        weights['Q'] = tf.constant(self.params['Q'], name='Q', dtype='float32')
        weights['R'] = tf.constant(self.params['R'], name='R', dtype='float32')

        if 'time_scale' in self.params.keys():
            weights['time_scale'] = tf.constant(self.params['time_scale'], name='time_scale')

        return weights

    def build_reward(self, s_cur, s_abs, s_des, a, s_nxt, s_abs_nxt, p, k, weights=None):
        if weights is None:
            weights = self.weights

        if 'n_ders' in self.params.keys() and self.params['n_ders'] > 0:
            s_des = s_des[:, 0, :]

        err = s_des - s_abs

        Q_term = tf.einsum('ij,jk,ik->i', err, weights['Q'], err)
        R_term = tf.einsum('ijk,kl,ijl->i', a, weights['R'], a)
        S_term = 0.0

        if 'delta' in self.params.keys():
            pos = s_cur[:, :, 0]
            S_term = tf.reduce_sum(tf.square(tf.maximum(tf.abs(pos)-self.params['delta']/2.0, 0.0)), axis=1)
        if 'Q_p' in self.params.keys():
            S_term *= self.params['Q_p']

        r = -Q_term - R_term - S_term

        if 'time_scale' in self.params.keys():
            r = weights['time_scale']*k*r

        return r
