"""
PendReward.py

pendulum loss
"""

import tensorflow as tf
from .Reward import Reward

class PendReward(Reward):
    def setup_weights(self):
        weights = {}
        weights['theta'] = tf.constant(self.params['theta_gain'], name='theta', dtype='float32')
        weights['omega'] = tf.constant(self.params['omega_gain'], name='omega', dtype='float32')
        weights['tau'] = tf.constant(self.params['tau_gain'], name='tau', dtype='float32')
        
        if 'time_scale' in self.params.keys():
            weights['time_scale'] = tf.constant(self.params['time_scale'], name='time_scale')
            
        return weights
        
    def build_reward(self, s0, s1, o0, o1, a, p, k, weights=None):
        if weights is None:
            weights = self.weights
            
        theta = s0[:, 0]
        omega = s0[:, 1]
        tau = a[:, 0]
        
        theta = tf.atan2(tf.sin(theta), tf.cos(theta))
        
        r = weights['theta']*theta**2 + weights['omega']*omega**2 + weights['tau']*tau**2
        
        if 'time_scale' in self.params.keys():
            weights['time_scale'] = tf.constant(self.params['time_scale'], name='time_scale')
            r = weights['time_scale']*k*r
            
        return -r

