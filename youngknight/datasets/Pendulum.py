'''
data manager for pendulum
'''

import numpy as np
import _pickle as pkl

from youngknight.core import DataManager
from youngknight.sims import Pendulum

class Pendulum_DM(DataManager):
    def load_data_params(self, index):
        self.limits, self.noise_params, self.dt, self.inv, self.params, self.ctrl, self.name = pkl.load(open(self.home+self.param_dir+'dataset_%s_params.pkl'%index, 'rb'))
        
    def gen_sim(self):
        self.sim = Pendulum(self.limits, self.noise_params, dt=self.dt, inv=self.inv, ctrl=self.ctrl, name=self.name)
    
    def gen_description(self):
        desc = 'Pendulum dataset %s with %d trajectories to a horizon of %d steps.'%(self.index, self.n, self.horizon)
        if self.use_noise:
            desc += ' Simulation has built in process and sensor noise.'
        return desc