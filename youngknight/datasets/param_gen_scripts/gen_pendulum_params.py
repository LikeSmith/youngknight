'''
generates parameter file for pendulum data_manager
'''

import numpy as np
import _pickle as pkl

index = 'test1'
location = '../../../tests/Pendulum_test/params/'

limits = {}
limits['tau_min'] = -10
limits['tau_max'] = 10
limits['m_min'] = 0.5
limits['m_max'] = 5
limits['l_min'] = 0.5
limits['l_max'] = 2
limits['c_min'] = 1
limits['c_max'] = 2
limits['g_min'] = 9.81
limits['g_max'] = 9.81
limits['theta_min'] = -np.pi
limits['theta_max'] = np.pi
limits['omega_min'] = -1
limits['omega_max'] = 1
    
noise_params = {}
noise_params['process_mean'] = np.zeros((1, 2))
noise_params['process_cov'] = np.eye(2)*0.01
noise_params['signal_mean'] = np.zeros((1, 1))
noise_params['signal_cov'] = np.eye(1)*0.01

dt = 0.01
inv = True
params = None
ctrl = None
name = 'pend_DM_test'

pkl.dump([limits, noise_params, dt, inv, params, ctrl, name], open(location+'dataset_%s_params.pkl'%index, 'wb'))
