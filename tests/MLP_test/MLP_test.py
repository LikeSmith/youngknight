"""
test MLP Model

this script tests the linear model
"""

import numpy as np
from youngknight.models import MLP

def lin_activ(a):
    return a

if __name__=='__main__':
    layer_sizes = [3, 100, 1]
    activ = [None, 'relu', None]
    learning_rate = 0.0001
    samples = 10000
    val_samp = 100

    x_min = np.array([[-10.0, -10.0, -10.0]])
    x_max = np.array([10.0, 10.0, 10.0])

    params = {'layer_sizes':layer_sizes, 'activ':activ, 'learning_rate':learning_rate, 'bias':True}

    mdl = MLP(params, home='test_data/', name='Linear_Model_test')

    x_min_train = np.tile(x_min.reshape(1, 3), (samples, 1))
    x_max_train = np.tile(x_max.reshape(1, 3), (samples, 1))

    x = x_min_train + (x_max_train - x_min_train)*np.random.rand(samples, 3)
    y = np.matmul(x.reshape(samples, 1, 3), x.reshape(samples, 3, 1)).reshape(samples, 1)

    x_min_val = np.tile(x_min.reshape(1, 3), (val_samp, 1))
    x_max_val = np.tile(x_max.reshape(1, 3), (val_samp, 1))

    x_val = x_min_val + (x_max_val - x_min_val)*np.random.rand(val_samp, 3)
    y_val = np.matmul(x_val.reshape(val_samp, 1, 3), x_val.reshape(val_samp, 3, 1)).reshape(val_samp, 1)

    mdl.train(mdl.gen_train_feed(x, y), mdl.gen_train_feed(x_val, y_val), samples, n_epochs=1000, batch_size=128, patience=1000, verbosity=2)

    mdl.save('test1.pkl')

    mdl2 = MLP(load_file='test1.pkl', home='test_data/', name='Linear_Model_test2')

    x_test = np.array([[1.0, 2.0, 3.0]])

    print(np.dot(x_test, x_test.T))
    print(mdl(mdl.gen_input_feed(x_test)))
    print(mdl2(mdl2.gen_input_feed(x_test)))
