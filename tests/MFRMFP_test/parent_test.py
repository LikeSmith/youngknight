'''
parent_test
'''

import numpy as np
import matplotlib.pyplot as plt

from youngknight.sims import PlaneSystem

K_p = [3.1, 3.3]

dt = 0.1
horizon = 1000
n = 1

limits = {}
limits['theta_min'] = -1.0
limits['theta_max'] = 1.0
limits['omega_min'] = -10.0
limits['omega_max'] = 10.0
limits['mu_min'] = -5.0
limits['mu_max'] = 5.0
limits['J_min'] = 0.0
limits['J_max'] = 5.0
limits['J_dot_min'] = -5.0
limits['J_dot_max'] = 5.0
limits['J_p_min'] = 0.5
limits['J_p_max'] = 1.0
limits['gamma_1_min'] = 0.01
limits['gamma_1_max'] = 0.09
limits['gamma_2_min'] = 1000
limits['gamma_2_max'] = 1000
limits['gamma_23_min'] = 300
limits['gamma_23_max'] = 300
limits['gamma_14_min'] = 0.001
limits['gamma_14_max'] = 0.009
limits['gamma_5_min'] = 1000
limits['gamma_5_max'] = 1000
limits['gamma_6_min'] = 0.0
limits['gamma_6_max'] = 0.0

def ctrl(state, k, params):
    n = state.shape[0]
    t = K_p[0]*state[:, 0] + K_p[1]*state[:, 1]
    a = np.zeros((n, 3))
    a[:, 0] = t
    return a

if __name__ == '__main__':
    sim = PlaneSystem(limits=limits, dt=dt, enforce_limits=True, ctrl=ctrl, name='PlaneSys_test')
    
    res, p = sim.gen_trajectories(sim.init_state_gen(n), horizon, sim.param_gen(n))
    
    t = np.arange(horizon+1)*dt
    
    plt.figure()
    plt.plot(t, res['states'][:, :, 0].T)
    plt.xlabel('time (s)')
    plt.ylabel('theta (rad)')
    
    plt.figure()
    plt.plot(t, res['states'][:, :, 1].T)
    plt.xlabel('time (s)')
    plt.ylabel('omega (rad/s)')
    
    plt.figure()
    plt.plot(t[:-1], res['actions'][:, :, 0].T)