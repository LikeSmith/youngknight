"""
Single Integrator Swarm test
"""

import numpy as np
import matplotlib.pyplot as plt

from youngknight.sims import DoubleIntegratorSwarm

K_p = -10.0*np.eye(2)
K_d = 5.0*np.eye(2)
k_sd = 1.0
dt = 0.01

omega_mu = 0.1*2*np.pi
omega_J = 0.5*2*np.pi
phi_mu = 0.0
phi_J = 0.0

swarm_size = 10
L = 1.0
n = 1
horizon = 2000

limits = {}
limits['p_max'] = 0.5
limits['p_min'] = -0.5
limits['v_max'] = 1.0
limits['v_min'] = -1.0
limits['f_max'] = 10.0
limits['f_min'] = -10.0
limits['m_max'] = 0.5
limits['m_min'] = 0.25
limits['gamma_1_min'] = 0.0
limits['gamma_1_max'] = 0.0
limits['gamma_2_min'] = 1000
limits['gamma_2_max'] = 1000
limits['gamma_23_min'] = 300
limits['gamma_23_max'] = 300
limits['gamma_14_min'] = 0.0
limits['gamma_14_max'] = 0.0
limits['gamma_5_min'] = 1000
limits['gamma_5_max'] = 1000
limits['gamma_6_min'] = 1.0
limits['gamma_6_max'] = 1.0

def abs_d(k):
    mu_d = np.sin(omega_mu*dt*k + phi_mu)
    J_d = np.sin(omega_J*dt*k + phi_J) + 2.0
    mu_d_dot = omega_mu*np.cos(omega_mu*dt*k + phi_mu)
    J_d_dot = omega_J*np.cos(omega_J*dt*k + phi_J)
    mu_d_ddot = -omega_mu**2*np.sin(omega_mu*dt*k + phi_mu)
    J_d_ddot = -omega_J**2*np.sin(omega_J*dt*k + phi_J)
    
    return mu_d, J_d, mu_d_dot, J_d_dot, mu_d_ddot, J_d_ddot

def ctrl(state, k, params, abs_state, aux_state):
    mu_d, J_d, mu_d_dot, J_d_dot, mu_d_ddot, J_d_ddot = abs_d(k)
    
    a = abs_state[0, 0:2]
    a_dot = np.array([abs_state[0, 2], aux_state[0, 8]])
    
    phi_1 = (params[0, 0]*aux_state[0, 2] - params[0, 0]*state[0, 0]*aux_state[0, 1])/aux_state[0, 3]
    phi_2 = (params[0, 0]*state[0, 0]*aux_state[0, 0] - params[0, 0]*aux_state[0, 1])/(2*aux_state[0, 3])
    
    a_d = np.array([mu_d, J_d])
    a_d_dot = np.array([mu_d_dot, J_d_dot])
    a_d_ddot = np.array([mu_d_ddot, J_d_ddot])
    
    a_err = a - a_d
    a_err_dot = a_dot - a_d_dot
    
    C_a = np.array([[aux_state[0, 4], aux_state[0, 5]], [aux_state[0, 6], aux_state[0, 7]]])
    Phi_inv = np.array([phi_1, phi_2])
    
    f = np.dot(Phi_inv, params[0, 0]*(np.matmul(K_p, a_err) + np.matmul(K_d - C_a, a_err_dot) - np.array([0, aux_state[0, 9]]) + a_d_ddot) + (params[:, 6] + k_sd)*a_d_dot) - k_sd*state[0, 1]
    
    return np.array([[f]])

if __name__ == '__main__':
    sim = DoubleIntegratorSwarm(limits=limits, del_t=dt, L=L, k_sd=k_sd,  enforce_limits=False, full_state_feedback=False, ctrl=ctrl, name='SI_Swarm_test')
    
    res, p = sim.gen_trajectories(initial_states=sim.init_state_gen(n, swarm_size), horizon=horizon, params=sim.param_gen(n, swarm_size))
    
    abs_states = np.zeros((horizon+1, 2))
    abs_states_d = np.zeros((horizon+1, 2))
    
    for i in range(horizon+1):
        abs_states_d[i, :] = np.array(abs_d(i)[:2])
        abs_states[i, :] = sim.calc_abs_state(res['states'][:, i, :, :], i, p)[0, 0:2]
        
    t = np.arange(horizon+1)*dt
    
    plt.figure()
    plt.plot(t, abs_states)
    plt.plot(t, abs_states_d)
    plt.xlabel('time (s)')
    plt.title('abstract states')
    
    plt.figure()
    plt.plot(t, res['states'][0, :, :, 0])
    plt.xlabel('time (s)')
    plt.ylabel('positions (m)')
    
        