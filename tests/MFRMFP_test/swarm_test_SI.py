"""
Single Integrator Swarm test
"""

import numpy as np
import matplotlib.pyplot as plt

from youngknight.sims import SingleIntegratorSwarm

K_mu = 1.0
K_J = 1.0
dt = 0.01

omega_mu = 0.1*2*np.pi
omega_J = 0.5*2*np.pi
phi_mu = 0.0
phi_J = 0.0

swarm_size = 10
L = 1.0
n = 1
horizon = 2000

limits = {}
limits['p_max'] = 0.25
limits['p_min'] = -0.25
limits['v_max'] = 1.0
limits['v_min'] = -1.0
limits['m_max'] = 0.5
limits['m_min'] = 0.25

def abs_d(k):
    mu_d = np.sin(omega_mu*dt*k + phi_mu)
    J_d = np.sin(omega_J*dt*k + phi_J) + 2.0
    mu_d_dot = omega_mu*np.cos(omega_mu*dt*k + phi_mu)
    J_d_dot = omega_J*np.cos(omega_J*dt*k + phi_J)
    
    return mu_d, J_d, mu_d_dot, J_d_dot

def ctrl(state, k, params, abs_state, aux_state):
    mu_d, J_d, mu_d_dot, J_d_dot = abs_d(k)
    
    d_abs1 = K_mu*(mu_d - abs_state[:, 0]) + mu_d_dot
    d_abs2 = K_J*(J_d - abs_state[:, 1]) + J_d_dot
    
    phi_1 = (params[:, 0]*aux_state[:, 2] - params[:, 0]*state[:, 0]*aux_state[:, 1])/aux_state[:, 3]
    phi_2 = (params[:, 0]*state[:, 0]*aux_state[:, 0] - params[:, 0]*aux_state[:, 1])/(2*aux_state[:, 3])
    
    return d_abs1*phi_1 + d_abs2*phi_2

if __name__ == '__main__':
    
    sim = SingleIntegratorSwarm(limits=limits, del_t=dt, L=L, enforce_limits=False, full_state_feedback=False, ctrl=ctrl, name='SI_Swarm_test')
    
    res, p = sim.gen_trajectories(initial_states=sim.init_state_gen(n, swarm_size), horizon=horizon, params=sim.param_gen(n, swarm_size))
    
    abs_states = np.zeros((horizon+1, 2))
    abs_states_d = np.zeros((horizon+1, 2))
    
    for i in range(horizon+1):
        abs_states_d[i, :] = np.array(abs_d(i)[:2])
        abs_states[i, :] = sim.calc_abs_state(res['states'][:, i, :, :], i, p)[0, 0:2]
        
    t = np.arange(horizon+1)*dt
    
    plt.figure()
    plt.plot(t, abs_states)
    plt.plot(t, abs_states_d)
    plt.xlabel('time (s)')
    plt.title('abstract states')
    
    plt.figure()
    plt.plot(t, res['states'][0, :, :, 0])
    plt.xlabel('time (s)')
    plt.ylabel('positions (m)')
    
        