'''
test basic abstract actor
'''

import numpy as np
import matplotlib.pyplot as plt

from youngknight.sims import MFRMFP
from youngknight.frameworks.a3c.actors import Basic_Abs_Actor
from youngknight.frameworks.a3c import A3C
from youngknight.core import quad

dt = 0.05
swarm_size = 4
L = 1.0
horizon = 2000
home = 'results/'
log_dir = 'logs'
glb_filename = 'global_actor.pkl'
loc_filename = 'local_actor%d.pkl'

Q_p = np.diag([1, 0.0])
Q_si = np.diag([0.1])
R_si = np.diag([0.01])

parent_limits = {}
parent_limits['theta_min'] = -0.5
parent_limits['theta_max'] = 0.5
parent_limits['omega_min'] = -0.5
parent_limits['omega_max'] = 0.5
parent_limits['mu_min'] = -5.0
parent_limits['mu_max'] = 5.0
parent_limits['J_min'] = 1.0
parent_limits['J_max'] = 5.0
parent_limits['J_dot_min'] = -5.0
parent_limits['J_dot_max'] = 5.0
parent_limits['J_p_min'] = 0.5
parent_limits['J_p_max'] = 1.0
parent_limits['gamma_1_min'] = 0.00
parent_limits['gamma_1_max'] = 0.00
parent_limits['gamma_2_min'] = 1000
parent_limits['gamma_2_max'] = 1000
parent_limits['gamma_23_min'] = 300
parent_limits['gamma_23_max'] = 300
parent_limits['gamma_14_min'] = -0.00
parent_limits['gamma_14_max'] = -0.00
parent_limits['gamma_5_min'] = 1000
parent_limits['gamma_5_max'] = 1000
parent_limits['gamma_6_min'] = 1.0
parent_limits['gamma_6_max'] = 1.0

child_limits = {}
child_limits['p_max'] = 0.5
child_limits['p_min'] = -0.5
child_limits['v_max'] = 0.5
child_limits['v_min'] = -0.5
child_limits['m_max'] = 0.5
child_limits['m_min'] = 0.25

actor_params = {}
actor_params['abs_size'] = 3
actor_params['aux_size'] = 5
actor_params['hidden_size_prnt_ctrl'] = 100
actor_params['activ_prnt_ctrl_h'] = 'leakyrelu_0.1'
actor_params['activ_prnt_ctrl_f'] = 'linear'
actor_params['hidden_size_abs_cur'] = 100
actor_params['activ_abs_cur_h'] = 'leakyrelu_0.1'
actor_params['activ_abs_cur_f'] = 'linear'
actor_params['hidden_size_abs_aux'] = 100
actor_params['activ_abs_aux_h'] = 'leakyrelu_0.1'
actor_params['activ_abs_aux_f'] = 'linear'
actor_params['hidden_size_pred'] = 100
actor_params['activ_pred_h'] = 'leakyrelu_0.1'
actor_params['activ_pred_f'] = 'linear'
actor_params['hidden_size_chld_ctrl'] = 100
actor_params['activ_chld_ctrl_h'] = 'leakyrelu_0.1'
actor_params['activ_chld_ctrl_mu'] = 'linear'
actor_params['activ_chld_ctrl_sig'] = 'softplus'
actor_params['hidden_size_val'] = 100
actor_params['activ_val_h'] = 'leakyrelu_0.1'
actor_params['activ_val_f'] = 'linear'
actor_params['sig_min'] = 1e-8
actor_params['gamma'] = 0.9
actor_params['beta_1'] = 0.01
actor_params['beta_2'] = 0.1
actor_params['beta_3'] = 0.1
actor_params['beta_4'] = 1.0
actor_params['pol_learning_rate'] = 0.0001
actor_params['val_learning_rate'] = 0.001
actor_params['verbosity'] = 2
actor_params['T_max'] = 2000
actor_params['t_max'] = 200
actor_params['global_update_rate'] = 5
actor_params['validation_rate'] = 20
actor_params['use_noise'] = False
actor_params['use_err'] = False

#normalize
tot = np.sum(Q_p) + np.sum(Q_si) + np.sum(R_si)
Q_p = Q_p/tot
Q_si = Q_si/tot/swarm_size
R_si = R_si/tot/swarm_size

def prnt_rwd(state, state1, action, k, params):
    cost = quad(state, Q_p)
    return -cost

def chld_rwd(state, state1, action, k, params):
    p = state[:, 0:1]
    cost = quad(p, Q_si) + quad(action, R_si)
    return -cost

if __name__ == '__main__':
    print('setting up simulator...')
    sim = MFRMFP(parent_limits=parent_limits, child_limits=child_limits, swarm_size=swarm_size, dt=dt, L=L, use_double_int=False, k_sd=1.0, enforce_limits=False, parent_full_state_feedback=True, child_full_state_feedback=False, parent_rwd=prnt_rwd, child_rwd=chld_rwd, name='MFRMFP_A3C_test')
    
    print('initializing a3c...')
    a3c = A3C(sim=sim, actor=Basic_Abs_Actor, actor_params=actor_params, use_obs=True, home=home, name='MFRMFP_A3C', log_dir=log_dir)
    
    #a3c.actors[0].train()
    
    print('starting_actors...')
    a3c.start_actors(with_val=True)
    print('started %d threads'%(len(a3c.actor_threads),))
    a3c.join()
    
    a3c.save_results(glb_filename)
    a3c.save_actors(loc_filename)
    
    print('training finished!')
    hists = a3c.get_histories()
    glb_hist = a3c.get_val_history()
    glb_actor = a3c.get_final_actor()
    
    t_max = 600
    
    s_p = np.zeros((1, t_max+1, 2))
    s_c = np.zeros((1, t_max+1, swarm_size, 2))
    o_p = np.zeros((1, t_max+1, 2))
    o_c = np.zeros((1, t_max+1, swarm_size, 1))
    a_c = np.zeros((1, t_max, swarm_size, 1))
    
    abs_cur = np.zeros((1, t_max, actor_params['abs_size']))
    abs_des = np.zeros((1, t_max, actor_params['abs_size']))
    abs_aux = np.zeros((1, t_max, actor_params['aux_size']))
    abs_cal = np.zeros((1, t_max, 7))
    
    params_p = glb_actor.params['sim_prnt_params']
    params_c = glb_actor.params['sim_chld_params']
    
    s_p[:, 0, :] = sim.parent_init_state_gen(1)
    s_c[:, 0, :] = sim.child_init_state_gen(1)
    o_p[:, 0, :] = sim.parent_sim.obs_gen(s_p[:, 0, :], k=0, params=params_p)
    o_c[:, 0, :] = sim.child_sim.obs_gen(s_c[:, 0, :], k=0, params=params_c)
    
    t = np.arange(t_max+1)*dt
    
    #glb_actor.training=True
    
    for i in range(t_max):
        a_c[:, i, :, :] = glb_actor.policy([o_p[:, i, :], o_c[:, i, :, :]])
        s_p_, s_c_, o_p_, o_c_, r, d = sim.step(s_p[:, i, :], s_c[:, i, :, :], a_c[:, i, :, :], k=i, parent_params=params_p, child_params=params_c)
        
        s_p[:, i+1, :] = s_p_
        s_c[:, i+1, :, :] = s_c_
        o_p[:, i+1, :] = o_p_
        o_c[:, i+1, :, :] = o_c_
        
        s_abs = glb_actor.abstract(o_p[:, i, :], o_c[:, i, :])
        
        abs_cur[:, i, :] = s_abs['cur']
        abs_des[:, i, :] = s_abs['des']
        abs_aux[:, i, :] = s_abs['aux']
        abs_cal[:, i, :] = sim.child_sim.calc_abs_state(s_c[:, i, :, :], i, params_c)        
        
    plt.figure()
    plt.plot(a3c.get_glb_r())
    plt.plot(glb_hist['ep_stamp'], glb_hist['running_ep_r'])
    plt.xlabel('global episode number')
    plt.ylabel('running episode reward')
    
    plt.figure()
    for hist in hists:
        plt.plot(np.squeeze(np.array(hist['running_ep_r'])))
    plt.xlabel('local episode number')
    plt.ylabel('running episode reward')
    
    plt.figure()
    for hist in hists:
        plt.plot(np.squeeze(np.array(hist['l_p'])))
    plt.xlabel('update number')
    plt.ylabel('policy loss')
    
    plt.figure()
    for hist in hists:
        plt.plot(np.squeeze(np.array(hist['l_v'])))
    plt.xlabel('update number')
    plt.ylabel('value loss')
    
    plt.figure()
    plt.plot(t, s_p[0, :, :])
    plt.xlabel('time (s)')
    plt.legend(['theta (rad)', 'omega (rad/s)'])
    
    plt.figure()
    plt.plot(t, s_c[0, :, :, 0])
    plt.xlabel('time (s)')
    plt.ylabel('position (m)')
    
    plt.figure()
    plt.plot(t[:-1], abs_cur[0, :, :] - abs_des[0, :, :])
    plt.xlabel('time (s)')
    plt.ylabel('abstract error')
    
    plt.figure()
    plt.plot(t[:-1], abs_cal[0, :, :2])
    plt.xlabel('time (s)')
    plt.legend(['tau', 'J'])
    