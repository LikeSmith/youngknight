'''
sinusoid trajectory test
'''

import numpy as np
import matplotlib.pyplot as plt

from youngknight.trajectories import Sinusoid

limits = {}
limits['min_mag'] = 1.0
limits['max_mag'] = 1.0
limits['min_off'] = 0
limits['max_off'] = 0
limits['min_phi'] = 0
limits['max_phi'] = 0
limits['min_frq'] = 0.1
limits['max_frq'] = 0.1

dt = 0.01
channels = 1
dirs = 3
n = 3
N = 1000

traj = Sinusoid(limits, dt, channels, dirs)

tr = np.zeros((n, N, channels*(dirs+1)))

for i in range(N):
    tr[:, i, :] = traj(i, n)
    
plt.figure()
for i in range(n):
    for j in range(channels*(dirs+1)):
        plt.plot(np.arange(N)*dt, tr[i, :, j])
        