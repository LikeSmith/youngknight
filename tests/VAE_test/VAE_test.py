"""
test VAE Model on MNIST data

this script tests the Variational AutoEncoder
"""

import numpy as np
import tensorflow as tf
#import matplotlib.pyplot as plt
import _pickle as pkl
from youngknight.models import VAE

if __name__=='__main__':
    # load MNIST Data
    print('Loading Training Data...')

    try:
        train_n_rows, train_n_cols, train_size, train_dat, valid_dat, test_dat = pkl.load(open('data/MNIST/MNIST.pkl', 'rb'))
    except FileNotFoundError:
        with open('data/MNIST/MNIST_train_imgs.idx3-ubyte', 'rb') as file:
            if int.from_bytes(file.read(4), byteorder='big') != 2051:
                print('Trainiing Data file corrupt')
                exit(1)
        
            train_size = int.from_bytes(file.read(4), byteorder='big')
            train_n_rows = int.from_bytes(file.read(4), byteorder='big')
            train_n_cols = int.from_bytes(file.read(4), byteorder='big')
            
            train_dat = np.zeros((train_size, train_n_rows, train_n_cols))
            
            for i in range(train_size):
                for j in range(train_n_rows):
                    for k in range(train_n_cols):
                        train_dat[i, j, k] = float(int.from_bytes(file.read(1), byteorder='big'))/255.0
        
        print('Loading Validation Data...')    
        with open('data/MNIST/MNIST_valid_imgs.idx3-ubyte', 'rb') as file:
            if int.from_bytes(file.read(4), byteorder='big') != 2051:
                print('Validation Data file corrupt')
                exit(1)
            
            valid_size = int.from_bytes(file.read(4), byteorder='big')
            valid_n_rows = int.from_bytes(file.read(4), byteorder='big')
            valid_n_cols = int.from_bytes(file.read(4), byteorder='big')
            
            valid_dat = np.zeros((valid_size, valid_n_rows, valid_n_cols))
            
            for i in range(valid_size):
                for j in range(valid_n_rows):
                    for k in range(valid_n_cols):
                        valid_dat[i, j, k] = float(int.from_bytes(file.read(1), byteorder='big'))/255.0
                        
        print('Sorting Test Data')
        with open('data/MNIST/MNIST_valid_labels.idx1-ubyte', 'rb') as file:
            if int.from_bytes(file.read(4), byteorder='big') != 2049:
                print('Test Labels file corrupt')
                exit()
                
            test_size = int.from_bytes(file.read(4), byteorder='big')
            test_labels = np.zeros(test_size, dtype=int)
            
            for i in range(test_size):
                test_labels[i] = int.from_bytes(file.read(1), byteorder='big')
                        
        assert test_size == valid_size
        
        test_dat = []
        
        for i in range(10):
            inds = [j for j in range(test_size) if test_labels[j] == i]
            test_dat += [np.zeros((len(inds), valid_n_rows, valid_n_cols))]
            for j in range(len(inds)):
                test_dat[i][j, :, :] = valid_dat[inds[j], :, :]
        
        assert valid_n_rows == train_n_rows and valid_n_cols == train_n_cols
        
        pkl.dump([train_n_rows, train_n_cols, train_size, train_dat, valid_dat, test_dat], open('data/MNIST/MNIST.pkl', 'wb'))
    
    input_size = [train_n_rows, train_n_cols]
    lat_size = 3
    int_size = 128
    learning_rate = 0.0001
    seed = None
    
    params = {'input_size':input_size, 'int_size':int_size, 'lat_size':lat_size, 'learning_rate':learning_rate, 'seed':seed}
    
    print('Training...')
    mdl = VAE(params, home='data/', name='VAE_test', debug=False)
    mdl.train(mdl.gen_feed(train_dat), mdl.gen_feed(valid_dat), train_size, epochs=1000, batch_size=128, patience=10, verbosity=2)
    
    mdl.save('test1.pkl')
    
    
    