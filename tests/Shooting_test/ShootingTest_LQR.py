"""
ShootingTest_LQR.py

Test shooting method on LQR Problem
"""

import numpy as np
import scipy as sp
import tensorflow as tf
import matplotlib.pyplot as plt

from youngknight.frameworks.Shooting import Shooting
from youngknight.frameworks.Shooting.policies import LinearPolicy
from youngknight.models.Dynamics import LinMSD
from youngknight.models.Rewards import LQRReward
from youngknight.core.utils import rand_vectors

home = 'results/'

n_epochs = 100
n_batches = 1000
batch_size = 128
val_size = 200

dt = 0.05

p_min = -10
p_max = 10
v_min = -10.0
v_max = 10.0
f_min = -10.0
f_max = 10.0

m_min = 1.0
m_max = 1.0
k_min = 0.0
k_max = 0.0
c_min = 0.1
c_max = 0.1

dyn_params = {}
dyn_params['del_t'] = dt
dyn_params['p_min'] = p_min
dyn_params['p_max'] = p_max
dyn_params['v_min'] = v_min
dyn_params['v_max'] = v_max
dyn_params['v_min'] = f_min
dyn_params['v_max'] = f_max
dyn_params['clip_f'] = False
dyn_params['clip_v'] = False
dyn_params['clip_p'] = False

lqr_params = {}
lqr_params['Q'] = np.array([[1.0, 0.0], [0.0, 0.1]])
lqr_params['R'] = np.array([[0.1]])

pol_params = {}
pol_params['use_bias'] = False
pol_params['init_var'] = 0.1

lrn_params = {}
lrn_params['n_steps'] = 100
lrn_params['use_l1_loss'] = False
lrn_params['use_l2_loss'] = False
lrn_params['gamma_l1'] = 0.001
lrn_params['gamma_l2'] = 0.001
lrn_params['trainer'] = tf.train.AdamOptimizer(learning_rate=0.001)
lrn_params['verbosity'] = 2
lrn_params['init_state_gen'] = lambda n: rand_vectors(n, [p_min, v_min], [p_max, v_max])
lrn_params['param_gen'] = lambda n: rand_vectors(n, [m_min, k_min, c_min], [m_max, k_max, c_max])
lrn_params['regen_data'] = True
lrn_params['patience'] = 5
lrn_params['use_best'] = True

if __name__ == '__main__':
    print('Setting up Models...')
    sess = tf.Session()
    dyn = LinMSD(params=dyn_params, home=home, name='dyn_model', sess=sess)
    
    state_size = dyn.state_size
    actin_size = dyn.actin_size
    param_size = dyn.param_size
    
    rwd = LQRReward(state_size, state_size, actin_size, param_size, params=lqr_params, home=home, name='lqr_reward', sess=sess)
    pol = LinearPolicy(state_size, actin_size, param_size, params=pol_params, home=home, name='lin_pol', sess=sess)
    
    print('Building learner...')
    learner = Shooting(dyn, rwd, pol, params=lrn_params, home=home, name='shooting_learner', sess=sess)

    hist = learner.train(n_epochs, n_batches, batch_size, val_size)
    
    print('Done!')
    plt.figure()
    plt.plot(hist['ave_loss'])
    plt.plot(hist['val_loss'])
    plt.xlabel('epoch')
    plt.ylabel('loss')
    plt.legend(['Training', 'Validation'])
    
    print('Simulating Results...')
    
    p = lrn_params['param_gen'](1)
    m = p[0, 0]
    k = p[0, 1]
    c = p[0, 2]
    
    A = np.array([[1-dt**2*k/m, dt-dt**2*c/m], [-dt*k/m, 1-dt*c/m]])
    B = np.array([[dt**2/m], [dt/m]])
    
    Q = lqr_params['Q']
    R = lqr_params['R']
    
    P = sp.linalg.solve_discrete_are(A, B, Q, R)
    K = np.matmul(np.linalg.inv(R + np.matmul(np.matmul(B.T, P), B)), np.matmul(np.matmul(B.T, P), A))
    
    weights = pol.save_weights()
    
    print('K actual:')
    print(K)
    print('K learned:')
    print(weights['K'])
    