"""
ShootingTest_Basic_Adv.py

Test shooting method with advantage on pendulum
"""

import numpy as np
import tensorflow as tf
import matplotlib.pyplot as plt

from youngknight.frameworks.Shooting import ShootingAdv
from youngknight.frameworks.Shooting.policies import BasicAdv
from youngknight.frameworks.Shooting.ctg import BasicCTG
from youngknight.models.Dynamics import PendulumDyn, PendulumObs
from youngknight.models.Rewards import PendReward
from youngknight.core.utils import rand_vectors

home = 'results/'

n_epochs = 100
n_batches = 1000
batch_size = 128
val_size = 200

dt = 0.05

theta_min = -np.pi
theta_max = np.pi
omega_min = -8.0
omega_max = 8.0
tau_min = -2.0
tau_max = 2.0

m_min = 1.0
m_max = 1.0
l_min = 1.0
l_max = 1.0
c_min = 0.0
c_max = 0.0
g_min = 9.81
g_max = 9.81

dyn_params = {}
dyn_params['del_t'] = dt
dyn_params['tau_min'] = tau_min
dyn_params['tau_max'] = tau_max
dyn_params['omega_min'] = omega_min
dyn_params['omega_max'] = omega_max
dyn_params['theta_min'] = theta_min
dyn_params['theta_max'] = theta_max
dyn_params['clip_tau'] = True
dyn_params['clip_omega'] = True
dyn_params['clip_theta'] = False
dyn_params['inv'] = True

rwd_params = {}
rwd_params['theta_gain'] = 1.0
rwd_params['omega_gain'] = 0.1
rwd_params['tau_gain'] = 0.001

pol_params = {}
pol_params['h_size'] = 100
pol_params['use_bias'] = True
pol_params['activ'] = 'leakyrelu_0.1'
pol_params['a_min'] = [tau_min]
pol_params['a_max'] = [tau_max]
pol_params['sig_min'] = 1e-4
pol_params['beta_entropy'] = 0.01
pol_params['init_var'] = 0.1

ctg_params = {}
ctg_params['h_size'] = 100
ctg_params['use_bias'] = True
ctg_params['activ'] = 'leakyrelu_0.1'
ctg_params['init_var'] = 0.1

lrn_params = {}
lrn_params['n_steps'] = 100
lrn_params['gamma'] = 0.9
lrn_params['use_l1_loss_pol'] = False
lrn_params['use_l2_loss_pol'] = False
lrn_params['use_l1_loss_ctg'] = False
lrn_params['use_l2_loss_ctg'] = False
lrn_params['gamma_l1_pol'] = 0.001
lrn_params['gamma_l2_pol'] = 0.001
lrn_params['gamma_l1_ctg'] = 0.001
lrn_params['gamma_l2_ctg'] = 0.001
lrn_params['trainer_pol'] = tf.train.RMSPropOptimizer(learning_rate=0.0001)
lrn_params['trainer_ctg'] = tf.train.RMSPropOptimizer(learning_rate=0.001)
lrn_params['verbosity'] = 2
lrn_params['init_state_gen'] = lambda n: rand_vectors(n, [theta_min, 1.0], [theta_max, -1.0])
lrn_params['param_gen'] = lambda n: rand_vectors(n, [m_min, l_min, c_min, g_min], [m_max, l_max, c_max, g_max])
lrn_params['regen_data'] = True
lrn_params['patience'] = 10
lrn_params['use_best'] = True

if __name__ == '__main__':
    print('Setting up Models...')
    sess = tf.Session()
    dyn = PendulumDyn(params=dyn_params, home=home, name='pend_model_dyn', sess=sess)
    obs = PendulumObs(home=home, name='pend_model_obs', sess=sess)

    state_size = dyn.state_size
    obsrv_size = obs.obsrv_size
    actin_size = dyn.actin_size
    param_size = dyn.param_size

    rwd = PendReward(state_size, obsrv_size, actin_size, param_size, params=rwd_params, home=home, name='lqr_reward', sess=sess)
    pol = BasicAdv(obsrv_size, actin_size, param_size, params=pol_params, home=home, name='basic_pol', sess=sess)
    ctg = BasicCTG(obsrv_size, param_size, params=ctg_params, home=home, name='basic_ctg', sess=sess)

    print('Building learner...')
    learner = ShootingAdv(dyn, rwd, pol, ctg, obs=obs, params=lrn_params, home=home, name='shooting_learner', sess=sess)

    hist = learner.train(n_epochs, n_batches, batch_size, val_size)

    print('Done!')
    t = np.arange(lrn_params['n_steps']+1)*dt

    plt.figure()
    plt.plot(hist['ave_ep_c'])
    plt.plot(hist['val_ep_c'])
    plt.xlabel('epoch')
    plt.ylabel('loss')
    plt.legend(['Training', 'Validation'])

    plt.figure()
    plt.plot(t, hist['s'][hist['min_loss_epoch'], :, :, 0].T)
    plt.xlabel('time (s)')
    plt.ylabel('theta (rad)')

    plt.figure()
    plt.plot(t, hist['s'][hist['min_loss_epoch'], :, :, 1].T)
    plt.xlabel('time (s)')
    plt.ylabel('omega (rad/s)')

    plt.figure()
    plt.plot(t[:-1], hist['a'][hist['min_loss_epoch'], :, :, 0].T)
    plt.xlabel('time (s)')
    plt.ylabel('tau (N-m)')
