"""
ModelBaseA3c_test_linMSD.py

Test of Model Based A3C
"""

import numpy as np
import tensorflow as tf
import matplotlib.pyplot as plt

from youngknight.frameworks.ModelBasedA3C import A3C
from youngknight.frameworks.ModelBasedA3C.actors import Actor
from youngknight.frameworks.ModelBasedA3C.policies import LinearPolicy
from youngknight.frameworks.ModelBasedA3C.values import BasicValue
from youngknight.models.Dynamics import LinMSD
from youngknight.models.Rewards import LQRReward
from youngknight.core.utils import rand_vectors

home = 'results/'
log_dir = 'logs/'

dt = 0.05

p_min = -10
p_max = 10
v_min = -10.0
v_max = 10.0
f_min = -10.0
f_max = 10.0

m_min = 1.0
m_max = 1.0
k_min = 0.0
k_max = 0.0
c_min = 0.1
c_max = 0.1

pend_params = {}
pend_params['del_t'] = dt
pend_params['p_min'] = p_min
pend_params['p_max'] = p_max
pend_params['v_min'] = v_min
pend_params['v_max'] = v_max
pend_params['v_min'] = f_min
pend_params['v_max'] = f_max
pend_params['clip_f'] = False
pend_params['clip_v'] = False
pend_params['clip_p'] = False

lqr_params = {}
lqr_params['Q'] = np.array([[1.0, 0.0], [0.0, 0.1]])
lqr_params['R'] = np.array([[0.001]])

pol_params = {}
pol_params['h_size'] = 200
pol_params['activ'] = 'leakyrelu_0.1'
pol_params['use_bias'] = True
pol_params['use_l1_loss'] = False
pol_params['use_l2_loss'] = False
pol_params['sig_min'] = 1e-4
pol_params['beta_entropy'] = 0.01
pol_params['init_var'] = 0.01

val_params = {}
val_params['h_size'] = 100
val_params['activ'] = 'leakyrelu_0.1'
val_params['use_bias'] = True
val_params['use_l1_loss'] = False
val_params['use_l2_loss'] = True
val_params['init_var'] = 0.1

actor_params = {}
actor_params['gamma'] = 0.9
actor_params['block_size'] = 10
actor_params['pol_trainer'] = tf.train.RMSPropOptimizer(learning_rate=0.0001)
actor_params['val_trainer'] = tf.train.RMSPropOptimizer(learning_rate=0.001)
actor_params['get_init_state'] = lambda n: rand_vectors(n, [p_min, 0], [p_max, 0])
actor_params['get_params'] = lambda n: rand_vectors(n, [m_min, k_min, c_min], [m_max, k_max, c_max])
actor_params['batch_size'] = 10
actor_params['T_max'] = 2000
actor_params['t_max'] = 200
actor_params['verbosity'] = 2
actor_params['validation_rate'] = 10
#actor_params['validation_patience'] = 10
actor_params['new_param_rate'] = 50

if __name__ == '__main__':
    print('setting up actors...')
    sess = tf.Session()
    
    dyn = LinMSD(params=pend_params, home=home, name='pend_model_dyn', sess=sess)
    
    state_size = dyn.state_size
    actin_size = dyn.actin_size
    param_size = dyn.param_size
    
    rwd = LQRReward(state_size, state_size, actin_size, param_size, params=lqr_params, home=home, name='lqr_reward', sess=sess)
    pol = LinearPolicy(state_size[0], actin_size[0], param_size[0], params=pol_params, home=home, name='Basic_Policy', sess=sess)
    val = BasicValue(state_size[0], param_size[0], params=val_params, home=home, name='Basic_Value', sess=sess)
    
    glb_actor = Actor(dyn, rwd, pol, val, params=actor_params, home=home, name='Actor', sess=sess)
    
    print('setting up learner...')
    learner = A3C(glb_actor, home=home, name='A3C_learner', sess=sess, log_dir=log_dir)
    
    #learner.actors[0].train()
    
    print('begining training...')
    learner.start_actors()
    learner.join()
    
    print('done!')
    hists = learner.get_histories()
    val_hist = learner.get_val_history()
    
    plt.figure()
    plt.plot(learner.get_glb_r())
    plt.plot(val_hist['ep_stamp'], val_hist['running_ep_r'])
    plt.xlabel('global episode number')
    plt.ylabel('running episode reward')
    
    plt.figure()
    for hist in hists:
        plt.plot(np.squeeze(np.array(hist['running_ep_r'])))
    plt.xlabel('local episode number')
    plt.ylabel('running episode reward')
    
    plt.figure()
    for hist in hists:
        plt.plot(np.squeeze(np.array(hist['l_p'])))
    plt.xlabel('update number')
    plt.ylabel('policy loss')
    
    plt.figure()
    for hist in hists:
        plt.plot(np.squeeze(np.array(hist['l_v'])))
    plt.xlabel('update number')
    plt.ylabel('value loss')
