"""
ModelBaseA3c_test.py

Test of Model Based A3C
"""

import numpy as np
import tensorflow as tf
import matplotlib.pyplot as plt

from youngknight.frameworks.ModelBasedA3C import A3C
from youngknight.frameworks.ModelBasedA3C.actors import Actor
from youngknight.frameworks.ModelBasedA3C.policies import BasicPolicy
from youngknight.frameworks.ModelBasedA3C.values import BasicValue
from youngknight.models.Dynamics import PendulumDyn, PendulumObs
from youngknight.models.Rewards import LQRReward
from youngknight.core.utils import rand_vectors

home = 'results/'
log_dir = 'logs/'

dt = 0.05

theta_min = -np.pi/4.0
theta_max = np.pi/4.0
omega_min = -8.0
omega_max = 8.0
tau_min = -15.0
tau_max = 15.0

m_min = 1.0
m_max = 1.0
l_min = 1.0
l_max = 1.0
c_min = 0.0
c_max = 0.0
g_min = 9.81
g_max = 9.81

pend_params = {}
pend_params['del_t'] = dt
pend_params['tau_min'] = tau_min
pend_params['tau_max'] = tau_max
pend_params['omega_min'] = omega_min
pend_params['omega_max'] = omega_max
pend_params['clip_tau'] = True
pend_params['clip_omega'] = True
pend_params['clip_theta'] = False
pend_params['inv'] = True

lqr_params = {}
lqr_params['Q'] = np.array([[1.0, 0.0], [0.0, 0.1]])
lqr_params['R'] = np.array([[0.001]])

pol_params = {}
pol_params['h_size'] = 200
pol_params['activ'] = 'leakyrelu_0.1'
pol_params['use_bias'] = False
pol_params['use_l1_loss'] = False
pol_params['use_l2_loss'] = True
pol_params['a_min'] = [tau_min]
pol_params['a_max'] = [tau_max]
pol_params['sig_min'] = 1e-4
pol_params['beta_entropy'] = 0.01
pol_params['init_var'] = 0.1

val_params = {}
val_params['h_size'] = 100
val_params['activ'] = 'leakyrelu_0.1'
val_params['use_bias'] = True
val_params['use_l1_loss'] = False
val_params['use_l2_loss'] = True
val_params['init_var'] = 0.05

actor_params = {}
actor_params['gamma'] = 0.9
actor_params['block_size'] = 10
#actor_params['pol_trainer'] = tf.train.RMSPropOptimizer(learning_rate=0.00001)
#actor_params['val_trainer'] = tf.train.RMSPropOptimizer(learning_rate=0.0001)
actor_params['pol_trainer'] = tf.train.AdamOptimizer(learning_rate=0.000001)
actor_params['val_trainer'] = tf.train.AdamOptimizer(learning_rate=0.0001)
actor_params['get_init_state'] = lambda n: rand_vectors(n, [theta_min, 0], [theta_max, 0])
actor_params['get_params'] = lambda n: rand_vectors(n, [m_min, l_min, c_min, g_min], [m_max, l_max, c_max, g_max])
actor_params['batch_size'] = 10
actor_params['T_max'] = 10000
actor_params['t_max'] = 200
actor_params['verbosity'] = 2
actor_params['validation_rate'] = 10
#actor_params['validation_patience'] = 10
actor_params['new_param_rate'] = 50

if __name__ == '__main__':
    print('setting up actors...')
    sess = tf.Session()
    
    dyn = PendulumDyn(params=pend_params, home=home, name='pend_model_dyn', sess=sess)
    obs = PendulumObs(home=home, name='pend_model_obs', sess=sess)
    
    state_size = dyn.state_size
    actin_size = dyn.actin_size
    param_size = dyn.param_size
    obsrv_size = obs.obsrv_size
    
    rwd = LQRReward(state_size, obsrv_size, actin_size, param_size, params=lqr_params, home=home, name='lqr_reward', sess=sess)
    pol = BasicPolicy(obsrv_size[0], actin_size[0], param_size[0], params=pol_params, home=home, name='Basic_Policy', sess=sess)
    val = BasicValue(obsrv_size[0], param_size[0], params=val_params, home=home, name='Basic_Value', sess=sess)
    
    glb_actor = Actor(dyn, rwd, pol, val, obs=obs, params=actor_params, home=home, name='Actor', sess=sess)
    
    print('setting up learner...')
    learner = A3C(glb_actor, home=home, name='A3C_learner', sess=sess, log_dir=log_dir)
    
    #learner.actors[0].train()
    
    print('begining training...')
    learner.start_actors()
    learner.join()
    
    print('done!')
    hists = learner.get_histories()
    val_hist = learner.get_val_history()
    
    plt.figure()
    plt.plot(learner.get_glb_r())
    plt.plot(val_hist['ep_stamp'], val_hist['running_ep_r'])
    plt.xlabel('global episode number')
    plt.ylabel('running episode reward')
    
    plt.figure()
    for hist in hists:
        plt.plot(np.squeeze(np.array(hist['running_ep_r'])))
    plt.xlabel('local episode number')
    plt.ylabel('running episode reward')
    
    plt.figure()
    for hist in hists:
        plt.plot(np.squeeze(np.array(hist['l_p'])))
    plt.xlabel('update number')
    plt.ylabel('policy loss')
    
    plt.figure()
    for hist in hists:
        plt.plot(np.squeeze(np.array(hist['l_v'])))
    plt.xlabel('update number')
    plt.ylabel('value loss')
