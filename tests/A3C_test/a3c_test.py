"""
a3c test code
"""

import numpy as np
import matplotlib.pyplot as plt

from youngknight.frameworks.a3c.actors import Basic_Actor_Cont
from youngknight.frameworks.a3c import A3C
from youngknight.sims import Pendulum

if __name__ == '__main__':
    sim_limits = {}
    sim_limits['tau_min'] = -5
    sim_limits['tau_max'] = 5
    sim_limits['m_min'] = 1.0
    sim_limits['m_max'] = 1.0
    sim_limits['l_min'] = 1.0
    sim_limits['l_max'] = 1.0
    sim_limits['c_min'] = 0.0
    sim_limits['c_max'] = 0.0
    sim_limits['g_min'] = 9.81
    sim_limits['g_max'] = 9.81
    sim_limits['theta_min'] = -np.pi
    sim_limits['theta_max'] = np.pi
    sim_limits['omega_min'] = -10
    sim_limits['omega_max'] = 10
    
    sim_noise_params = {}
    sim_noise_params['process_mean'] = np.zeros((1, 2))
    sim_noise_params['process_cov'] = np.eye(2)*0.01
    sim_noise_params['signal_mean'] = np.zeros((1, 1))
    sim_noise_params['signal_cov'] = np.eye(1)*0.01
    
    actor_params = {}
    actor_params['hidden_size_policy'] = 200
    actor_params['hidden_size_value'] = 100
    actor_params['activ_policy'] = 'relu6'
    actor_params['activ_value'] = 'relu6'
    actor_params['min_a'] = [-5]
    actor_params['max_a'] = [5]
    actor_params['pol_learning_rate'] = 0.0001
    actor_params['val_learning_rate'] = 0.001
    actor_params['T_max'] = 2000
    actor_params['t_max'] = 200
    actor_params['global_update_rate'] = 10 
    actor_params['validation_rate'] = 10
    actor_params['valication_patience'] = 100
    actor_params['gamma'] = 0.9
    actor_params['beta_entropy'] = 0.01
    actor_params['sig_min'] = 1e-4
    actor_params['use_noise'] = False
    actor_params['zero_reg'] = 0
    actor_params['use_l1_loss'] = False
    actor_params['use_l2_loss'] = False
    actor_params['use_bias'] = True
    actor_params['use_es'] = False
    actor_params['verbosity'] = 2
    
    dt = 0.05
    use_obs = False
    home = 'results/'
    log_dir = 'logs'
    glb_filename = 'global_actor.pkl'
    loc_filename = 'local_actor%d.pkl'
    
    print('setting up simulator...')
    sim = Pendulum(sim_limits, sim_noise_params, dt, t_f=actor_params['t_max'], enforce_lims=True)
    actor_params['sim_params'] = sim.param_gen(1)
    
    print('initializing_framework...')
    a3c_framework = A3C(sim, Basic_Actor_Cont, actor_params, use_gym=False, use_obs=use_obs, home=home, name='A3C_test', log_dir=log_dir)
    
    print('starting_actors...')
    a3c_framework.start_actors()
    a3c_framework.join()
    
    a3c_framework.save_results(glb_filename)
    a3c_framework.save_actors(loc_filename)
    
    print('training finished!')
    hists = a3c_framework.get_histories()
    
    plt.figure()
    for hist in hists:
        plt.plot(np.squeeze(np.array(hist['l_p'])))
    plt.xlabel('update number')
    plt.ylabel('policy loss')
    
    plt.figure()
    for hist in hists:
        plt.plot(np.squeeze(np.array(hist['l_v'])))
    plt.xlabel('update number')
    plt.ylabel('value loss')
    
    plt.figure()
    for hist in hists:
        plt.plot(np.squeeze(np.array(hist['running_ep_r'])))
    plt.xlabel('local episode number')
    plt.ylabel('running episode reward')
    
    plt.figure()
    plt.plot(a3c_framework.get_glb_r())
    plt.xlabel('global episode number')
    plt.ylabel('running episode reward')
    
    ctrl = lambda state, params, k, des_traj: a3c_framework.get_final_actor().policy(state, k)
    
    sim = Pendulum(sim_limits, sim_noise_params, dt, ctrl=ctrl, full_state_feedback=(not use_obs), enforce_lims=True)
    
    x_0 = sim.init_state_gen(10)
    
    print('running simulation...')
    n = 1000
    res, _ = sim.gen_trajectories(x_0, n, params=actor_params['sim_params'], verbosity=1)
    t = np.arange(n+1)*dt
    
    plt.figure()
    plt.plot(t.T, res['states'][:, :, 0].T)
    plt.title('Test Run: Position')
    
    plt.figure()
    plt.plot(t.T, res['states'][:, :, 1].T)
    plt.title('Test Run: Velocity')
    
    plt.figure()
    plt.plot(t[:-1].T, res['actions'][:, :, 0].T)
    
    plt.figure()
    plt.plot(t[:-1].T, res['rewards'].T)
    
    plt.show()
    
    