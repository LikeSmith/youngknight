"""
a3c test code
"""

import numpy as np
import matplotlib.pyplot as plt
import gym

from youngknight.frameworks.a3c.actors import Basic_Actor_Cont
from youngknight.frameworks.a3c import A3C

if __name__ == '__main__':
    actor_params = {}
    actor_params['hidden_size_policy'] = 200
    actor_params['hidden_size_value'] = 100
    actor_params['activ_policy'] = 'relu6'
    actor_params['activ_value'] = 'relu6'
    actor_params['min_a'] = -2
    actor_params['max_a'] = 2
    actor_params['seed'] = None
    actor_params['pol_learning_rate'] = 0.0001
    actor_params['val_learning_rate'] = 0.001
    actor_params['T_max'] = 2000
    actor_params['t_max'] = 200
    actor_params['global_update_rate'] = 10 
    actor_params['gamma'] = 0.9
    actor_params['beta_entropy'] = 0.01
    actor_params['sig_min'] = 1e-4
    actor_params['use_noise'] = False
    actor_params['verbosity'] = 2
    
    dt = 0.05
    use_obs = False
    home = 'results/'
    log_dir = 'logs/'
    glb_filename = 'global_actor.pkl'
    loc_filename = 'local_actor%d.pkl'
        
    print('initializing_framework...')
    a3c_framework = A3C('Pendulum-v0', Basic_Actor_Cont, actor_params,  use_gym=True, use_obs=use_obs, home=home, name='A3C_test', log_dir=log_dir)
    
    print('starting_actors...')
    a3c_framework.start_actors()
    a3c_framework.join()
    
    a3c_framework.save_results(glb_filename)
    a3c_framework.save_actors(loc_filename)
    
    print('training finished!')
    hists = a3c_framework.get_histories()
    
    plt.figure()
    for hist in hists:
        plt.plot(np.squeeze(np.array(hist['l_p'])))
    plt.xlabel('update number')
    plt.ylabel('policy loss')
    
    plt.figure()
    for hist in hists:
        plt.plot(np.squeeze(np.array(hist['l_v'])))
    plt.xlabel('update number')
    plt.ylabel('value loss')
    
    plt.figure()
    for hist in hists:
        plt.plot(np.squeeze(np.array(hist['running_ep_r'])))
    plt.xlabel('local episode number')
    plt.ylabel('running episode reward')
    
    plt.figure()
    plt.plot(a3c_framework.get_glb_r())
    plt.xlabel('global episode number')
    plt.ylabel('running episode reward')
    
    ctrl = lambda state, params: a3c_framework.get_final_actor().policy(state)

    n = 1000
    t = np.arange(n+1)*dt
    a = np.zeros(n)
    s = np.zeros((n+1, 3))
    r = np.zeros(n)
    th = np.zeros(n+1)
    
    env = gym.make('Pendulum-v0')
    
    s[0, :] = env.reset()
    th[0] = env.env.state[0]
    
    for i in range(n):
        a[i] = ctrl(s[i:i+1, :], None)
        s_, r_, _, _ = env.step(a[i:i+1])
        r[i] = r_
        s[i+1, :] = s_
        th[i+1] = env.env.state[0]
    
    plt.figure()
    plt.plot(t, s[:, :2])
    
    plt.figure()
    plt.plot(t, th)
    
    plt.figure()
    plt.plot(t, s[:, 2])
    
    plt.figure()
    plt.plot(t[:-1], a)
    
    plt.figure()
    plt.plot(t[:-1], r)
    
    plt.show()
    