"""
Asynchronous Advantage Actor Critic (A3C) with continuous action space, Reinforcement Learning.

The Pendulum example.

View more on my tutorial page: https://morvanzhou.github.io/tutorials/

Using:
tensorflow 1.8.0
gym 0.10.5
"""

import multiprocessing
import threading
import tensorflow as tf
import numpy as np
from youngknight.sims.Pendulum import Pendulum
import os
import shutil
import matplotlib.pyplot as plt
import gym

OUTPUT_GRAPH = True
LOG_DIR = './log'
N_WORKERS = multiprocessing.cpu_count()
MAX_EP_STEP = 200
MAX_GLOBAL_EP = 4000
GLOBAL_NET_SCOPE = 'Global_Net'
UPDATE_GLOBAL_ITER = 10
GAMMA = 0.9
ENTROPY_BETA = 0.01
LR_A = 0.0001    # learning rate for actor
LR_C = 0.001      # learning rate for critic
GLOBAL_RUNNING_R = []
GLOBAL_EP = 0

sim_limits = {}
sim_limits['tau_min'] = -2
sim_limits['tau_max'] = 2
sim_limits['m_min'] = 1.0
sim_limits['m_max'] = 1.0
sim_limits['l_min'] = 1.0
sim_limits['l_max'] = 1.0
sim_limits['c_min'] = 0.0
sim_limits['c_max'] = 0.0
sim_limits['g_min'] = 10
sim_limits['g_max'] = 10
sim_limits['theta_min'] = -np.pi
sim_limits['theta_max'] = np.pi
sim_limits['omega_min'] = -8
sim_limits['omega_max'] = 8

sim_noise_params = {}
sim_noise_params['process_mean'] = np.zeros((1, 2))
sim_noise_params['process_cov'] = np.eye(2)*0.01
sim_noise_params['signal_mean'] = np.zeros((1, 1))
sim_noise_params['signal_cov'] = np.eye(1)*0.01
    
dt = 0.05

sim = Pendulum(sim_limits, sim_noise_params, dt, enforce_lims=True)

N_S = 3
N_A = 1
A_BOUND = [sim_limits['tau_min'], sim_limits['tau_max']]
params = sim.param_gen(1)

actor_params = {}
actor_params['hidden_size_policy'] = 200
actor_params['hidden_size_value'] = 100
actor_params['activ_policy'] = 'relu6'
actor_params['activ_value'] = 'relu6'
actor_params['min_a'] = -2
actor_params['max_a'] = 2
actor_params['seed'] = None
actor_params['pol_learning_rate'] = 1e-4
actor_params['val_learning_rate'] = 1e-4
actor_params['T_max'] = 2000
actor_params['t_max'] = 200
actor_params['global_update_rate'] = 10 
actor_params['gamma'] = 0.9
actor_params['beta_entropy'] = 0.01
actor_params['sig_min'] = 1e-4
actor_params['use_noise'] = False
actor_params['verbosity'] = 2

use_obs = True
home = 'results/'
from youngknight.frameworks.a3c.actors import Basic_Actor_Cont
from youngknight.frameworks.a3c import A3C
a3c_framework = A3C('Pendulum-v0', Basic_Actor_Cont, actor_params,  use_gym=True, use_obs=use_obs, home=home, name='A3C_test')

class ACNet(object):
    def __init__(self, scope, globalAC=None):

        if scope == GLOBAL_NET_SCOPE:   # get global network
            with tf.variable_scope(scope):
                self.s = tf.placeholder(tf.float32, [None, N_S], 'S')
                self.a_params, self.c_params = self._build_net(scope)[-2:]
        else:   # local net, calculate losses
            with tf.variable_scope(scope):
                self.s = tf.placeholder(tf.float32, [None, N_S], 'S')
                self.a_his = tf.placeholder(tf.float32, [None, N_A], 'A')
                self.v_target = tf.placeholder(tf.float32, [None, 1], 'Vtarget')

                mu, sigma, self.v, self.a_params, self.c_params = self._build_net(scope)

                td = tf.subtract(self.v_target, self.v, name='TD_error')
                with tf.name_scope('c_loss'):
                    self.c_loss = tf.reduce_mean(tf.square(td))

                with tf.name_scope('wrap_a_out'):
                    mu, sigma = mu * A_BOUND[1], sigma + 1e-4
                self.mu = mu
                normal_dist = tf.distributions.Normal(mu, sigma)

                with tf.name_scope('a_loss'):
                    log_prob = normal_dist.log_prob(self.a_his)
                    exp_v = log_prob * tf.stop_gradient(td)
                    entropy = normal_dist.entropy()  # encourage exploration
                    self.exp_v = ENTROPY_BETA * entropy + exp_v
                    self.a_loss = tf.reduce_mean(-self.exp_v)

                with tf.name_scope('choose_a'):  # use local params to choose action
                    self.A = tf.clip_by_value(tf.squeeze(normal_dist.sample(1), axis=[0, 1]), A_BOUND[0], A_BOUND[1])
                with tf.name_scope('local_grad'):
                    self.a_grads = tf.gradients(self.a_loss, self.a_params)
                    self.c_grads = tf.gradients(self.c_loss, self.c_params)

            with tf.name_scope('sync'):
                with tf.name_scope('pull'):
                    self.pull_a_params_op = [l_p.assign(g_p) for l_p, g_p in zip(self.a_params, globalAC.a_params)]
                    self.pull_c_params_op = [l_p.assign(g_p) for l_p, g_p in zip(self.c_params, globalAC.c_params)]
                with tf.name_scope('push'):
                    self.update_a_op = OPT_A.apply_gradients(zip(self.a_grads, globalAC.a_params))
                    self.update_c_op = OPT_C.apply_gradients(zip(self.c_grads, globalAC.c_params))

    def _build_net(self, scope):
        w_init = tf.random_normal_initializer(0., .1)
        with tf.variable_scope('actor'):
            l_a = tf.layers.dense(self.s, 200, tf.nn.relu6, kernel_initializer=w_init, name='la')
            mu = tf.layers.dense(l_a, N_A, tf.nn.tanh, kernel_initializer=w_init, name='mu')
            sigma = tf.layers.dense(l_a, N_A, tf.nn.softplus, kernel_initializer=w_init, name='sigma')
        with tf.variable_scope('critic'):
            l_c = tf.layers.dense(self.s, 100, tf.nn.relu6, kernel_initializer=w_init, name='lc')
            v = tf.layers.dense(l_c, 1, kernel_initializer=w_init, name='v')  # state value
        a_params = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES, scope=scope + '/actor')
        c_params = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES, scope=scope + '/critic')
        return mu, sigma, v, a_params, c_params

    def update_global(self, feed_dict):  # run by a local
        SESS.run([self.update_a_op, self.update_c_op], feed_dict)  # local grads applies to global net

    def pull_global(self):  # run by a local
        SESS.run([self.pull_a_params_op, self.pull_c_params_op])

    def choose_action(self, s, use_sample=True):  # run by a local
        s = s[np.newaxis, :]
        if use_sample:
            return SESS.run(self.A, {self.s: s})
        else:
            return SESS.run(self.mu, {self.s: s})

class Worker(object):
    def __init__(self, name, globalAC):
        self.name = name
        self.AC = ACNet(name, globalAC)
        self.env = gym.make('Pendulum-v0').unwrapped
        self.running_R = []

    def work(self):
        global GLOBAL_RUNNING_R, GLOBAL_EP
        total_step = 1
        buffer_s, buffer_a, buffer_r = [], [], []
        self.hist = {}
        self.hist['l_a'] = []
        self.hist['l_c'] = []
        
        while not COORD.should_stop() and GLOBAL_EP < MAX_GLOBAL_EP:
            #s = np.squeeze(sim.init_state_gen(1))
            #o = np.squeeze(sim.obs_gen(np.expand_dims(s, axis=0), 0, params=params))
            o = self.env.reset()
            s = self.env.state
            
            ep_r = 0
            for ep_t in range(MAX_EP_STEP):
                #if self.name == 'W_0':
                #    self.env.render()
                a = self.AC.choose_action(o)
                #s_, o_, r, done = sim.step(np.expand_dims(s, axis=0), np.expand_dims(a, axis=0), ep_t, params=params)
                #s_ = np.squeeze(s_)
                #o_ = np.squeeze(o_)
                #r = r[0]
                o_, r, done, _ = self.env.step(a)
                s_ = self.env.state
                    
                done = True if ep_t == MAX_EP_STEP - 1 else False

                ep_r += r
                buffer_s.append(o)
                buffer_a.append(a)
                buffer_r.append((r+8)/8)    # normalize

                if total_step % UPDATE_GLOBAL_ITER == 0 or done:   # update global and assign to local net
                    if done:
                        v_s_ = 0   # terminal
                    else:
                        v_s_ = SESS.run(self.AC.v, {self.AC.s: o_[np.newaxis, :]})[0, 0]
                    buffer_v_target = []
                    for r in buffer_r[::-1]:    # reverse buffer r
                        v_s_ = r + GAMMA * v_s_
                        buffer_v_target.append(v_s_)
                    buffer_v_target.reverse()

                    buffer_s, buffer_a, buffer_v_target = np.vstack(buffer_s), np.vstack(buffer_a), np.vstack(buffer_v_target)
                    feed_dict = {
                        self.AC.s: buffer_s,
                        self.AC.a_his: buffer_a,
                        self.AC.v_target: buffer_v_target,
                    }
                    self.AC.update_global(feed_dict)
                    l_c = SESS.run(self.AC.c_loss, feed_dict)
                    l_a = SESS.run(self.AC.a_loss, feed_dict)
                    
                    self.hist['l_c'].append(l_c)
                    self.hist['l_a'].append(l_a)
    
                    buffer_s, buffer_a, buffer_r = [], [], []
                    self.AC.pull_global()

                s = s_
                o = o_
                total_step += 1
                if done:
                    if len(self.running_R) == 0:
                        self.running_R.append(ep_r)
                    else:
                        self.running_R.append(0.9*self.running_R[-1] + 0.1*ep_r)
                    if len(GLOBAL_RUNNING_R) == 0:  # record running episode reward
                        GLOBAL_RUNNING_R.append(ep_r)
                    else:
                        GLOBAL_RUNNING_R.append(0.9 * GLOBAL_RUNNING_R[-1] + 0.1 * ep_r)
                    print(
                        self.name,
                        "Ep:", GLOBAL_EP,
                        "| Ep_r: %i" % GLOBAL_RUNNING_R[-1],
                          )
                    GLOBAL_EP += 1
                    break
                
    def simulate(self):
        s_record = []
        o_record = []
        a_record = []
        r_record = []
        
        s = np.squeeze(sim.init_state_gen(1))
        o = np.squeeze(sim.obs_gen(np.expand_dims(s, axis=0), 0, params=params))
        ep_r = 0
        
        self.AC.pull_global()
        
        for ep_t in range(MAX_EP_STEP):
            a = self.AC.choose_action(o, False)
            s_, o_, r, done = sim.step(np.expand_dims(s, axis=0), np.expand_dims(a, axis=0), ep_t, params=params)
            s_ = np.squeeze(s_)
            o_ = np.squeeze(o_)
            r = np.squeeze(r)
            
            ep_r += r
            s_record += [s]
            o_record += [o]
            a_record += [a]
            r_record += [r]
            
            s = s_
            o = o_
        
        s_record += [s]
        o_record += [o]
        
        return [s_record, o_record, a_record, r_record, ep_r]

if __name__ == "__main__":
    SESS = tf.Session()
    #SESS = a3c_framework.sess
    
    with tf.device("/cpu:0"):
        OPT_A = tf.train.RMSPropOptimizer(LR_A, name='RMSPropA')
        OPT_C = tf.train.RMSPropOptimizer(LR_C, name='RMSPropC')
        GLOBAL_AC = ACNet(GLOBAL_NET_SCOPE)  # we only need its params
        workers = []
        # Create worker
        for i in range(N_WORKERS):
            i_name = 'W_%i' % i   # worker name
            workers.append(Worker(i_name, GLOBAL_AC))

    COORD = tf.train.Coordinator()
    SESS.run(tf.global_variables_initializer())

    if OUTPUT_GRAPH:
        if os.path.exists(LOG_DIR):
            shutil.rmtree(LOG_DIR)
        tf.summary.FileWriter(LOG_DIR, SESS.graph)

    worker_threads = []
    for worker in workers:
        job = lambda: worker.work()
        t = threading.Thread(target=job)
        t.start()
        worker_threads.append(t)
    COORD.join(worker_threads)
    
    plt.figure()
    for worker in workers:
        plt.plot(worker.hist['l_a'])
    plt.xlabel('update number')
    plt.ylabel('actor loss')
    
    plt.figure()
    for worker in workers:
        plt.plot(worker.hist['l_c'])
    plt.xlabel('update number')
    plt.ylabel('critic loss')
    
    plt.figure()
    for worker in workers:
        plt.plot(worker.running_R)
    plt.xlabel('step')
    plt.ylabel('Total moving reward (local)')
    
    plt.figure()
    plt.plot(np.arange(len(GLOBAL_RUNNING_R)), GLOBAL_RUNNING_R)
    plt.xlabel('step')
    plt.ylabel('Total moving reward')

    s_record, o_record, a_record, r_record, ep_r = workers[0].simulate()
    
    t = np.arange(MAX_EP_STEP+1)*dt
    
    plt.figure()
    plt.plot(t.T, np.squeeze(np.array(s_record)[:, 0]))
    
    plt.figure()
    plt.plot(t.T, np.squeeze(np.array(s_record)[:, 1]))
    
    plt.figure()
    plt.plot(t[:-1].T, np.squeeze(np.array(a_record)[:, 0]))
    
    plt.figure()
    plt.plot(t[:-1].T, np.squeeze(np.array(r_record)))
    
    plt.show()
