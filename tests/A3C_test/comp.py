'''
compare my simulator to gym
'''

import numpy as np
import matplotlib.pyplot as plt
import gym
from youngknight.models.Dynamics import PendulumDyn, PendulumObs
from youngknight.models.Rewards import PendReward

home = 'results/'
log_dir = 'logs/'

dt = 0.05
n = 200

theta_min = -np.pi/4.0
theta_max = np.pi/4.0
omega_min = -8.0
omega_max = 8.0
tau_min = -15.0
tau_max = 15.0

m_min = 1.0
m_max = 1.0
l_min = 1.0
l_max = 1.0
c_min = 0.0
c_max = 0.0
g_min = 9.81
g_max = 9.81

pend_params = {}
pend_params['del_t'] = dt
pend_params['tau_min'] = tau_min
pend_params['tau_max'] = tau_max
pend_params['omega_min'] = omega_min
pend_params['omega_max'] = omega_max
pend_params['theta_min'] = theta_min
pend_params['theta_max'] = theta_max
pend_params['clip_tau'] = True
pend_params['clip_omega'] = True
pend_params['clip_theta'] = False
pend_params['inv'] = True

rwd_params = {}
rwd_params['theta_gain'] = 1.0
rwd_params['omega_gain'] = 0.1
rwd_params['tau_gain'] = 0.001

K_d = 0.1
K_p = 1

env = gym.make('Pendulum-v0')
p = np.array([1.0, 1.0, 0.0, 9.81]).reshape(1, 4)

dyn = PendulumDyn(params=pend_params, home=home, name='pend_model_dyn')
obs = PendulumObs(home=home, name='pend_model_obs')

state_size = dyn.state_size
actin_size = dyn.actin_size
param_size = dyn.param_size
obsrv_size = obs.obsrv_size
    
rwd = PendReward(state_size, obsrv_size, actin_size, param_size, params=rwd_params, home=home, name='lqr_reward')

o_0 = env.reset()
x_0 = env.env.state

t = np.arange(n+1)*dt
a = np.zeros(n)
x_sim = np.zeros((n+1, 2))
o_sim = np.zeros((n+1, 3))
r_sim = np.zeros(n)

x_gym = np.zeros((n+1, 2))
o_gym = np.zeros((n+1, 3))
r_gym = np.zeros(n)

x_sim[0, :] = x_0
x_gym[0, :] = x_0
o_sim[0, :] = o_0
o_gym[0, :] = o_0

for i in range(n):
    a[i] = -2 + 4*np.random.rand()
    
    s = dyn.step(x_sim[i, :].reshape(1, 2), a[i].reshape(1, 1), p, i)
    o = obs.observe(x_sim[i, :].reshape(1, 2), p, i)
    r = rwd.get_rwd(x_sim[i, :].reshape(1, 2), s, o_sim[i, :].reshape(1, 3), o, a[i].reshape(1, 1), p, i)
    
    x_sim[i+1, :] = s[0, :]
    o_sim[i+1, :] = o[0, :]
    r_sim[i] = r[0]
    
    o, r, _, _ = env.step(a[i:i+1])
    x_gym[i+1, :] = env.env.state
    o_gym[i+1, :] = o
    r_gym[i] = r
    
plt.figure()
plt.plot(t, o_sim[:, 0])
plt.plot(t, o_gym[:, 0])

plt.figure()
plt.plot(t, o_sim[:, 1])
plt.plot(t, o_gym[:, 1])

plt.figure()
plt.plot(t, o_sim[:, 2])
plt.plot(t, o_gym[:, 2])

plt.figure()
plt.plot(t, x_sim[:, 0])
plt.plot(t, x_gym[:, 0])

plt.figure()
plt.plot(t, x_sim[:, 1])
plt.plot(t, x_gym[:, 1])

plt.figure()
plt.plot(t[0:-1], r_sim)
plt.plot(t[0:-1], r_gym)

plt.show()