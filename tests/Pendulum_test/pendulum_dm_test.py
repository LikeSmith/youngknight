"""
pendulum data_manager test
"""

import numpy as np
import matplotlib.pyplot as plt

from youngknight.datasets import Pendulum_DM

if __name__ == '__main__':
    home = './'
    index = 'test1'
    
    n = 100
    horizon = 1000
    
    dm = Pendulum_DM(index, home)
    dm.autoload(n, horizon, use_noise=False, stack_front=2, stack_back=2)
    
    dm.save()
    
    t = np.arange(horizon+1)*dm.dt
    
    plt.figure()
    plt.plot(t.T, dm['states'][:, :, 0].T)
    
    plt.figure()
    plt.plot(t.T, dm['states'][:, :, 1].T)