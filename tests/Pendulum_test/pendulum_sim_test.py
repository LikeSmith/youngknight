"""
Pendulum test
"""

from youngknight.sims import Pendulum
import numpy as np
import matplotlib.pyplot as plt

def pend_ctrl(state, params):
    K = np.array([-50, -10])
    return np.expand_dims(np.dot(state, K), axis=-1)

if __name__ == '__main__':
    limits = {}
    limits['tau_min'] = -10
    limits['tau_max'] = 10
    limits['m_min'] = 0.5
    limits['m_max'] = 5
    limits['l_min'] = 0.5
    limits['l_max'] = 2
    limits['c_min'] = 1
    limits['c_max'] = 2
    limits['g_min'] = 9.81
    limits['g_max'] = 9.81
    limits['theta_min'] = -np.pi
    limits['theta_max'] = np.pi
    limits['omega_min'] = -1
    limits['omega_max'] = 1
    
    noise_params = {}
    noise_params['process_mean'] = np.zeros((1, 2))
    noise_params['process_cov'] = np.eye(2)*0.01
    noise_params['signal_mean'] = np.zeros((1, 1))
    noise_params['signal_cov'] = np.eye(1)*0.01
    
    dt = 0.01
    horizon = 1000
    N_sims = 100
    
    sim_rand = Pendulum(limits, noise_params, dt)
    sim_ctrl = Pendulum(limits, noise_params, dt, ctrl=pend_ctrl)
    
    x_0 = sim_rand.init_state_gen(N_sims)
    
    res_rand, _ = sim_rand.gen_trajectories(x_0, horizon, verbosity=1)
    res_ctrl, _ = sim_ctrl.gen_trajectories(x_0, horizon, verbosity=1)

    t = np.arange(horizon+1)*dt
    
    plt.figure()
    plt.plot(t.T, res_rand['states'][:, :, 0].T)
    plt.title('Randomized Inputs, Position')
    
    plt.figure()
    plt.plot(t.T, res_rand['states'][:, :, 1].T)
    plt.title('Randomized Inputs, Velocity')
    
    plt.figure()
    plt.plot(t.T, res_ctrl['states'][:, :, 0].T)
    plt.title('Controlled Inputs, Position')
    
    plt.figure()
    plt.plot(t.T, res_ctrl['states'][:, :, 1].T)
    plt.title('Controlled Inputs, Velocity')

    plt.show()
