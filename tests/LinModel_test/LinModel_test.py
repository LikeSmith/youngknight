"""
test Linear Model

this script tests the linear model
"""

import numpy as np
from youngknight.models import LinModel

if __name__=='__main__':
    input_size = 3
    output_size = 2
    learning_rate = 0.1
    samples = 10000
    val_samp = 1000
    
    A = np.array([[1.0, 2.0, 3.0], [3.0, 2.0, 1.0]])
    b = np.array([5.0, 6.0])
    
    x_min = np.array([[-10.0, -10.0, -10.0]])
    x_max = np.array([10.0, 10.0, 10.0])
    
    params = {'input_size':input_size, 'output_size':output_size, 'learning_rate':learning_rate}
    
    mdl = LinModel(params, home='test_data/', name='Linear_Model_test')
    
    x_min_train = np.tile(x_min.reshape(1, input_size), (samples, 1))
    x_max_train = np.tile(x_max.reshape(1, input_size), (samples, 1))
    
    x = x_min_train + (x_max_train - x_min_train)*np.random.rand(samples, input_size)
    y = np.dot(x, A.T) + np.tile(b.T, (samples, 1))
    
    x_min_val = np.tile(x_min.reshape(1, input_size), (val_samp, 1))
    x_max_val = np.tile(x_max.reshape(1, input_size), (val_samp, 1))
    
    x_val = x_min_val + (x_max_val - x_min_val)*np.random.rand(val_samp, input_size)
    y_val = np.dot(x_val, A.T) + np.tile(b.T, (val_samp, 1))
    
    mdl.train(mdl.gen_train_feed(x, y), mdl.gen_train_feed(x_val, y_val), samples, epochs=100, batch_size=128, patience=10, verbosity=2)
    
    mdl.save('test1.pkl')
    
    mdl2 = LinModel(load_file='test1.pkl', home='test_data/', name='Linear_Model_test2')
    
    x_test = np.array([[1.0, 2.0, 3.0]])
    
    print(np.dot(x_test, A.T) + b.T)
    print(mdl(mdl.gen_input_feed(x_test)))
    print(mdl2(mdl2.gen_input_feed(x_test)))
    