#YoungKnight#
*Young Knight, learn...*
-Johannes Liechtenauer

##What is this?##
This repo is a set of tools built on top of Google's tensorflow library to facilitate deep learning experiments and research
at the University of Utah and at the George Washington University.
